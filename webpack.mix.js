const mix = require('laravel-mix');

let SpritesmithPlugin = require('webpack-spritesmith');
let path = require('path');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.disableSuccessNotifications()
    .webpackConfig({
        plugins: [
            new SpritesmithPlugin({
                src: {
                    cwd: path.resolve(__dirname, 'resources/sass/site/sprites'),
                    glob: '*.png'
                },
                target: {
                    image: path.resolve(__dirname, 'public/css/sprite.png'),
                    css: path.resolve(__dirname, 'resources/sass/site/libs/sprite.scss')
                },
                apiOptions: {
                    cssImageRef: "/css/sprite.png"
                }
            })
        ]
    })
    .js('resources/js/admin/app.js', 'public/admin/js/app.bundle.js')
    .js('resources/js/site/app.js', 'public/js/app.bundle.js')
    .sass('resources/sass/site/app.scss', 'public/css/app.bundle.css')
    .sass('resources/sass/admin/app.scss', 'public/admin/css/app.bundle.css')
    .sourceMaps(false, 'source-map')
    .version();
