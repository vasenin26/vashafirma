<?php

Route::get('/', 'EstateCatalogController@index')->name('index');
Route::get('/search', 'EstateCatalogController@search')->name('ad.search');
Route::get('/object-{id}', 'EstateCatalogController@showAd')->name('ad.show');

Route::get('/category/{alias}', 'EstateCatalogController@showCategory')
    ->where('alias', '[A-Za-z/-]+')
    ->name('category.show');
Route::get('/catalog/sold', 'EstateCatalogController@sold')->name('category.sold');

Route::get('/articles', 'ArticleController@index')->name('articles.index');

Route::get('/catalog/sale', function() {
    return view('site/special/sale', ['meta_title' => 'Продажа бизнеса']);
})->name('category.sale');

Route::get('/reviews', 'ReviewController@index')->name('reviews.index');
Route::get('/jobs', 'JobsController@index')->name('jobs.index');
Route::get('/jobs/{id}', 'JobsController@show')->name('jobs.show');

Route::group([
    'as' => 'special.',
], function () {
    Route::get('/modal/{key}', 'ServicePageController@showModal')->name('modal');
    Route::post('/form/{key}', 'FromHandlerController@send')->name('form.handler');

    /**
     * Special page resolved by router name
     * All special page used template from site/special/[route_name]
     */
    Route::get('/contacts', 'ServicePageController@special')->name('contacts');
    Route::get('/about-us', 'ServicePageController@special')->name('about');

    /** Business services */
    Route::get('/poluchenie-vypisok', 'ServicePageController@special')->name('extracts');
    Route::get('/poisk-biznesa', 'ServicePageController@special')->name('business-search');
    Route::get('/proverka-riskov', 'ServicePageController@special')->name('risk-check');
    Route::get('/soprovozhdenie-sdelki', 'ServicePageController@special')->name('transaction-support');
    Route::get('/registraciya-yuridicheskih-lic', 'ServicePageController@special')->name('registration-entities');

    /** Other service page */
    Route::get('/services', 'ServicePageController@special')->name('other-service');
});

Auth::routes();

/* admin management routes */

Route::group([
    'prefix' => 'admin',
    'as' => 'admin.',
    'namespace' => 'Admin',
    'middleware' => [
        'auth'
    ]
], function () {

    App\Http\Controllers\Admin\CustomerCallingController::route();
    App\Http\Controllers\Admin\DashboardController::route();
    App\Http\Controllers\Admin\DeclarationController::route();
    App\Http\Controllers\Admin\PlaneController::route();

    Route::resource('articles', 'ArticlesController');
    Route::resource('categories', 'CategoriesController');

    Route::resource('users', 'UsersController');
    Route::get('users/{id}/roles', 'UsersController@showRoles')->name('users.roles');
    Route::put('users/{id}/roles', 'UsersController@updateRoles')->name('users.updateRoles');

    Route::resource('roles', 'RolesController');

    //TODO: exclude show resource

    App\Http\Controllers\Admin\EstateController::route();

    Route::get('customer-calling-history/{id}/event/{event_id}', 'CustomerCallingController@editEvent')->name('customer.calling.event');
    Route::put('customer-calling-history/{id}/event/{event_id}', 'CustomerCallingController@updateEvent')->name('customer.calling.event.update');
    Route::get('customer-calling-history/{id}/event/{event_id}/delete', 'CustomerCallingController@deleteEvent')->name('customer.calling.event.delete');

    Route::get('customer-calling-history/{id}/comments', 'CustomerCallingController@comments')->name('customer.calling.comments');
    Route::post('customer-calling-history/{id}/comments', 'CustomerCallingController@addComment')->name('customer.calling.comments.add');
    Route::get('customer-calling-history/{id}/comments/{commentId}', 'CustomerCallingController@editComment')->name('customer.calling.comments.edit');
    Route::put('customer-calling-history/{id}/comments/{commentId}', 'CustomerCallingController@updateComment')->name('customer.calling.comments.update');
    Route::get('customer-calling-history/{id}/comments/{commentId}/delete', 'CustomerCallingController@deleteComment')->name('customer.calling.comments.delete');

    Route::resource('review', 'ReviewController');
    Route::resource('jobs', 'JobsController');

    App\Http\Controllers\Admin\DirectoryController::route();

    Route::get('directory/{index}/item/{id}', 'DirectoryController@item')->name('directory.item');

    Route::get('/review', 'ReviewController@index')->name('review.index');
    Route::delete('/review/{id}', 'ReviewController@destroy')->name('review.destroy');

    Route::get('/jobs', 'JobsController@index')->name('jobs.index');
    Route::get('/jobs/{id}', 'JobsController@show')->name('job.show');
    Route::get('/jobs/{id}/delete', 'JobsController@destroy')->name('jobs.destroy');

    Route::get('documents', 'DocumentsController@index')->name('documents.index');
    Route::post('documents', 'DocumentsController@upload')->name('documents.upload');
    Route::get('documents/delete', 'DocumentsController@delete')->name('documents.delete');

    Route::get('forms/', 'FormReportController@index')->name('form.index');
    Route::get('forms/{form_id}', 'FormReportController@show')->name('form.show');

    Route::get('forms/{estate}/comments', 'FormReportController@comments')->name('form.comments');
    Route::post('forms/{estate}/comments', 'FormReportController@addComment')->name('form.comments.add');
    Route::get('forms/{estate}/comments/{commentId}', 'FormReportController@editComment')->name('form.comments.edit');
    Route::put('forms/{estate}/comments/{commentId}', 'FormReportController@updateComment')->name('form.comments.update');
    Route::get('forms/{estate}/comments/{commentId}/delete', 'FormReportController@deleteComment')->name('form.comments.delete');
});

Route::get('/image/', 'ImageGeneratorController@generate');
Route::get('/image/flush', 'ImageGeneratorController@flush');

Route::post('basket', 'FileBasketController@putFile')->name('basket');
Route::delete('basket', 'FileBasketController@dropFile');

Route::get('{url}', 'ArticleController@show')->name('articles.show')
    ->where('url', '.*');