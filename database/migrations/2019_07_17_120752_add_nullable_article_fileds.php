<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNullableArticleFileds extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('articles', function (Blueprint $table) {
            $table->string('meta_title')->nullable(true)->change();
            $table->string('description')->nullable(true)->change();
            $table->string('keywords')->nullable(true)->change();
            $table->string('parent_id')->nullable(true)->default(null)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('articles', function (Blueprint $table) {
            $table->string('meta_title')->nullable(false)->change();
            $table->string('description')->nullable(false)->change();
            $table->string('keywords')->nullable(false)->change();
            $table->string('parent_id')->nullable(false)->default(0)->change();
        });
    }
}
