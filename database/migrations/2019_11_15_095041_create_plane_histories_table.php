<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlaneHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plane_histories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('broker_id');

            $table->unsignedInteger('object_id')->nullable();
            $table->string('object_class')->nullable();

            $table->unsignedInteger('action_id')->nullable();
            $table->dateTime('action_at')->nullable();

            $table->unsignedInteger('created_by');
            $table->mediumText('content');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('plane_histories');
    }
}
