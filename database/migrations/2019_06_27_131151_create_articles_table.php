<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    public function up()
    {
        Schema::create('articles', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('title', 140);
            $table->string('alias', 140)->unique();
            $table->string('meta_title');
            $table->string('description');
            $table->string('keywords');
            $table->text('text');
            $table->integer('parent_id')->default(0);
            $table->string('created_by');
            $table->string('published_at');
            $table->string('deleted_by')->nullable();
            $table->string('deleted_at')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('articles');
    }
}
