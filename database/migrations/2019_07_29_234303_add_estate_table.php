<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEstateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('estates', function (Blueprint $table) {

            $table->increments('id');

            $table->unsignedBigInteger('broker_id')->nullable();
            $table->unsignedBigInteger('estate_state')->nullable();
            $table->unsignedBigInteger('publication_state')->nullable();
            $table->unsignedBigInteger('marketing_type')->nullable();
            $table->datetime('first_published_at')->nullable();
            $table->boolean('is_archived')->default(0);
            $table->unsignedBigInteger('contract_type')->nullable();
            $table->datetime('contract_signing_at')->nullable();
            $table->datetime('contract_expiring_at')->nullable();

            $table->text('object_title')->nullable();
            $table->text('object_addr')->nullable();

            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('estates');
    }
}
