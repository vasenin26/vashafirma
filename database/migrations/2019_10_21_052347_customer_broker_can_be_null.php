<?php

use Doctrine\DBAL\Schema\Table;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CustomerBrokerCanBeNull extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('customer_calling_histories', function (Blueprint $table) {
            $table->unsignedBigInteger('broker_id')
                ->nullable(true)
                ->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customer_calling_histories', function (Blueprint $table) {
            $table->unsignedBigInteger('broker_id')
                ->nullable(false)
                ->change();
        });
    }
}
