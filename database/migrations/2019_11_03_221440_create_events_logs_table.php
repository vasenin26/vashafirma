<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('call_events_logs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('thread_id');
            $table->string('class_name');

            $table->integer('created_by');
            $table->mediumText('description')->default('')->nullable();
            $table->dateTime('next_call_date')->nullable();
            $table->integer('rating')->default(0);

            $table->integer('managed_by')->nullable();
            $table->mediumText('managed_reason')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('call_events_logs');
    }
}
