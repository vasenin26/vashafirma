<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeclarationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('declarations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('created_by');

            $table->string('client_name');
            $table->string('client_phone');
            $table->string('client_email')->nullable();
            $table->boolean('is_subscription');
            $table->text('comment')->nullable();

            $table->unsignedBigInteger('estate_id');
            $table->date('display_date');
            $table->unsignedBigInteger('broker_id');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('declarations');
    }
}
