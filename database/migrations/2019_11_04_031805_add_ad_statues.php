<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAdStatues extends Migration
{

    private $statuses = [
        [
            'special_key' => 'ad_state_checked',
            'dictionary_group' => 'ad_state',
            'value' => 'Проверено',
            'description' => 'Активируется на сайте (при сохранении объекта)',
        ],
        [
            'special_key' => 'ad_state_wait',
            'dictionary_group' => 'ad_state',
            'value' => 'На стадии проверки',
            'description' => 'Не попадает в рабочий день',
        ],
        [
            'special_key' => 'ad_state_new',
            'dictionary_group' => 'ad_state',
            'value' => 'Новое',
            'description' => 'Попадает в рабочий день',
        ],
        [
            'special_key' => 'ad_state_paid',
            'dictionary_group' => 'ad_state',
            'value' => 'Платное',
            'description' => 'Дает возможность на сайте заменить контакты брокера на контакты собственника',
        ],
        [
            'special_key' => 'ad_state_archive',
            'dictionary_group' => 'ad_state',
            'value' => 'Отправить в архив',
            'description' => 'Стандартные объявления',
        ],
    ];

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        foreach($this->statuses as $item){
            $permission = new \App\Dictionary();

            $permission->special_key = $item['special_key'];
            $permission->dictionary_group = $item['dictionary_group'];
            $permission->value = $item['value'];
            $permission->description = $item['description'];

            $permission->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        foreach($this->statuses as $item){
            $item = \App\Dictionary::whereSpecialKey($item['special_key'])->first();

            if($item){
                $item->delete();
            }
        }
    }
}
