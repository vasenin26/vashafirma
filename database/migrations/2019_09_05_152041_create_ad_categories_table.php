<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ad_categories', function (Blueprint $table) {

            $table->unsignedBigInteger('ad_id');
            $table->unsignedBigInteger('category_id');

            $table->foreign('ad_id')
                ->references('id')
                ->on('ads');

            $table->foreign('category_id')
                ->references('id')
                ->on('estate_categories');

            $table->unique(['ad_id', 'category_id']);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ad_categories');
    }
}
