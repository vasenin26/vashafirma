<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPersonalPlanePermission extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $permission = new \App\Permission();

        $permission->name = 'personal_planning';
        $permission->display_name = 'Планирование личного времени';

        $permission->save();

        $permission = new \App\Permission();

        $permission->name = 'broker_planning';
        $permission->display_name = 'Просмотр планов броекров';

        $permission->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \App\Permission::whereName('personal_planning')
            ->first()->delete();

        \App\Permission::whereName('broker_planning')
            ->first()->delete();
    }
}
