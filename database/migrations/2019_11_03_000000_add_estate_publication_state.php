<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEstatePublicationState extends Migration
{

    private $statuses = [
        [
            'spacial_key' => 'estate_typical',
            'dictionary_group' => 'publication_state',
            'value' => 'Стандарт',
            'description' => 'Стандартные объявления',
        ],
        [
            'spacial_key' => 'estate_premium',
            'dictionary_group' => 'publication_state',
            'value' => 'Premium',
            'description' => 'Премиальне объявления',
        ],
    ];

    private $estate_state = [
        [
            'spacial_key' => 'advert_typical',
            'dictionary_group' => 'estate_state',
            'value' => 'Стандарт',
            'description' => 'Стандартные объявления',
        ],
        [
            'spacial_key' => 'advert_premium',
            'dictionary_group' => 'estate_state',
            'value' => 'Premium',
            'description' => 'Премиальне объявления',
        ],
    ];

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        foreach($this->statuses as $item){
            $permission = new \App\Dictionary();

            $permission->spacial_key = $item['spacial_key'];
            $permission->dictionary_group = $item['dictionary_group'];
            $permission->value = $item['value'];
            $permission->description = $item['description'];

            $permission->save();
        }

        foreach($this->estate_state as $item){
            $item = \App\Dictionary::whereSpacialKey($item['spacial_key'])->first();

            if($item){
                $item->delete();
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        foreach($this->statuses as $item){
            $item = \App\Dictionary::whereSpacialKey($item['spacial_key'])->first();

            if($item){
                $item->delete();
            }
        }

        foreach($this->estate_state as $item){
            $permission = new \App\Dictionary();

            $permission->spacial_key = $item['spacial_key'];
            $permission->dictionary_group = $item['dictionary_group'];
            $permission->value = $item['value'];
            $permission->description = $item['description'];

            $permission->save();
        }
    }
}
