<?php

use App\EstateCategory;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCategoryCounters extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('estate_categories', function (Blueprint $table) {
            $table->integer('ad_counters')->nullable()->after('alias');
            $table->integer('min_price')->nullable()->after('ad_counters');
        });

        $categories = EstateCategory::all();

        foreach($categories as $category){
            EstateCategory::calculate($category->id);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('estate_categories', function (Blueprint $table) {
            $table->dropColumn('ad_counters');
            $table->dropColumn('min_price');
        });
    }
}
