<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDictionaryAccessPermission extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $permission = new \App\Permission();

        $permission->name = 'full_dictionary_access';
        $permission->display_name = 'Доступ ко всем данным словарей';

        $permission->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \App\Permission::whereName('full_dictionary_access')
            ->first()->delete();
    }
}
