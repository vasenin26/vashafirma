<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ads', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('created_by');
            $table->unsignedBigInteger('estate_id');

            $table->string('contact_name');
            $table->string('contact_phone');
            $table->string('contact_email')->nullable();

            $table->text('ad_description');

            $table->string('region')->nullable();
            $table->string('metro')->nullable();
            $table->string('legal_form')->nullable();
            $table->string('site_url')->nullable();

            $table->integer('period_of_existence')->nullable();
            $table->integer('number_of_staff')->nullable();

            $table->string('reason_for_sale')->nullable();

            $table->unsignedInteger('price');
            $table->integer('revenue')->nullable();
            $table->integer('profit')->nullable();
            $table->integer('projected_earnings')->nullable();
            $table->integer('payback_period')->nullable();

            $table->text('cost_structure')->nullable();
            $table->text('property')->nullable();
            $table->text('means_of_production')->nullable();
            $table->text('documents')->nullable();
            $table->text('staff')->nullable();
            $table->text('benefits')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ads');
    }
}
