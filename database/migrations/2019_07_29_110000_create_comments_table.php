<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comment', function (Blueprint $table) {

            $table->increments('id');
            $table->integer('parent')->nullable();
            $table->unsignedInteger('thread_id');
            $table->string('class_name');

            $table->integer('created_by');
            $table->mediumText('content');
            $table->integer('rating')->default(0);

            $table->integer('managed_by')->nullable();
            $table->mediumText('managed_reason')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comment');
    }
}
