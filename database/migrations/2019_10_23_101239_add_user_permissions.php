<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUserPermissions extends Migration
{

    private $permissions = [
        [
            'name' => 'user_data_access',
            'display_name' => 'Доступ к данным других пользователей',
        ],
        [
            'name' => 'manage_user',
            'display_name' => 'Управление пользователями',
        ],
        [
            'name' => 'manage_documents',
            'display_name' => 'Управление документами',
        ],
        [
            'name' => 'manage_dictionary',
            'display_name' => 'Управление справочниками',
        ],
        [
            'name' => 'manage_vacancy',
            'display_name' => 'Управление вакансиямиы',
        ],
        [
            'name' => 'manage_review',
            'display_name' => 'Управление отзывами',
        ],
    ];

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        foreach($this->permissions as $item){
            $permission = new \App\Permission();

            $permission->name = $item['name'];
            $permission->display_name = $item['display_name'];

            $permission->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        foreach($this->permissions as $item){
            \App\Permission::whereName($item['name'])
                ->first()->delete();
        }
    }
}
