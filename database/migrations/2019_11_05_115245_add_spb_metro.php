<?php

use App\Dictionary;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSpbMetro extends Migration
{
    private $list = [
        'Автово',
        'Адмиралтейская',
        'Академическая',
        'Балтийская',
        'Бухарестская',
        'Василеостровская',
        'Владимирская',
        'Волковская',
        'Выборгская',
        'Горьковская',
        'Гостиный двор',
        'Гражданский проспект',
        'Девяткино',
        'Достоевская',
        'Елизаровская',
        'Звёздная',
        'Звенигородская',
        'Кировский завод',
        'Комендантский проспект',
        'Крестовский остров',
        'Купчино',
        'Ладожская',
        'Ленинский проспект',
        'Лесная',
        'Лиговский проспект',
        'Ломоносовская',
        'Маяковская',
        'Международная',
        'Московская',
        'Московские ворота',
        'Нарвская',
        'Невский проспект',
        'Новочеркасская',
        'Обводный канал',
        'Обухово',
        'Озерки',
        'Парк Победы',
        'Парнас',
        'Петроградская',
        'Пионерская',
        'Площадь Александра Невского 1',
        'Площадь Александра Невского 2',
        'Площадь Восстания',
        'Площадь Ленина',
        'Площадь Мужества',
        'Политехническая',
        'Приморская',
        'Пролетарская',
        'Проспект Большевиков',
        'Проспект Ветеранов',
        'Проспект Просвещения',
        'Пушкинская',
        'Рыбацкое',
        'Садовая',
        'Сенная площадь',
        'Спасская',
        'Спортивная',
        'Старая Деревня',
        'Технологический институт 1',
        'Технологический институт 2',
        'Удельная',
        'Улица Дыбенко',
        'Фрунзенская',
        'Чёрная речка',
        'Чернышевская',
        'Чкаловская',
        'Электросила',
    ];


    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        foreach ($this->list as $item) {
            $d = new Dictionary();

            $d->value = $item;
            $d->created_by = 0;
            $d->dictionary_group = 'metro';

            $d->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Dictionary::where('dictionary_group', 'metro')->delete();
    }
}
