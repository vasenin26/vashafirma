<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPermissionCallEventsLogs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $permission = new \App\Permission();

        $permission->name = 'manage_events_logs';
        $permission->display_name = 'Редактирование журналированных событий';
        $permission->description = 'Эта превелегия продляет доступ к редактированию записей журнара событий.';

        $permission->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \App\Permission::whereName('manage_events_logs')
            ->first()->delete();
    }
}
