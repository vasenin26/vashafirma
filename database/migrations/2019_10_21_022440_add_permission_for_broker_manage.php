<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPermissionForBrokerManage extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $permission = new \App\Permission();

        $permission->name = 'set_declaration_broker';
        $permission->display_name = 'Назначать брокера для деклараций';

        $permission->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \App\Permission::whereName('set_declaration_broker')
            ->first()->delete();
    }
}
