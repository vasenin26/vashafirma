<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPusblishedFieldForArticle extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('articles', function (Blueprint $table) {
            $table->dateTime('published_at')->nullable(true)->change();
            $table->unsignedInteger('published_by')->nullable()->after('created_by');

            $table->dateTime('deleted_at')->nullable(true)->change();

            $table->unsignedInteger('created_by')->change();
            $table->unsignedInteger('deleted_by')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('articles', function (Blueprint $table) {
            $table->string('published_at')->nullable(false)->change();
            $table->dropColumn('published_by');
        });
    }
}
