<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DisplayDateAsNullable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('declarations', function (Blueprint $table) {
            $table->date('display_date')->nullable(true)->change();
        });

        DB::update("UPDATE `declarations` SET display_date = null WHERE display_date = '0000-00-00'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::update("UPDATE `declarations` SET display_date = '0000-00-00' WHERE display_date IS NULL ");
        Schema::table('declarations', function (Blueprint $table) {
            $table->date('display_date')->nullable(false)->change();
        });
    }
}
