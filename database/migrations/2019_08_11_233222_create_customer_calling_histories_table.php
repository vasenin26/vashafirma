<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomerCallingHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer_calling_histories', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('created_by');
            $table->unsignedBigInteger('broker_id');
            $table->unsignedBigInteger('customer_source_id');

            $table->string('phone');

            $table->text('ad_text');
            $table->text('call_result');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer_calling_histories');
    }
}
