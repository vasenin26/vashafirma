<?php

namespace App\Console\Commands;

use App\Permission;
use App\Role;
use App\User;
use Illuminate\Console\Command;

class AllowAll extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'allow_all {email}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Allow all to user';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $email = $this->argument('email');
        $user = User::whereEmail($email)->firstOrFail();

        $adminRole = $this->getAdminRole();

        if(!$user->hasRole($adminRole->name)){
            $user->attachRole($adminRole);
        }
    }

    private function getAdminRole()
    {
        $adminRole = Role::whereName('admin')->first();

        if(empty($adminRole)){
            /**
             * @var Role
             */
            $adminRole = Role::create([
                'name' => 'admin',
                'display_name' => 'Администратор'
            ]);

            $adminRole->attachPermissions(Permission::all());
        }

        return $adminRole;
    }
}
