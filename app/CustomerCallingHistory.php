<?php

namespace App;

use App\Interfaces\BelongsToUser;
use App\Interfaces\SelectableI;
use App\Traits\CommentsModel;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class CustomerCallingHistory extends Model implements BelongsToUser, SelectableI
{
    use Traits\BelongsToUser;
    use Traits\UserAccess;
    use Traits\CallLogModel;
    use Traits\SelectableModel;

    protected $accessPermission = 'edit_customer_call_history';
    protected $ownerAccessDelay = 300;

    protected $fillable = [
        'created_by',
        'broker_id',
        'customer_source_id',
        'phone',
        'link',
        'ad_text',
        'call_result',
    ];

    public function getPreviewLabelAttribute(): string
    {
        return Str::limit($this->attributes['ad_text']);
    }

    public function broker(){
        return $this->hasOne('App\User', 'id', 'broker_id');
    }

    public function source(){
        return $this->hasOne('App\Dictionary', 'id', 'customer_source_id');
    }

    public function scopeSearch(Builder $builder, $search)
    {
        return $builder->where('phone', 'like', "%$search%")
            ->orWhere('link', 'like', "%$search%")
            ->orWhere('ad_text', 'like', "%$search%")
            ->orWhere('call_result', 'like', "%$search%");
    }

}
