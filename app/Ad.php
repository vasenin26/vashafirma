<?php

namespace App;

use App\Services\ImageGenerator;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Str;
use App\Traits\ModelImages;
use App\Interfaces\ReachModelI;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Spatie\Url\Url as URI;
use Illuminate\Support\Facades\DB;

class Ad extends Model implements ReachModelI
{
    use ModelImages;

    protected $fillable = [
        'contact_name',
        'contact_phone',
        'contact_email',
        'ad_state',
        'ad_description',
        'region',
        'metro_id',
        'legal_form',
        'site_url',
        'period_of_existence',
        'number_of_staff',
        'reason_for_sale',
        'price',
        'revenue',
        'profit',
        'projected_earnings',
        'payback_period',
        'cost_structure',
        'property',
        'means_of_production',
        'documents',
        'staff',
        'benefits',
        'additional_info',

        'published_at',
        'publication_state',

        'main_category_id',
        'additional_category_id',

        'video_link',
        'presentation_file',
    ];

    protected $dates = ['published_at'];

    const PREMIUM_KEY = 'estate_premium';
    const PAID_KEY = 'ad_state_paid';

    public static function boot()
    {
        parent::boot();

        static::deleting(function ($model) {
            $model->adCategories()->delete();
        });
    }

    public function __construct(array $attributes = [])
    {
        $this->fileFolder = 'proposal/%s/%s';
        parent::__construct($attributes);
    }

    public function estate()
    {
        return $this->hasOne('App\Estate', 'id', 'estate_id');
    }

    public function publication()
    {
        return $this->hasOne('App\Dictionary', 'id', 'publication_state');
    }

    public function metro()
    {
        return $this->hasOne('App\Dictionary', 'id', 'metro_id');
    }

    public function setAdStateAttribute($value)
    {
        $option = Dictionary::find($value);

        if ($option) {

            $publishedAt = -1;

            switch ($option->special_key) {
                case 'ad_state_checked':
                case 'ad_state_paid':
                    $publishedAt = Carbon::now();
                    break;
                case 'ad_state_new':
                case 'ad_state_wait':
                case 'ad_state_archive':
                default:
                    $publishedAt = null;
                    break;
            }

            if ($publishedAt !== -1) {
                $this->attributes['published_at'] = $publishedAt;
            }

            if (!is_null($publishedAt)) {
                $estate = $this->estate;

                if ($estate && !$estate->first_published_at) {
                    $estate->first_published_at = $publishedAt;
                    $estate->save();
                }
            }
        } else if ($value === null) {
            $this->attributes['published_at'] = null;
        }

        $this->attributes['ad_state'] = $value;
    }

    public function getPresentationPathAttribute()
    {
        if (!$this->attributes['presentation_file']) {
            return null;
        }
        return Storage::url($this->getFolder() . $this->attributes['presentation_file']);
    }

    public function getVideoIdAttribute()
    {
        if (empty($this->attributes['video_link'])) {
            return null;
        }

        $uri = URI::fromString($this->attributes['video_link']);

        return $uri->getQueryParameter('v', null);
    }

    public function mainCategory()
    {
        return $this->hasOneThrough(
            EstateCategory::class,
            AdCategory::class,
            'ad_id',
            'id',
            'id',
            'category_id'
        )->whereNotNull('alias');
    }

    public function adCategories()
    {
        return $this->hasMany(AdCategory::class, 'ad_id', 'id');
    }

    public function categories()
    {
        return $this->hasManyThrough(
            EstateCategory::class,
            AdCategory::class,
            'ad_id',
            'id',
            'id',
            'category_id'
        );
    }

    public function state()
    {
        return $this->hasOne(
            Dictionary::class,
            'id',
            'ad_state'
        );
    }

    public function attachCategory($categoryId)
    {
        AdCategory::create([
            'ad_id' => $this->id,
            'category_id' => $categoryId
        ]);
    }

    public function detachCategory($categoryId)
    {
        AdCategory::erase($this->id, $categoryId);
    }

    public function attachePresentation($file)
    {
        if ($file instanceof \Illuminate\Http\File) {
            $this->attributes['presentation_file'] = Str::random() . '.' . $file->getExtension();

            \Storage::disk()->putFileAs(
                $this->getFolder(),
                $file,
                $this->attributes['presentation_file']
            );
        } else {
            $this->attributes['presentation_file'] = null;
        }

        return $this;
    }

    public function getPublicPhone()
    {
        if ($this->hasPaid()) {
            if ($this->contact_phone) {
                return $this->contact_phone;
            }
        }

        $borker = $this->getBroker();

        return $borker ? $borker->phone : $this->contact_phone;
    }

    public function getBroker($key = null)
    {
        $broker = $this->estate->broker;
        if (empty($broker)) {
            return null;
        }

        if ($key) {
            return $broker->$key;
        }

        return $broker;
    }

    public function getPublicPhoneDisguised()
    {
        return str_pad(substr($this->getPublicPhone(), 0, 12), 18, '-xx');
    }

    public function hasPaid()
    {
        static $paidStatus = null;

        if (is_null($paidStatus)) {
            $paidStatus = $this->state;
        }

        return $paidStatus && $paidStatus->special_key === self::PAID_KEY;
    }

    public function hasPremium()
    {
        static $publicationStatus = null;

        if (is_null($publicationStatus)) {
            $publicationStatus = $this->publication;
        }

        return $publicationStatus && $publicationStatus->special_key === self::PREMIUM_KEY;
    }

    public function getPreview()
    {
        $preview = current($this->images);

        if (empty($preview)) {
            $preview = new ImageGenerator();
        }

        return $preview;
    }

    public function getSimilarAdverts($limit = 3)
    {
        return [];
    }

    public function scopeIsPublished($scope)
    {
        return $scope->whereNotNull('published_at')
            ->whereHas('estate', function (Builder $builder) {
                return $builder->whereIsArchived(0);
            });
    }

    public function scopeIsPremium($scope)
    {
        return $scope->whereHas('publication', function (Builder $builder) {
            return $builder->whereSpecialKey(self::PREMIUM_KEY);
        });
    }

    public function scopeMinPrice($scope)
    {
        return $scope->select(DB::raw('MIN(ads.price) as min_price'))
            ->first();
    }
}
