<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class DirectoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        \App::bind('directory.manager', function () {
            return new \App\Service\Directory;
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
