<?php

namespace App\Traits;

use Illuminate\Http;
use App\Services\ImageGenerator;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;

trait ModelImages
{

    protected $imageFolderIdKey = 'id';
    protected $fileFolder = '';
    protected $folder = null;

    private $imageForRemove = [];

    public function getImagesAttribute($value)
    {

        if (empty($value)) {
            return [];
        }

        $files = json_decode($value, true);

        if (json_last_error() !== JSON_ERROR_NONE) {
            return [];
        }

        if (!is_array($files)) {
            return [];
        }

        $images = [];
        foreach ($files as $file) {
            $images[] = new ImageGenerator($this->getImagesPath($file));
        }

        return $images;
    }

    public function setImagesAttribute($value)
    {
        $this->attributes['images'] = json_encode($value);
    }

    public function attacheUploadedImage($uploaded)
    {
        if (!$uploaded instanceof Http\UploadedFile) {
            return $this;
        }

        $images = json_decode($this->attributes['images']);

        if (empty($images)) {
            $images = [];
        }

        $imagePath = $uploaded->store($this->getImagesPath());
        $image = \File::basename($imagePath);

        $images[] = $image;

        $this->setImagesAttribute($images);

        return $imagePath;
    }

    public function attacheImageByPath($filePath)
    {
        $images = json_decode($this->attributes['images'] ?? '[]', true);

        if (empty($images)) {
            $images = [];
        }

        $newFilePath = $this->generateNewImagePath($filePath);

        if (Storage::disk()->copy($filePath, $newFilePath)) {
            $images[] = \File::basename($newFilePath);
        }

        $this->setImagesAttribute($images);

        return $newFilePath;
    }

    public function detachImage($image)
    {
        $images = json_decode($this->attributes['images'] ?? '[]', true);

        $filteredImages = array_filter($images, function ($item) use ($image) {
            return $item !== $image;
        });

        $this->attributes['images'] = json_encode($filteredImages);
    }

    public function getFolder() : string
    {
        if (is_null($this->folder)) {
            $level1 = floor($this->{$this->imageFolderIdKey} / 1000);
            $level2 = $this->{$this->imageFolderIdKey} % 1000;

            $folder = sprintf($this->fileFolder, $level1, $level2);
            $folder = rtrim($folder, '/') . '/';

            $this->folder = $folder;// [$this->imagesPath, $level1, $level2];
        }

        return $this->folder;
    }

    public function getImagesPath(string $fileName = null) : string
    {
        $path = [$this->getFolder(), 'images'];

        if (!is_null($fileName)) {
            $path[] = $fileName;
        }

        return join('/', $path);
    }

    protected function actualizeImages()
    {
        //TODO: need actualize image after manipulation
    }

    private function generateNewImagePath($filePath)
    {
        $newFileName = Str::random() . '.' . \File::extension($filePath);

        return join('/', [$this->getImagesPath(), $newFileName]);
    }

}