<?php

namespace App\Traits;

use Illuminate\Support\Facades\Auth;

trait BelongsToUser
{

    protected static function bootBelongsToUser()
    {
        static::creating(function ($model) {
            if (!\Auth::guest()) {
                $userId = \Auth::user()->id;
            }

            $model->created_by = $userId ?? 0;
        });
    }

    public function scopeFilterByAccess($scope, $userId = null, $column = 'created_by')
    {
        if(Auth::guest()){
            abort(403);
        }

        if (!Auth::user()->can('user_data_access')) {
            $userId = Auth::user()->id;
        }

        if ($userId) {
            $scope->where($column, $userId);
        }

        return $scope;
    }

    public function owner()
    {
        return $this->belongsTo('App\User', 'created_by', 'id');
    }

    public function isOwner()
    {
        return Auth::user() && (int)$this->created_by === (int)Auth::user()->id;
    }

}