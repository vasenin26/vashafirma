<?php
/**
 * Filename: SelectableModel.php
 * Creator: Vasenin Leonid {leonid.vasenin@gmail.com}
 * Date: 17.11.2019
 */

namespace App\Traits;

use \Illuminate\Database\Eloquent\Builder;
use \Illuminate\Support\Collection;

trait SelectableModel
{
    public function scopeGetSelectableArray(Builder $scope): Collection
    {
        return $scope->get()->map(function (&$item, $index){
           return [
               'id' => $item->id,
               'title' => $item->preview_label,
           ];
        });
    }
}