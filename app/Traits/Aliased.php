<?php

namespace App\Traits;

trait Aliased{

    public function getAliasComponents()
    {
        return [$this->id];
    }

    public function getAlias(){

        $components = array_filter($this->getAliasComponents(), function($val){
            return !empty($val) || $val === 0;
        });

        $alias = $this->translit(join('_', $components));
        $alias = strtolower($alias);
        $alias = str_replace(" ", '_', $alias);
        $alias = urldecode($alias);
        $alias = htmlentities($alias);

        return $alias;
    }

    private function translit($string){
        return \Transliterator::create('Cyrillic-Latin; Latin-ASCII')->transliterate($string);
    }

}