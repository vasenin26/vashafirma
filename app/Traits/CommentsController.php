<?php

namespace App\Traits;

use Illuminate\Support\Facades\URL;
use Illuminate\Http\Request;

trait CommentsController
{
    private $commentedClass = null;
    private $tplWidget = 'comments/widget';
    protected $routerAlias = null;

    protected function assignAlias($alias)
    {
        $this->routerAlias = $alias;
    }

    protected function assignObject($modelClass)
    {
        $this->commentedClass = $modelClass;
    }

    public function renderComments($object)
    {
        $formAction = URL::route($this->routerAlias . '.comments.add', ['id' => $object->id]);
        $comments = $object->comments;
        $access = $this->getCommentAccess($object);

        return view($this->tplWidget, compact('formAction', 'comments', 'object', 'access'));
    }

    public function getCommentAccess(){
        return true;
    }

    public function addComment(Request $request, $objectId)
    {

        $this->validate($request, [
            'content' => 'required'
        ]);

        $content = $request->post('content');
        $userId = $request->user()->id;

        $object = $this->commentedClass::findOrFail($objectId);

        $object->addComment((int)$userId, $content);

        return redirect()->back();

    }
}