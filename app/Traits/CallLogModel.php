<?php

namespace App\Traits;

use App\EventsLog;

trait CallLogModel
{

    public function addCallEvent($userId, $description, $nextCallDate)
    {
        $event = EventsLog::create([
            'created_by' => $userId,
            'thread_id' => $this->getKey(),
            'class_name' => self::class,
            'description' => $description,
            'next_call_date' => $nextCallDate,
        ]);

        //count comments
        if (array_key_exists('events_log_counter', $this->attributes)) {
            $this->events_log_counter = $this->comments()->count();
        }

        $this->next_call_date = $nextCallDate;

        if ($this->isDirty()) {
            $this->save();
        }

        return $event;
    }

    public function callEvents()
    {
        return $this->hasMany('App\EventsLog', 'thread_id', 'id')
            ->with(['owner'])
            ->where('class_name', self::class)
            ->orderBy('created_at', 'desc');
    }

    public function lastCallEvent()
    {
        return $this->hasOne('App\EventsLog', 'thread_id', 'id')
            ->where('class_name', self::class)
            ->latest();
    }
}