<?php

namespace App\Traits;

use App\Comment;
use \App\Interfaces\BelongsToUser;
use Illuminate\Support\Facades\Schema;

trait CommentsModel
{

    public function addComment($userId, $comment, $parent = null)
    {
        Comment::create([
            'created_by' => $userId,
            'thread_id' => $this->getKey(),
            'class_name' => self::class,
            'parent' => $parent,
            'content' => $comment,
        ]);

        //count comments

        if (array_key_exists('comment_counter', $this->attributes))
        {
            $this->comment_counter = $this->comments()->count();
        }

        //send notify

        /*
        if ($this instanceof BelongsToUser) {
            if ($this->owner->id !== $userId) {
                $this->owner->pushNotify(
                    'comment',
                    self::class . '-' . $this->getKey(),
                    [
                        'id' => $this->getKey(),
                        'url' => $this->getUrl(),
                        'title' => $this->title
                    ]
                );
            }
        }
        */

        if($this->isDirty()){
            $this->save();
        }

    }

    public function comments()
    {

        return $this->hasMany('App\Comment', 'thread_id', 'id')
            ->with(['owner'])
            ->where('class_name', self::class)
            ->orderBy('created_at', 'asc');

    }
}