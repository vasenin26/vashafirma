<?php
/**
 * Created by PhpStorm.
 * User: vasenin26
 * Date: 30.03.2019
 * Time: 1:17
 */

namespace App\Traits;

use App\User;
use App\RateStat;
use Illuminate\Support\Facades\Auth;


trait RatingModel
{

    public function voteState()
    {
        return $this->hasMany('\App\RateStat', 'thread_id', 'id')
            ->where('class_name', static::class);
    }

    public function userVote(){

        $user = Auth::user();

        $userId = $user ? $user->id : null;

        return $this->hasOne('\App\RateStat', 'thread_id', 'id')
            ->where('class_name', static::class)
            ->where('created_by', $userId);
    }

    public function vote(User $user, $rate = 0)
    {

        $rate = $rate < 0 ? -1 : 1;

        //register vote
        $voteState = $this->voteState()
            ->where('created_by', $user->getKey())
            ->first();

        if (empty($voteState)) {
            RateStat::create([
                'created_by' => $user->getKey(),
                'thread_id' => $this->getKey(),
                'class_name' => self::class,
                'vote' => $rate
            ]);
        } else {
            $voteState->vote += $rate;
            if($voteState->vote < 0){
                $voteState->vote = -1;
            }
            if($voteState->vote > 0){
                $voteState->vote = 1;
            }
            $voteState->save();
        }

        //count votes
        $this->attributes['rating'] = $this->voteState->sum('vote');

        return $this;
    }

}