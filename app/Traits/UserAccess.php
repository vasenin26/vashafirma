<?php

namespace App\Traits;

use App\Interfaces\LazyCreation;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

trait UserAccess
{

    /*
     * Some fields can by set in model for manage access to object
     */

    // ownerAccessDelay - delay for closes access for owner
    // accessPermission - required permission for access not owner
    // frozen - if froze then owner can't edit model

    public function isUserAccess()
    {
        $user = Auth::user();

        if (empty($user)) {
            return false;
        }

        if ($this->accessPermission && $user->can($this->accessPermission)) {
            return true;
        }

        if ($user->id === (int)$this->created_by && !$this->frozen) {

            if ($this instanceof LazyCreation) {
                if (!$this->isPublished()) {
                    return true;
                }
            }

            if ($this->ownerAccessDelay) {
                return $this->created_at->greaterThan(Carbon::now()->subSeconds($this->ownerAccessDelay));
            } else {
                return true;
            }

        }

        return false;
    }

    public function accessDelay()
    {
        if(!$this->isUserAccess()){
            return false;
        }

        if(!$this->ownerAccessDelay){
            return false;
        }

        if ($this->accessPermission && Auth::user()->can($this->accessPermission)) {
            return false;
        }

        $diff = Carbon::now()->diff( $this->created_at->addSeconds($this->ownerAccessDelay) );

        $label = [];

        if($diff->i){
            $label[] = $diff->i;
            $label[] = getCase($diff->i, 'минуту', 'минуты', 'минут');
        }

        if($diff->s){
            $label[] = $diff->s;
            $label[] = getCase($diff->s, 'секунду', 'секунды', 'секунд');
        }

        return join (' ', $label);
    }
}