<?php

namespace App;

use App\Interfaces\SelectableI;
use App\Interfaces\BelongsToUser;
use App\Traits\CommentsModel;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Declaration extends Model implements BelongsToUser, SelectableI
{
    use Traits\BelongsToUser;
    use Traits\SelectableModel;
    use CommentsModel;
    use SoftDeletes;

    protected $fillable = [
        'created_by',
        'client_name',
        'client_phone',
        'client_email',
        'is_subscription',
        'comment',
        'display_date',
        'estate_id',
        'broker_id',
    ];

    protected $dates = [
        'display_date',
    ];

    public function __construct(array $attributes = [])
    {
        static::creating(function ($model) {
            if (!\Auth::guest()) {
                $userId = \Auth::user()->id;
            }

            $model->broker_id = $userId ?? 0;
        });

        parent::__construct($attributes);
    }

    public function broker()
    {
        return $this->belongsTo('App\User', 'broker_id', 'id');
    }

    public function estate()
    {
        return $this->belongsTo('App\Estate', 'estate_id', 'id');
    }

    public function getPreviewLabelAttribute(): string
    {
        return $this->attributes['client_name'];
    }

    public function scopeSearch(Builder $builder, $search)
    {
        return $builder->where('client_name', 'like', "%$search%")
            ->orWhere('client_phone', 'like', "%$search%")
            ->orWhere('client_email', 'like', "%$search%")
            ->orWhere('comment', 'like', "%$search%")
            ->orWhereHas('estate', function ($builder) use ($search) {
                $builder->where('title', 'like', "%$search%")
                    ->orWhere('object_title', 'like', "%$search%")
                    ->orWhere('object_addr', 'like', "%$search%")
                    ->orWhereHas('ad', function ($builder) use ($search) {
                        $builder->where('contact_name', 'like', "%$search%")
                            ->orWhere('contact_phone', 'like', "%$search%")
                            ->orWhere('contact_email', 'like', "%$search%")
                            ->orWhere('ad_description', 'like', "%$search%")
                            ->orWhere('region', 'like', "%$search%")
                            ->orWhere('legal_form', 'like', "%$search%")
                            ->orWhere('site_url', 'like', "%$search%")
                            ->orWhere('reason_for_sale', 'like', "%$search%")
                            ->orWhere('cost_structure', 'like', "%$search%")
                            ->orWhere('property', 'like', "%$search%")
                            ->orWhere('means_of_production', 'like', "%$search%")
                            ->orWhere('staff', 'like', "%$search%")
                            ->orWhere('benefits', 'like', "%$search%")
                            ->orWhere('additional_info', 'like', "%$search%");
                    });
            });
    }
}
