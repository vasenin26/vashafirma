<?php


namespace App;

use App\Interfaces\DirectoryI;
use App\Interfaces\BelongsToUser;
use Illuminate\Database\Eloquent\Model;

class Departments extends Model implements DirectoryI, BelongsToUser
{
    use \App\Traits\BelongsToUser;

    protected $table = 'departments';

    protected $fillable = [
      'title',
    ];

    public function jobs()
    {
        return $this->hasMany(Jobs::class, 'department_id', 'id');
    }

    public function getFields(): array
    {
        return [
          'title' => 'Название',
          'created_by' => 'Автор',
          'created_at' => 'Дата создания',
        ];
    }

    public function getOrderColumn(): string
    {
        return 'title';
    }

    public function getValuePreview(string $key)
    {
        $value = $this->getAttribute($key);

        switch ($key) {
            case 'created_by':
                if ($this->owner)
                    $value = $this->owner->name;
                break;
        }

        return $value;
    }

}