<?php

namespace App;

use App\Interfaces\ReachModelI;
use App\Services\ImageGenerator;
use App\Traits\BelongsToUser;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;

class Article extends Model implements ReachModelI
{
    use BelongsToUser;

    protected $fillable = [
        'category_id',
        'title',
        'preview',
        'url',
        'meta_title',
        'description',
        'keywords',
        'text',
        'parent_id',
        'is_published',
    ];

    protected $fileFolder = 'articles/%s/%s';

    protected $imageFolder = null;

    protected $previewFileName = 'preview';

    protected static function boot()
    {
        static::creating(function (Model $model) {
            if (is_null($model->getAttribute('url'))) {
                $model->createUrl();
            }
        });

        static::updating(function (Model $model) {
            if (is_null($model->getAttribute('url'))) {
                $model->createUrl();
            }
        });

        parent::boot();
    }


    public function getIsPublishedAttribute()
    {
        return is_null($this->attributes['deleted_at'] ?? null) && !is_null($this->attributes['published_at'] ?? null);
    }

    public function setIsPublishedAttribute($value)
    {
        $value = (bool)$value;

        if ($this->getIsPublishedAttribute() !== $value) {
            $this->attributes['published_by'] = $value ? \Auth::user()->id : null;
            $this->attributes['published_at'] = $value ? Carbon::now() : null;
        }
    }

    public function category()
    {
        return $this->hasOne(ArticleCategory::class, 'id', 'category_id');
    }

    public function getPreviewAttribute()
    {
        $filePath = null;
        $disk = \Storage::disk();

        $globalPath = $disk->path($this->getFolder() . $this->previewFileName . '.*');
        $files = File::glob($globalPath);

        if (is_array($files)) {
            $filePath = current($files);
            $filePath = str_replace($disk->path(''), '', $filePath);
        }

        return ImageGenerator::create($filePath);
    }

    public function setPreviewAttribute($file)
    {
        $globalPath = \Storage::disk()->path($this->getFolder() . '*');
        $files = File::glob($globalPath);
        File::delete(File::glob($globalPath));

        if ($file instanceof \Illuminate\Http\UploadedFile) {
            $fileName = $this->previewFileName . '.' . $file->guessExtension();

            \Storage::disk()->putFileAs(
                $this->getFolder(),
                $file,
                $fileName
            );
        }
    }

    public function getSimilarArticles($limit = 3)
    {
        return self::limit($limit)
            ->whereNotNull('published_by')
            ->get();
    }

    public function getFolder(): string
    {
        if(is_null($this->imageFolder)){
            $level1 = floor($this->id / 1000);
            $level2 = $this->id % 1000;

            $this->imageFolder = sprintf($this->fileFolder, $level1, $level2);
            $this->imageFolder = rtrim($this->imageFolder, '/') . '/';
        }

        return $this->imageFolder;
    }

    public function createUrl()
    {
        $url = [];

        if ($categoryId = $this->getAttribute('category_id')) {
            $categoryAlias = ArticleCategory::find($categoryId)
                ->alias;

            if ($categoryAlias) {
                $url[] = $categoryAlias;
            }
        }

        $url[] = Str::slug($this->getAttribute('title'));

        array_walk($url, function (&$item) {
            $item = trim($item, '/');
        });

        $this->setAttribute('url', join('/', $url));
    }
}
