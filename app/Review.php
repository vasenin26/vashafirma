<?php

namespace App;
use App\Traits\BelongsToUser;
use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    protected $fillable = [
      'author',
      'content',
    ];
}
