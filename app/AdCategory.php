<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdCategory extends Model
{
    protected $fillable = [
        'ad_id',
        'category_id',
    ];

    public $timestamps = false;

    public function __construct(array $attributes = [])
    {
        static::created(function ($model) {
            EstateCategory::calculate($model->category_id);
        });

        static::deleting(function ($model) {
            EstateCategory::calculate($model->category_id);
        });

        parent::__construct($attributes);
    }

    public function ad()
    {
        return $this->hasOne(Ad::class, 'category_id', 'id');
    }

    public function category()
    {
        return $this->hasOne(EstateCategory::class, 'category_id', 'id');
    }

    public static function erase($adId, $categoryId)
    {
        $links = self::where([
            'ad_id' => $adId,
            'category_id' => $categoryId
        ])->get();

        foreach ($links as $link) {
            $link->fireModelEvent('deleting');
        }

        self::where([
            'ad_id' => $adId,
            'category_id' => $categoryId
        ])->delete();
    }
}
