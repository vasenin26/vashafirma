<?php

namespace App;

use App\Interfaces\BelongsToUser;
use App\Traits\UserAccess;
use Illuminate\Database\Eloquent\Model;

class EstateEvent extends Model implements BelongsToUser
{
    use Traits\BelongsToUser;
    use UserAccess;

    protected $accessPermission = 'manage_estate_event_history';

    protected $fillable = [
        'description',
        'next_call_date',
    ];

    protected $dates = [
        'next_call_date'
    ];
}
