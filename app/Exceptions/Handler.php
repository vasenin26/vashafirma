<?php

namespace App\Exceptions;

use App\Article;
use Illuminate\Support\Facades\DB;
use App\EstateCategory;
use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param \Exception $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Exception $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        if ($exception instanceof ModelNotFoundException) {

            //TODO: move to service provider
            $categories = EstateCategory::orderBy('idx', 'asc')
                ->select('estate_categories.*')
                ->addSelect(
                    DB::raw("(SELECT count(id) FROM ad_categories, ads WHERE category_id = estate_categories.id) as ad_counter")
                )->addSelect(
                    DB::raw("(SELECT min(ads.price) FROM ad_categories, ads WHERE category_id = estate_categories.id) as min_price")
                )
                ->get();

            $articles = Article::whereNOtNull('published_by')
                ->limit(3)
                //->whereCategoryId(env('ARTICLE_CATEGORY', 1))
                ->orderBy('created_at')
                ->get();

            return response()->view('site/special/404', [
                'categories' => $categories,
                'articles' => $articles
            ]);
        }

        return parent::render($request, $exception);
    }
}
