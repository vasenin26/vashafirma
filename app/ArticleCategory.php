<?php

namespace App;

use App\Interfaces\BelongsToUser;
use Illuminate\Database\Eloquent\Model;
use Collective\Html\FormFacade as FormBuilder;

class ArticleCategory extends Model implements BelongsToUser
{
    use \App\Traits\BelongsToUser;

    protected $fillable = [
        'alias',
        'title',
        'template'
    ];

    private const templateTypes = [
        '' => 'По умолчанию',
        'service' => 'Сервис'
    ];

    public $timestamps = true;

    public function getFields(): array
    {
        return [
            'title' => 'Название',
            'alias' => 'Ссылка',
            'template' => [
                'title' => 'Шаблон',
                'field' => function ($value) {
                    return FormBuilder::select('template',
                        self::templateTypes,
                        $value, [
                            'class' => 'form-control'
                        ]);
                }
            ],
            'created_by' => 'Автор',
            'created_at' => 'Дата создания',
        ];
    }

    public static function getTemplateTypes(){
        return self::templateTypes;
    }

    public function getOrderColumn(): string
    {
        return 'title';
    }

    public function getValuePreview(string $key)
    {
        $value = $this->getAttribute($key);

        switch ($key) {
            case 'created_by':
                if ($this->owner)
                    $value = $this->owner->name;
                break;
            case 'template':
                if (array_key_exists($value, self::templateTypes))
                    $value = self::templateTypes[$value];
                break;
        }

        return $value;
    }
}
