<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class CommentAggrigator extends Comment
{
    public function estate()
    {
        return $this->hasOne(Estate::class, 'id', 'thread_id');
    }

    public function declaration()
    {
        return $this->hasOne(Declaration::class, 'id', 'thread_id');
    }

    /**
     * filterByBroker
     * @param Builder $scope
     * @param $brokerId
     */
    public function scopeFilterByBroker(\Illuminate\Database\Eloquent\Builder $scope, $brokerId)
    {
        $scope->leftJoinSub(
            DB::table("estates")
                ->select('estates.id AS estates_id')
                ->where("estates.broker_id", $brokerId),
            'broker_estate',
            function (\Illuminate\Database\Query\JoinClause $builder) {
                $builder->on('comment.thread_id', '=', 'estates_id');
            }
        )->leftJoinSub(
            DB::table("declarations")
                ->select('declarations.id AS declarations_id')
                ->where("declarations.broker_id", $brokerId),
            'broker_declarations',
            function (\Illuminate\Database\Query\JoinClause $builder) {
                $builder->on('comment.thread_id', '=', 'declarations_id');
            }
        )->where(function (\Illuminate\Database\Eloquent\Builder $condition) {
            $condition->whereNotNull('estates_id')
                ->orWhereNotNull('declarations_id');
        });
    }
}
