<?php

namespace App;

use App\Interfaces\BelongsToUser;
use App\Interfaces\DirectoryI;
use \App\Interfaces\NestedStructureI;
use Collective\Html\FormFacade as FormBuilder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class Dictionary extends Model implements DirectoryI, BelongsToUser, NestedStructureI
{

    use \App\Traits\BelongsToUser;

    protected $dictionaryGroup = null;

    protected $fillable = [
        'value',
        'color',
    ];

    public function getFields(): array
    {
        return [
            'value' => 'Значение',
            'color' => [
                'title' => 'Цвет',
                'field' => function ($value) {
                    return view('admin/directories/controls/colorpicker', ['value' => $value]);
                }
            ],
            'created_by' => 'Автор',
            'created_at' => 'Дата создания',
        ];
    }

    public function getOrderColumn(): string
    {
        return 'value';
    }

    public function getValuePreview(string $key)
    {
        $value = $this->getAttribute($key);

        switch ($key) {
            case 'created_by':
                if ($this->owner)
                    $value = $this->owner->name;
                break;
        }

        return $value;
    }

    public function setDictionaryGroup($dictionaryGroup)
    {
        $this->dictionaryGroup = $dictionaryGroup;
    }

    public function getDictionaryGroup()
    {
        return $this->dictionaryGroup;
    }

    private function paginate($perPage = 15, $columns = ['*'], $pageName = 'page', $page = null)
    {
        return self::whereGroup($this->dictionaryGroup)->paginate($perPage, $columns, $pageName, $page);
    }

    public static function updateOrderStructure(array $structure)
    {
        foreach ($structure as $item) {

            $itemModel = self::find($item['id']);

            if (!empty($itemModel)) {
                $itemModel->order = $item['order'];
                $itemModel->save();
            }

        }
    }

    public function scopeSelfOrder(Builder $builder)
    {
        $builder->getQuery()->orders = [];

        return $builder->orderBy('order', 'asc')
            ->orderBy('value', 'asc');
    }
}
