<?php

namespace App\Services;

use Illuminate\Support\Facades\URL;

class RichContent
{

    private $map = [];

    public function __construct($map)
    {
        $this->map = $map;
    }

    public static function create($content)
    {
        $map = json_decode($content, true);

        if (json_last_error() !== JSON_ERROR_NONE) {
            $map = [[
                'idx' => 0,
                'type' => 'text',
                'value' => $content
            ]];
        }

        return new self($map);
    }

    public static function parse($content)
    {
        return self::create($content)->getHtml();
    }

    public function getHtml($limitNodes = 0, $fullLink = '')
    {

        $nodes = [];

        foreach ($this->map as $node) {

            if (!empty($node['value'])) {

                $view = 'services/richContent/node/' . $node['type'];

                switch ($node['type'] ?? '') {

                    case 'text':
                        $node['value'] = strip_tags($node['value']);
                        $node['value'] = htmlspecialchars($node['value']);
                        $node['value'] = preg_replace_callback(
                            '/#[^\s]+/',
                            function ($matches) {
                                $tag = current($matches);
                                $url = URL::route('article.tag', trim($tag, '#'));
                                return "<a href='$url'>$tag</a>";
                            },
                            $node['value']);
                        break;

                    case 'video':
                    case 'image':
                    default:
                        //nothing to do
                        break;
                }

                if ($view) {
                    $nodes[] = view($view, $node);
                }
            }

            if ($fullLink && $limitNodes && count($nodes) >= $limitNodes) {

                $nodes[] = view('services/richContent/node/readFull', compact('fullLink'));
                break;
            }

        }

        return view('services/richContent/layout', ['nodes' => $nodes]);
    }


    public function getText($length = 256)
    {

        $text = [];

        foreach ($this->map as $node) {

            switch ($node['type'] ?? '') {
                case 'text':
                    $text[] = strip_tags($node['value'] ?? '');
                    break;
            }

        }

        $text = join(' ', $text);

        if ($length) {
            $text = mb_substr($text, 0, $length)
                . (mb_strlen($text) > $length ? '...' : '');
        }

        return $text;
    }

}