<?php


namespace App\Service;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Directory
{

    public $libraries = [];

    function __construct()
    {
        $this->libraries = [
            'brokers' => [
                'model' => \App\User::class,
            ],
            'broker_actions' => [
                'title' => 'Статусы действий', //will be show on dictionaries list
                'model' => null //will used All\Dictionary model
            ],
            'comments' => [
                'title' => 'Коментарии',
                'model' => \App\Comment::class,
            ],
            'estate_category' => [
                'title' => 'Категории объектов',
                'model' => \App\EstateCategory::class,
                'interface' => 'nested', //change interface for items list
                'perPage' => null
            ],
            'article_category' => [
                'title' => 'Категории статей',
                'model' => \App\ArticleCategory::class,
                'perPage' => null,
            ],
            'departments' => [
                'title' => 'Отделы',
                'model' => \App\Departments::class,
            ],
            'publication_state' => [
                'title' => 'Статус публикации',
                'model' => null
            ],
            'ad_state' => [
                'title' => 'Статусы объявления',
                'interface' => 'ordered', //change interface for items list
                'model' => null,
                'perPage' => null,
            ],
            'marketing_type' => [
                'title' => 'Маркетинг',
                'model' => null
            ],
            'metro' => [
                'title' => 'Метро',
                'model' => null
            ],
            'customer_source' => [
                'title' => 'Источники клиентов',
                'model' => null
            ],
            'contract_type' => [
                'title' => 'Вид договора',
                'model' => null
            ],
            \App\Estate::class => [
                'condition' => function ($query) { //limit list if user not have permission
                    $query->whereBrokerId(\Auth::user()->id);
                },
            ],
            \App\Declaration::class => [
                'condition' => function ($query) {
                    $query->whereBrokerId(\Auth::user()->id);
                },
            ],
            \App\CustomerCallingHistory::class => [
                'condition' => function ($query) {
                    $query->whereBrokerId(\Auth::user()->id);
                },
            ],
        ];
    }

    public function getLibraryModel($directoryIndex)
    {
        $library = $this->libraries[$directoryIndex] ?? null;

        if (empty($library)) {
            abort(404);
        }

        if (array_key_exists('model', $library)) {
            if (is_null($library['model'])) {
                $model = app(\App\Dictionary::class);
                $model->setDictionaryGroup($directoryIndex);
            } else {
                $model = app($library['model']);
            }
        } else {
            $model = app($directoryIndex);
        }

        return [$library, $model];
    }

    public function getList()
    {
        return array_filter($this->libraries, function ($item) {
            return array_key_exists('title', $item);
        });
    }

    public function getSelectArray($directoryIndex, $valueKey = null)
    {
        $library = null;

        if (array_key_exists($directoryIndex, $this->libraries)) {
            list($library, $model) = $this->getLibraryModel($directoryIndex);
        } else {
            $model = app($directoryIndex);
        }

        $key = $model->getKeyName();

        $query = DB::table($model->getTable());

        if (!is_null($valueKey)) {
            $query->select([$key, $valueKey])
                ->orderBy($valueKey);
        }

        if ($model instanceof \App\Dictionary) {
            $query->where('dictionary_group', $model->getDictionaryGroup());
            $query->orders = [
                [
                    'column' => 'order', 'direction' => 'asc'
                ],
                [
                    'column' => 'value', 'direction' => 'asc'
                ],
            ];
        }

        if ($library && (\Auth::guest() || !\Auth::user()->can('full_dictionary_access'))) {
            if (array_key_exists('condition', $library) && is_callable($library['condition'])) {
                $library['condition']($query);
            }
        }

        $collection = $query->get();
        $list = [];

        if (is_null($valueKey)) {
            $list = $collection->toArray();
        } else {
            foreach ($collection as $item) {
                $list[$item->$key] = $item->$valueKey;
            }
        }

        return $list;
    }

    public function getModel($directoryIndex)
    {
        list($library, $model) = $this->getLibraryModel($directoryIndex);

        return $model;
    }

}