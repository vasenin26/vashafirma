<?php

namespace App\Services;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;

class ImageGenerator
{

    private $imagePath;
    private $originImagePath;
    private $key = '123456789';
    private $notFoundImage = 'image-not-found.png';

    private $availableOptions = ['w', 'h', 'crop'];

    private static $checkMark = [];

    public static function create($imagePath)
    {
        return new self($imagePath);
    }

    public function __construct($imagePath = null)
    {

        $this->originImagePath = $imagePath;

        if (!is_null($imagePath) && Storage::exists($imagePath)) {
            $this->imagePath = $imagePath;
        } else {
            $this->imagePath = $this->notFoundImage;
        }

    }

    public function __toString()
    {
        return (string)Storage::url($this->originImagePath);
    }

    /**
     * Resize image and return relative HTTP path
     * @param $width
     * @param $height
     * @param bool $crop
     * @return string
     */
    public function getSize($width, $height, $crop = false)
    {

        $options = $this->filterOptions(['w' => $width, 'h' => $height, 'crop' => $crop]);

        $imagePath = $this->generatePath($options);

        if ($this->exist($imagePath)) {
            return Storage::url($imagePath);
        }

        $options['key'] = $this->generateHash($options);
        $options['path'] = $this->imagePath;
        $imagePath = $this->generateRequestUri($options);

        return $imagePath;
    }

    public function getWidth($width){

        //calculate size
        $source = Storage::disk()->path($this->imagePath);
        $size = getimagesize($source);

        if($width > $size[0]){
            $width = $size[0];
        }

        $height = $size[1] / ($size[0] / $width);

        return $this->getSize($width, $height, false);
    }

    public function getFileName()
    {
        return \File::basename($this->originImagePath);
    }

    public function getImageUrl($options)
    {
        return Storage::url($this->generateImage($options));
    }

    public function generateImage($options)
    {
        $imagePath = $this->generatePath($options);

        $source = Storage::disk()->path($this->imagePath);
        $destination = Storage::disk()->path($imagePath);

        if (!Storage::exists($this->imagePath)) {
            return false;
        }

        if ($this->exist($imagePath)) {
            return $imagePath;
        }

        $size = getimagesize($source);

        switch (strtolower($size['mime'])) {
            case 'image/gif':
                $srcImg = imagecreatefromgif($source);
                break;

            case 'image/jpeg':
                $srcImg = imagecreatefromjpeg($source);
                break;

            case 'image/png':
                $srcImg = imagecreatefrompng($source);
                break;
        }

        $options['h'] = (int)$options['h'];
        $options['w'] = (int)$options['w'];

        $cropWidth = $options['w'];
        $cropHeight = $options['h'];

        $cropHeight = $cropHeight * ($size[0] / $cropWidth);
        $cropWidth = $size[0];

        if ($cropHeight > $size[1]) {
            $cropWidth = $cropWidth * ($size[1] / $cropHeight);
            $cropHeight = $size[1];
        }

        $srcX = ($size[0] - $cropWidth) / 2;
        $srcY = ($size[1] - $cropHeight) / 2;

        $outImage = imagecreatetruecolor($options['w'], $options['h']);

        imagecopyresized($outImage, $srcImg, 0, 0, $srcX, $srcY, $options['w'], $options['h'], $cropWidth, $cropHeight);
        imagejpeg($outImage, $destination, 100);

        @imagedestroy($outImage);
        @imagedestroy($srcImg);

        /*
        try {
            ImageCache::create(['path' => $imagePath, 'created_at' => Carbon::now()]);
        } catch (\Illuminate\Database\QueryException $e) {

            $imageCache = ImageCache::find($imagePath);
            if ($imageCache) {
                $imageCache->setAttribute('created_at', Carbon::now())
                    ->save();
            } else {
                Storage::delete($imagePath);
                dd();
            }

        }
        */

        return $imagePath;
    }

    private function generatePath(array $options)
    {

        $options = $this->filterOptions($options);

        ksort($options);
        array_walk($options, function (&$val, $key) {
            $val = $key . '-' . $val;
        });

        if (!Storage::exists($this->imagePath)) {
            return null;
        }

        $pathInfo = pathinfo($this->imagePath);

        array_unshift($options, $pathInfo['filename']);

        return $pathInfo['dirname'] . '/' . join('_', $options) . '.jpg';

    }

    private function generateRequestUri($options)
    {

        array_walk($options, function (&$val, $key) {
            $val = $key . '=' . $val;
        });

        return '/image/?' . join('&', $options);
    }

    public function exist($path)
    {
        if (Storage::exists($path)) {

            /*
            if (!in_array($path, self::$checkMark)) {

                $imageCache = ImageCache::find($path);
                if ($imageCache) {

                    if ($imageCache->created_at->lessThan(Carbon::now()->subHour())) {
                        //need delay to update timestamp
                        $imageCache->setAttribute('created_at', Carbon::now())
                            ->save();
                    }

                    self::$checkMark[] = $path;
                }
            }
            */

            return true;

        }

        return false;
    }

    public function checkOptions($options)
    {
        $filtratedOptions = $this->filterHashOptions($options);

        return Hash::check(serialize($filtratedOptions), $options['key']);
    }

    private function generateHash($options)
    {

        $filtratedOptions = $this->filterHashOptions($options);

        return Hash::make(serialize($filtratedOptions));
    }

    private function filterHashOptions($options)
    {

        $hashable = ['w', 'h', 'crop', 'key'];

        $options['key'] = $this->key;

        $filtratedOptions = [];

        foreach ($hashable as $key) {
            if (array_key_exists($key, $options) && !empty($options[$key])) {
                $filtratedOptions[$key] = is_numeric($options[$key]) ? (int)$options[$key] : $options[$key];
            }
        }

        ksort($filtratedOptions);

        return $filtratedOptions;
    }

    private function filterOptions($options)
    {
        $filtrated = [];

        foreach ($this->availableOptions as $key) {
            if (array_key_exists($key, $options) && !empty($options[$key])) {
                $filtrated[$key] = is_numeric($options[$key]) ? (int)$options[$key] : $options[$key];
            }
        }

        ksort($filtrated);

        return $filtrated;
    }

    static function flush()
    {

        $records = ImageCache::where('created_at', '<', Carbon::now()->subDay(1))
            ->limit(10)
            ->get();


        foreach ($records as $record) {
            if (Storage::exists($record->path)) {
                (new self($record->path))->remove();
            } else {
                $record->delete();
            }
        }

    }

    public function remove()
    {
        Storage::delete($this->imagePath);
        ImageCache::find($this->imagePath)->delete();
    }

}