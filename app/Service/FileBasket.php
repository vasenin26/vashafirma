<?php

namespace App\Services;

use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use \Illuminate\Http\File;
use Illuminate\Http\UploadedFile;

class FileBasket
{

    private $basketId;
    private $filesFolder;

    static function create($basketId = null)
    {
        if (empty($basketId)) {
            $basketId = Str::random();
        }

        return new self($basketId);
    }

    public function __construct($basketId)
    {
        $this->basketId = $basketId;
        $this->filesFolder = 'file_basket/' . $this->basketId . '/';
    }

    public function getBasketId()
    {
        return $this->basketId;
    }

    public function getFile($fileName)
    {
        foreach (Storage::disk()->allFiles($this->filesFolder) as $file) {

            $fileName = \File::name($file);

            if (preg_match('/(_origin)$/', $fileName)) {
                $filePath = Storage::disk()->path($file);
                return new File($filePath);
            }

        }

        return null;
    }

    public function putFile($filePath)
    {
        //WARNING: wrong code
        $file = new File($filePath);

        return $this->putUploadedFile($file);
    }

    public function putUploadedFile(UploadedFile $uploadedFile)
    {
        $fileExtension = $uploadedFile->getClientOriginalExtension();

        if (!$this->checkAvailableExtensions($fileExtension)) {
            abort(412);
        }

        $newFileName = $this->generateNewFileName($fileExtension);

        $uploadedFile->storeAs($this->filesFolder, $newFileName);

        return $newFileName;
    }

    public function deleteFile($fileName)
    {
        $filePath = $this->filesFolder . '/' . $fileName;

        $disk = Storage::disk();

        if ($disk->exists($filePath)) {
            return $disk->delete($filePath);
        }

        return true;
    }

    public function getAllFiles()
    {

        $basketFiles = [];

        foreach (Storage::disk()->allFiles($this->filesFolder) as $file) {

            $fileName = \File::name($file);

            if (preg_match('/(_origin)$/', $fileName)) {
                $basketFiles[] = $file;
            }

        }

        return $basketFiles;
    }

    public function getAllImages($imagesModel = null, $imageWith = 300, $imageHeight = 300)
    {
        $images = [];

        if (!is_null($imagesModel)) {
            foreach ($imagesModel->images as $image) {
                $images[] = [
                    'url' => $image->getSize($imageWith, $imageHeight),
                    'file' => $image->getFileName(),
                    'attached' => true
                ];
            }
        }

        foreach ($this->getAllFiles() as $file) {
            $image = new ImageGenerator($file);

            $images[] = [
                'url' => $image->getSize($imageWith, $imageHeight),
                'file' => $image->getFileName(),
                'attached' => false
            ];
        }

        return $images;
    }

    private function generateNewFileName($extension)
    {
        return Str::random() . '_origin.' . $extension;
    }

    private function checkAvailableExtensions($extension)
    {
        $extension = mb_strtolower($extension);
        return in_array($extension, ['pdf', 'jpeg', 'jpg', 'png', 'gif', 'mp4']);
    }
}