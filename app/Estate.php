<?php

namespace App;

use App\Interfaces\SelectableI;
use App\Traits\CommentsModel;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Estate extends Model implements SelectableI
{
    use CommentsModel;
    use Traits\SelectableModel;

    protected $fillable = [
        'title',

        'broker_id',
        'estate_state',
        'publication_state',
        'marketing_type',
        'first_published_at',
        'is_archived',

        'contract_type',
        'contract_signing_at',
        'contract_expiring_at',

        'object_title',
        'object_addr',
        'information_source_link',
    ];

    protected $dates = [
        'first_published_at',
        'contract_signing_at',
        'contract_expiring_at',
    ];

    public static function boot()
    {
        parent::boot();

        static::deleting(function ($model) {
            $model->ad->delete();
            $model->declarations()->delete();
            $model->events()->delete();
        });
    }

    public function getPreviewLabelAttribute(): string
    {
        return $this->attributes['title'];
    }

    public function ad()
    {
        return $this->hasOne('App\Ad', 'estate_id', 'id');
    }

    public function declarations()
    {
        return $this->hasMany('App\Declaration', 'estate_id', 'id');
    }

    public function broker()
    {
        return $this->hasOne('App\User', 'id', 'broker_id');
    }

    public function marketing()
    {
        return $this->hasOne('App\Dictionary', 'id', 'marketing_type');
    }

    public function contract()
    {
        return $this->hasOne('App\Dictionary', 'id', 'contract_type');
    }


    public function events()
    {
        return $this->hasMany('App\EstateEvent', 'estate_id', 'id');
    }

    public function pushEvent(array $details)
    {
        $event = new EstateEvent();

        $event->estate_id = $this->id;
        $event->created_by = Auth::user()->id;

        $event->fill($details);

        $event->save();

        return $event;
    }

    public function scopeSearch(Builder $builder, $search)
    {
        return $builder->where('title', 'like', "%$search%")
            ->orWhere('object_title', 'like', "%$search%")
            ->orWhere('object_addr', 'like', "%$search%")
            ->orWhereHas('ad', function ($builder) use ($search){
                $builder->where('contact_name', 'like', "%$search%")
                    ->orWhere('contact_phone', 'like', "%$search%")
                    ->orWhere('contact_email', 'like', "%$search%")
                    ->orWhere('ad_description', 'like', "%$search%")
                    ->orWhere('region', 'like', "%$search%")
                    ->orWhere('legal_form', 'like', "%$search%")
                    ->orWhere('site_url', 'like', "%$search%")
                    ->orWhere('reason_for_sale', 'like', "%$search%")
                    ->orWhere('cost_structure', 'like', "%$search%")
                    ->orWhere('property', 'like', "%$search%")
                    ->orWhere('means_of_production', 'like', "%$search%")
                    ->orWhere('staff', 'like', "%$search%")
                    ->orWhere('benefits', 'like', "%$search%")
                    ->orWhere('additional_info', 'like', "%$search%");
            })
            ->orWhereHas('declarations', function ($builder) use ($search){
                $builder->where('client_name', 'like', "%$search%")
                    ->orWhere('client_phone', 'like', "%$search%")
                    ->orWhere('client_email', 'like', "%$search%")
                    ->orWhere('comment', 'like', "%$search%");
            });
    }
}
