<?php


namespace App\Interfaces;


interface DirectoryI
{
    public function getFields(): array;

    public function getOrderColumn();

    public function getValuePreview(string $key);

    public function isFillable($key);
}