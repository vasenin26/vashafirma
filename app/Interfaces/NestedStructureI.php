<?php


namespace App\Interfaces;


interface NestedStructureI
{
    public static function updateOrderStructure(array $structure);
}