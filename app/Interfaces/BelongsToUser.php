<?php

namespace App\Interfaces;

interface BelongsToUser
{
    public function owner();
}