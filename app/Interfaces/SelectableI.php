<?php
/**
 * Filename: SelectableI.php
 * Creator: Vasenin Leonid {leonid.vasenin@gmail.com}
 * Date: 17.11.2019
 */

namespace App\Interfaces;


interface SelectableI
{
    public function scopeGetSelectableArray(\Illuminate\Database\Eloquent\Builder $scope): \Illuminate\Support\Collection;

    public function getPreviewLabelAttribute(): string;
}