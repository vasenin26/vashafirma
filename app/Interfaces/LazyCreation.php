<?php
/**
 * Created by PhpStorm.
 * User: vasenin26
 * Date: 06.05.2019
 * Time: 23:21
 */

namespace App\Interfaces;


interface LazyCreation
{
    public function isPublished(): bool;
}