<?php


namespace App\Interfaces;


interface ReachModelI
{
    public function getFolder() : string;
}