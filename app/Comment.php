<?php

namespace App;

use App\Interfaces\BelongsToUser;
use App\Interfaces\DirectoryI;
use App\Traits\UserAccess;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class Comment extends Model implements DirectoryI, BelongsToUser
{
    use \App\Traits\UserAccess;
    use \App\Traits\BelongsToUser;
    use SoftDeletes;

    protected $table = 'comment';

    protected $accessPermission = ['manage_comments'];
    protected $ownerAccessDelay = 300;

    protected $fillable = [
        'thread_id',
        'class_name',
        'created_by',
        'parent',
        'content',
    ];

    public function isOwner()
    {
        return Auth::user() && (int)$this->created_by === (int)Auth::user()->id;
    }

    public function getFields(): array
    {
        return [
            'thread_id' => 'Ветка',
            'class_name' => 'Класс',
            'created_by' => 'Автор',
            'created_at' => 'Дата создания',
            'content' => 'Текст',
            'rating' => 'Рейтинг',
        ];
    }

    public function getOrderColumn(): string
    {
        return 'created_at';
    }

    public function getValuePreview(string $key)
    {
        $value = $this->getAttribute($key);

        switch ($key) {
            case 'content':
                $value = substr($value, 0, 100);
                break;
            case 'created_by':
                if ($this->owner)
                    $value = $this->owner->name;
                break;
        }

        return $value;
    }
}
