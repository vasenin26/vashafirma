<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventsLog extends Model
{
    use \App\Traits\UserAccess;
    use \App\Traits\BelongsToUser;
    use \Illuminate\Database\Eloquent\SoftDeletes;

    protected $table = 'call_events_logs';

    protected $accessPermission = ['manage_events_logs'];
    protected $ownerAccessDelay = 300;

    protected $fillable = [
        'thread_id',
        'class_name',
        'created_by',
        'description',
        'next_call_date',
    ];

    protected $dates = ['next_call_date'];
}
