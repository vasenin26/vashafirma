<?php

namespace App\Http\Controllers;

use App\Article;
use App\ArticleCategory;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;

class ArticleController extends Controller
{
    public function index()
    {
        $articles = Article::whereNotNull('published_by')
            //->whereCategoryId(env('ARTICLE_CATEGORY', 1))
            ->paginate(12);

        return view('site/articles/list', [
            'articles' => $articles
        ]);
    }

    public function show($alias)
    {
        $template = 'site/articles/article';

        $article = Article::whereUrl($alias)->with(['category'])
            ->whereNotNull('published_by')
            ->firstOrFail();

        /**
         * @var ArticleCategory;
         */
        $category = $article->category;

        if ($category && $category->template) {
            $template = 'site/articles/templates/' . $category->template;
        }

        return view($template, [
            'article' => $article
        ]);
    }
}
