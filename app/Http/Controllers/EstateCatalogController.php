<?php

namespace App\Http\Controllers;

use App\Ad;
use App\Article;
use App\EstateCategory;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class EstateCatalogController extends Controller
{
    private $orders = [
        'price' => 'price',
        'profit' => 'profit',
        'created' => 'created_at',
    ];

    public function index()
    {
        $categories = EstateCategory::orderBy('idx', 'asc')
            ->select('estate_categories.*')
            ->isPublished()
            ->whereNull('estate_categories.parent_id')
            ->get();

        $ads = Ad::with(['estate'])
            ->isPublished()
            ->orderBy('created_at', 'desc')
            ->limit(12)
            ->get();

        $premium = Ad::with(['estate', 'state'])
            ->isPublished()
            ->isPremium()
            ->orderBy('created_at')
            ->limit(6)
            ->get();

        $articles = Article::whereNOtNull('published_by')
            ->limit(3)
            ->orderBy('created_at')
            ->get();

        return view('site/home/index', [
            'categories' => $categories,
            'ads' => $ads,
            'articles' => $articles,
            'premium' => $premium
        ]);
    }

    public function search(Request $request)
    {
        if ($request->filled('category_id')) {
            $categoryId = $request->input('category_id');
            $category = EstateCategory::findOrFail($categoryId);
            return redirect()->route('category.show', [
                    'alias' => $category->alias
                ] + $request->all());
        }

        $searchFormTabIdx = 0;

        $ads = Ad::with(['estate'])
            ->whereHas('estate', function (Builder $builder) {
                return $builder->whereIsArchived(0);
            })
            ->orderBy('created_at');

        $searchFormTabIdx = $this->filterAd($ads, $request);

        return view('site/ad/search/result', [
            'ads' => $ads->paginate(12),
            'searchFormTabIdx' => $searchFormTabIdx,
            'filters' => $request->all(),
        ]);
    }

    public function showAd($adId)
    {
        $ad = Ad::with(['estate'])
            ->isPublished()
            ->findOrFail($adId);

        return view('site/ad/show', [
            'ad' => $ad
        ]);
    }

    public function showCategory(Request $request, $alias)
    {
        $filterValues = $request->all();

        $category = EstateCategory::whereAlias($alias)->firstOrFail();
        $children = EstateCategory::getChildren($category->id);
        $children[] = $category->id;

        if ($request->exists('category_id')) {
            if ($request->filled('category_id')) {
                $categoryId = $request->input('category_id');
                if ($category->id != $categoryId) {
                    $category = EstateCategory::findOrFail($categoryId);
                    return redirect()->route('category.show', [
                            'alias' => $category->alias
                        ] + $request->all());
                }
            } else {
                return redirect()->route('ad.search');
            }
        }

        if ($category) {
            $filterValues['category_id'] = $category->id;
        }

        $ads = Ad::with(['estate'])
            ->orderBy('created_at')
            ->join('ad_categories', 'ad_id', '=', 'ads.id')
            ->whereIn('ad_categories.category_id', $children)
            ->groupBy('ads.id');

        $searchFormTabIdx = $this->filterAd($ads, $request);

        $children = EstateCategory::orderBy('idx', 'asc')
            ->select('estate_categories.*')
            ->whereParentId($category->id)
            ->isPublished()
            ->get();

        return view('site/categories/show', [
            'ads' => $ads->paginate(12),
            'category' => $category,
            'children' => $children,
            'searchFormTabIdx' => $searchFormTabIdx,
            'filters' => $filterValues
        ]);
    }

    public function sold(Request $request)
    {
        $ads = Ad::with(['estate'])
            ->whereHas('estate', function (Builder $builder) {
                return $builder->whereIsArchived(1);
            })
            //->whereAdState(ENV('SOLD_ESTATE_STATE_ID', 0))
            ->orderBy('created_at');

        if ($request->filled('order')) {
            $order = $request->input('order');

            if (array_key_exists($order, $this->orders)) {
                $ads->orders = [];
                $ads->orderBy($this->orders[$order]);
            }
        }

        return view('site/ad/sold', [
            'ads' => $ads->paginate(12)
        ]);
    }

    private function filterAd($ads, Request $request)
    {
        $searchFormTabIdx = 0;

        $filters = [
            'price_from' => function (Builder $ads, $value) {
                $ads->where('price', '>=', $value);
            },
            'price_to' => function (Builder $ads, $value) {
                $ads->where('price', '<=', $value);
            },
            'profit_from' => function (Builder $ads, $value) {
                $ads->where('revenue', '>=', $value);
            },
            's' => function (Builder $ads, $value) {
                $ads->whereHas('estate', function (Builder $ads) use ($value) {
                    $value = "%$value%";
                    $ads->where('estates.object_title', 'like', $value)
                        ->orWhere('estates.object_addr', 'like', $value)
                        ->orWhere('estates.title', 'like', $value);
                });
            }
        ];

        foreach ($filters as $filter => $condition) {
            if ($request->filled($filter)) {
                if (is_callable($condition)) {
                    $condition($ads, $request->input($filter));
                } else {
                    $ads->where($condition, $request->input($filter));
                }
            }
        }

        if ($request->filled('order')) {
            $order = $request->input('order');

            if (array_key_exists($order, $this->orders)) {
                $ads->getQuery()->orders = [];
                $ads->orderBy($this->orders[$order], 'asc');
            }
        }

        if ($request->has('s')) {
            $searchFormTabIdx = 1;
        }

        return $searchFormTabIdx;
    }
}
