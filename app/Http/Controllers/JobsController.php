<?php

namespace App\Http\Controllers;

use App\Departments;
use App\Jobs;
use Illuminate\Database\Eloquent\Builder;

class JobsController extends Controller
{
    public function index()
    {
        $departments = Departments::with(['jobs'])
            ->whereHas('jobs', function (Builder $builder){
                $builder->whereNotNull('published_at');
            })
            ->get();

        return view('site/jobs/list', [
          'departments' => $departments,
        ]);
    }

    public function show($id)
    {
        $job = Jobs::findOrFail($id);

        return view('site/jobs/job', [
          'job' => $job
        ]);
    }
}
