<?php

namespace App\Http\Controllers\Admin;

use App\Service\Directory;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use \DirectoryService;

class DirectoryController extends Controller
{

    private const PER_PAGE_DEFAULT = 20;

    public function __construct()
    {
        $this->middleware('permission:manage_dictionary');
    }

    public static function route()
    {
        \Route::get('directory', 'DirectoryController@index')->name('directory.index');
        \Route::get('directory/{index}', 'DirectoryController@show')->name('directory.show');
        \Route::put('directory/{index}/nested', 'DirectoryController@updateNestedList')->name('directory.nested.update');

        \Route::get('directory/{index}/item/create', 'DirectoryController@createItem')->name('directory.item.create');
        \Route::post('directory/{index}/item/create', 'DirectoryController@storeItem')->name('directory.item.store');
        \Route::put('directory/{index}/item/{id}', 'DirectoryController@updateItem')->name('directory.item.update');
        \Route::get('directory/{index}/item/{id}/delete', 'DirectoryController@deleteItem')->name('directory.item.delete');
    }

    public function index()
    {
        $list = DirectoryService::getList();

        return view('/admin/directories/list', [
            'libraries' => $list
        ]);
    }

    public function show($directoryIndex)
    {
        list($library, $model) = DirectoryService::getLibraryModel($directoryIndex);

        $items = null;

        $orderColumn = $model->getOrderColumn();
        $itemsQuery = $model::orderBy($orderColumn, 'asc');

        if ($model instanceof \App\Dictionary) {
            $itemsQuery->whereDictionaryGroup($model->getDictionaryGroup())
                ->selfOrder();
        }

        if (array_key_exists('perPage', $library) && is_null($library['perPage'])) {
            $items = $itemsQuery->get();
        } else {
            $items = $itemsQuery->paginate($library['perPage'] ?? self::PER_PAGE_DEFAULT);
        }

        $cols = $model->getFields();
        array_walk($cols, function (&$item) {
            $item = is_array($item) ? $item['title'] : $item;
        });

        return view('/admin/directories/content', [
            'library' => $library,
            'directoryIndex' => $directoryIndex,
            'model' => $model,
            'cols' => $cols,
            'items' => $items,
            'type' => $library['interface'] ?? 'list',
        ]);
    }

    public function item($directoryIndex, $itemId, Request $request)
    {
        list($library, $model) = DirectoryService::getLibraryModel($directoryIndex);

        return view('/admin/directories/item/edit', [
            'library' => $library,
            'directoryIndex' => $directoryIndex,
            'item' => $model::findOrFail($itemId),
            'request' => $request
        ]);
    }

    public function createItem($directoryIndex)
    {
        list($library, $model) = DirectoryService::getLibraryModel($directoryIndex);

        return view('/admin/directories/item/create', [
            'library' => $library,
            'directoryIndex' => $directoryIndex,
            'item' => $model,
        ]);
    }

    public function storeItem($directoryIndex, Request $request)
    {
        list($library, $model) = DirectoryService::getLibraryModel($directoryIndex);

        $item = new $model();
        $item->fill($request->all());

        if ($model instanceof \App\Interfaces\BelongsToUser) {
            $item->created_by = Auth::user()->id;
        }

        if ($model instanceof \App\Dictionary) {
            $item->dictionary_group = $model->getDictionaryGroup();
        }

        $item->save();

        return redirect()
            ->route('admin.directory.show', $directoryIndex)
            ->with('newItemId', $item->id)
            ->with('success', 1);
    }

    public function updateItem($directoryIndex, $itemId, Request $request)
    {
        list($library, $model) = DirectoryService::getLibraryModel($directoryIndex);

        $model::findOrFail($itemId)->update($request->all());

        if ($request->filled('back')) {
            return redirect($request->input('back'));
        }

        return redirect()->back()
            ->with('success', 1);
    }

    public function deleteItem($directoryIndex, $itemId, Request $request)
    {
        list($library, $model) = DirectoryService::getLibraryModel($directoryIndex);

        $model = $model::findOrFail($itemId);

        if ($model->special_key) {
            abort(403);
        }

        $model->delete();

        return redirect()->back()
            ->with('success', 1);
    }

    /* nested structure */

    public function updateNestedList($directoryIndex, Request $request)
    {
        list($library, $model) = DirectoryService::getLibraryModel($directoryIndex);

        if (!$model instanceof \App\Interfaces\NestedStructureI) {
            return abort(412);
        }

        if ($request->has('nested_structure')) {
            $structure = json_decode($request->input('nested_structure'), true);

            if (array_key_exists('order', $structure) && is_array($structure['order'])) {
                $model::updateOrderStructure($structure['order']);
            }

            if (!empty($structure['removed']) && is_array($structure['removed'])) {
                $model::whereId($structure['removed'])->delete();
            }
        }

        return redirect()->back()->with('success', 1);
    }
}
