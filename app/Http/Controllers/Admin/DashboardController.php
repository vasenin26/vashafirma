<?php

namespace App\Http\Controllers\Admin;

use App\CommentAggrigator;
use App\CustomerCallingHistory;
use App\Declaration;
use App\Estate;
use App\FormReport;
use App\Http\Controllers\Controller;
use App\PlaneHistory;
use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public static function route()
    {
        \Route::get('/', 'DashboardController@index')->name('dashboard');
        \Route::get('/search', 'DashboardController@search')->name('dashboard.search');
        \Route::get('/my-objects', 'DashboardController@brokerObjects')->name('dashboard.my-objects');
        \Route::get('/broker-planes', 'DashboardController@brokerPlanes')->name('dashboard.broker-planes');
    }

    public function index(Request $request)
    {
        $meta = [
            'title' => 'Панель администратора',
        ];

        $userId = null;
        if (!Auth::user()->can('user_data_access')) {
            $userId = Auth::user()->id;
        } else {
            if ($request->filled('broker_id')) {
                $userId = $request->input('broker_id');
            }
        }

        $ownerFilter = function (Builder $builder, $ownerColumn = 'created_by', $owner = null) use ($userId) {
            $userId = $owner ?: $userId;

            if ($userId) {
                $builder->where($ownerColumn, $userId);
            }

            return $builder;
        };

        $declarationsToday = $ownerFilter(Declaration::whereDisplayDate(Carbon::today()), 'broker_id')->count();

        $callsToday = $ownerFilter(CustomerCallingHistory::orderBy('id'), 'broker_id')->whereNextCallDate(Carbon::today())
            ->count();

        $estateToday = $ownerFilter(Estate::orderBy('id'), 'broker_id')->whereHas('events', function (Builder $events) {
            $events->whereNextCallDate(Carbon::today())->groupBy('estate_id');
        })->count();

        $unreadFormCounter = null;

        if (Auth::user()->can('mancage_request_form')) {
            $unreadFormCounter = FormReport::whereNull('opened_by')->count();
        }

        $brokerPlane = null;

        if (Auth::user()->can('personal_planning')) {
            $brokerPlane = PlaneHistory::whereBrokerId(Auth::user()->id)
                ->with(['action', 'estate', 'declaration', 'customer_history'])
                ->get();
        }

        return view('admin/dashboard/main', [
            'ownerFilter' => $ownerFilter,
            'addOwnerKey' => function ($params) use ($userId) {
                if ($userId) $params['broker_id'] = $userId;

                return $params;
            },
            'declarationsToday' => $declarationsToday,
            'estateToday' => $estateToday,
            'callsToday' => $callsToday,
            //'brokerObjectComments' => $this->getBrokerObjectComments($userId),
            'unreadFormCounter' => $unreadFormCounter,
            'brokerPlane' => $brokerPlane,
            'users' => User::whereStatus('active')->get()
        ], $meta);
    }

    public function search(Request $request)
    {
        $search = $request->input('s');

        $result = [
            'estates' => null,
            'declarations' => null,
            'call_history' => null,
        ];

        if (!empty($search)) {
            $result = [
                'estates' => Estate::search($search)->with(['broker', 'marketing', 'contract'])->get(),
                'declarations' => Declaration::search($search)->with(['owner', 'broker', 'estate'])->get(),
                'call_history' => CustomerCallingHistory::search($search)->with(['broker', 'source'])->get()
            ];
        }

        return view('admin/dashboard/search', $result);
    }

    public function brokerPlanes(Request $request)
    {
        $planes = PlaneHistory::with(['action', 'estate', 'declaration', 'customer_history'])
            ->orderBy('action_at', 'desc')
            ->orderBy('created_at', 'desc');

        if ($request->filled('broker_id')) {
            $planes->whereBrokerId($request->input('broker_id'));
        }

        if ($request->filled('period')) {
            switch ($request->input('period')) {
                case 'today':
                    $planes->where('action_at', '=', Carbon::today());
                    break;
                case 'week':
                    $planes->whereBetween('action_at', [Carbon::now(), Carbon::today()->addWeek(1)]);
                    break;
                case '2weeks':
                    $planes->whereBetween('action_at', [Carbon::now(), Carbon::today()->addWeek(2)]);
                    break;
                case 'month':
                    $planes->whereBetween('action_at', [Carbon::now(), Carbon::today()->addMonth(1)]);
                    break;
            }
        }

        return view('admin/dashboard/list', [
            'planes' => $planes->paginate(20)
        ]);
    }

    public function brokerObjects(Request $request)
    {
        $sourceClass = null;
        $sources = [
            'declaration' => Declaration::class,
            'estate' => Estate::class,
            'calls' => CustomerCallingHistory::class
        ];

        if ($request->filled('type')) {
            $sourceType = $request->input('type');
            if (array_key_exists($sourceType, $sources)) {
                $sourceClass = $sources[$sourceType];
            }
        }

        if (is_null($sourceClass)) {
            return [];
        }

        return $sourceClass::whereBrokerId(Auth::user()->id)->getSelectableArray();
    }

    private function getBrokerObjectComments($brokerId = null)
    {
        $builder = CommentAggrigator::whereIn('class_name', ['App\\Estate', 'App\\Declaration'])
            ->with(['owner', 'estate', 'declaration']);

        if ($brokerId) {
            $builder->filterByBroker($brokerId);
        }

        return $builder->limit(5)->get();
    }

    private function getStatsByBroker($userId): array
    {
        $result = [];

        $declarationsToday = Declaration::whereDisplayDate(Carbon::today())->count();

        $callsToday = CustomerCallingHistory::orderBy('id')->whereHas('callEvents', function (Builder $events) {
            $events->latest()->whereNextCallDate(Carbon::today())->groupBy('thread_id');
        })->count();

        $estateToday = Estate::orderBy('id')->whereHas('events', function (Builder $events) {
            $events->whereNextCallDate(Carbon::today())->groupBy('estate_id');
        })->count();

        return $result;
    }
}
