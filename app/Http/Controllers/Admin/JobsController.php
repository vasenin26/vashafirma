<?php


namespace App\Http\Controllers\Admin;

use App\Jobs;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class JobsController extends Controller
{
    public function index()
    {
        $jobs = Jobs::paginate(20);

        return view('admin/jobs/list', [
          'jobs' => $jobs,
        ]);
    }

    public function create(Request $request)
    {
        return view('admin/jobs/create', [
          'jobs' => $request
        ]);
    }

    public function edit($id)
    {
        $jobs = Jobs::findOrFail($id);

        return view('admin/jobs/edit', [
          'jobs' => $jobs
        ]);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
          'department_id' => 'required',
          'job_name' => 'required',
          'job_description' => 'required',
        ]);

        $jobs = Jobs::create($request->all());
        return redirect()->route('admin.jobs.index')
          ->with('status', 'Запись успешно обновлена')
          ->with('new_jobs_id', $jobs->id);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
          'department_id' => 'required',
          'job_name' => 'required',
          'job_description' => 'required',
        ]);

        $jobs = Jobs::findOrFail($id);

        $jobs->update($request->all());

        return redirect()->route('admin.jobs.index')
          ->with('status', 'Запись успешно обновлена')
          ->with('new_jobs_id', $jobs->id);
    }

    public function destroy($id)
    {
      Jobs::findOrFail($id)->delete();

      return redirect()->route('admin.jobs.index')
          ->with('status', 'Запись успешло удалена');
    }
}
