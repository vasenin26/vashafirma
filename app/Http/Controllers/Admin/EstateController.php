<?php

namespace App\Http\Controllers\Admin;

use App\Ad;
use App\Estate;
use App\EstateCategory;
use App\Services\FileBasket;
use App\Traits\CommentsController;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Builder;

class EstateController extends Controller
{

    use CommentsController;

    public static function route(){
        \Route::resource('estate', 'EstateController')->except(['show']);

        \Route::get('estate/archive', function (Request $request, EstateController $controller){
            $request->query->set('is_archived', 1);

            return $controller->index($request);
        })->name('estate.archive');

        \Route::get('estate/{estate}/comments', 'EstateController@comments')->name('estate.comments');
        \Route::post('estate/{estate}/comments', 'EstateController@addComment')->name('estate.comments.add');
        \Route::get('estate/{estate}/comments/{commentId}', 'EstateController@editComment')->name('estate.comments.edit');
        \Route::put('estate/{estate}/comments/{commentId}', 'EstateController@updateComment')->name('estate.comments.update');
        \Route::get('estate/{estate}/comments/{commentId}/delete', 'EstateController@deleteComment')->name('estate.comments.delete');

        \Route::get('estate/{estate}/proposal', 'EstateController@showAd')->name('estate.ad');
        \Route::put('estate/{estate}/proposal', 'EstateController@updateAd')->name('estate.ad.update');

        \Route::get('estate/{estate}/declarations', 'EstateController@declarations')->name('estate.declarations');

        \Route::get('estate/{estate}/history', 'EstateController@history')->name('estate.history');
        \Route::post('estate/{estate}/history', 'EstateController@pushHistoryEvent')->name('estate.history.add');
        \Route::get('estate/{estate}/history/{eventId}', 'EstateController@editHistoryEvent')->name('estate.history.edit');
        \Route::put('estate/{estate}/history/{eventId}', 'EstateController@updateHistoryEvent')->name('estate.history.update');
        \Route::get('estate/{estate}/history{eventId}/delete', 'EstateController@deleteHistoryEvent')->name('estate.history.delete');
    }

    public function __construct()
    {
        $this->tplWidget = 'admin/estate/comments/widget';
        $this->assignObject(Estate::class);
        $this->assignAlias('admin.estate');
    }

    public function index(Request $request)
    {
        $estates = Estate::with([
            'broker',
            'marketing',
            'contract',
        ]);

        if ($request->has('s')) {
            $search = '%' . $request->input('s') . '%';
            $estates->where(function ($query) use ($search) {

                return $query->where('title', 'like', $search)
                    ->orWhere('object_title', 'like', $search)
                    ->orWhere('object_addr', 'like', $search);

            });
        }

        if ($request->filled('today')) {

            $estates->whereHas('events', function (Builder $events) {
                $events->whereNextCallDate(Carbon::today())->groupBy('estate_id');
            })->count();
        }

        $estates->where('is_archived', $request->has('is_archived'));

        $estates->leftJoinSub(
            DB::table('estate_events')
                ->select('estate_id', DB::raw('MIN(next_call_date) as next_call_date'))
                ->where('next_call_date', '>', Carbon::now())
                ->groupBy('estate_id')
            ,
            'next_call_date',
            'estate_id',
            '=',
            'estates.id'
        );

        if ($request->filled('ad_state')) {
            $estates->whereHas('ad', function (Builder $builder) use ($request) {
                $builder->whereAdState($request->input('ad_state'));
            });
        }

        if ($request->filled('publication_state')) {
            $estates->whereHas('ad', function (Builder $builder) use ($request) {
                $builder->wherePublicationState($request->input('publication_state'));
            });
        }

        $userId = null;
        if (Auth::user()->can('user_data_access')) {
            if ($request->filled('broker_id')) {
                $userId = $request->input('broker_id');
            }
        } else {
            $userId = Auth::user()->id;
        }

        if ($userId) {
            $estates->whereBrokerId($userId);
        }

        $estates = $estates
            ->orderBy('created_at', 'desc')
            ->paginate(20);

        return view('admin/estate/list', [
            'estates' => $estates
        ]);
    }

    function create()
    {
        return view('admin/estate/create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required'
        ]);

        $data = $request->all();

        $data['broker_id'] = Auth::user()->id;
        if (Auth::user()->can('user_data_access')) {
            if ($request->filled('broker_id')) {
                $data['broker_id'] = $request->input('broker_id');
            }
        }

        $estate = Estate::create($data);

        return redirect()->route('admin.estate.edit', [
            'id' => $estate->id
        ]);
    }

    public function edit($id)
    {
        $estate = Estate::findOrFail($id);

        return view('admin/estate/edit', [
            'estate' => $estate
        ]);
    }

    public function update(Request $request, $id)
    {
        $estate = Estate::findOrFail($id);

        if ($request->has('is_delete')) {
            return view('admin/estate/delete_confirmation', [
                'estate' => $estate
            ]);
        }

        $estate->fill($request->all())
            ->save();

        return redirect()->back()->with('success', 1);
    }

    public function destroy($id)
    {
        Estate::findOrFail($id)
            ->delete();

        return redirect()->route('admin.estate.index');
    }

    /* ad section */

    public function showAd($estateId)
    {
        $estate = Estate::findOrFail($estateId);
        $ad = $estate->ad;

        if (empty($ad)) {
            $ad = new Ad();
            $ad->estate_id = $estate->id;
        }

        $galleryBasket = FileBasket::create(old('gallery_basket_id'));
        $filesBasket = FileBasket::create(old('files_basket_id'));

        $gallery = $galleryBasket->getAllImages($estate->ad, 300, 300);

        return view('admin/estate/ad', [
            'estate' => $estate,
            'ad' => $ad,
            'galleryBasket' => $galleryBasket,
            'filesBasket' => $filesBasket,
            'gallery' => $gallery,
            'categories' => EstateCategory::all()
        ]);
    }

    public function updateAd($estateId, Request $request)
    {
        $estate = Estate::findOrFail($estateId);
        $ad = $estate->ad;

        $this->validate($request, [
            'contact_name' => 'required',
            'contact_phone' => 'required',
            'price' => 'required',
            'ad_description' => 'required',
        ]);

        if (empty($ad)) {
            $ad = new Ad();
            $ad->created_by = Auth::user()->id;
            $ad->estate_id = $estate->id;

            $ad->save();
        }

        $ad->fill($request->all());

        //collect categories
        if ($request->filled('categories')) {
            $reqCategories = explode(',', $request->input('categories'));
            $adCategories = $ad->adCategories()->pluck('category_id')->toArray();

            foreach ($adCategories as $categoryId) {
                if (!in_array($categoryId, $reqCategories)) {
                    $ad->detachCategory($categoryId);
                }
            }

            foreach ($reqCategories as $categoryId) {
                if ($categoryId && !in_array($categoryId, $adCategories)) {
                    $ad->attachCategory($categoryId);
                }
            }
        }


        //attache images
        if ($request->filled('gallery_basket_id')) {

            $galleryBasket = FileBasket::create($request->input('gallery_basket_id'));

            foreach ($galleryBasket->getAllFiles() as $file) {
                $ad->attacheImageByPath($file);
            }
        }

        //drop images
        if ($request->filled('dropped_images')) {

            $droppedImages = json_decode($request->input('dropped_images'));

            if (json_last_error() === JSON_ERROR_NONE && is_array($droppedImages)) {
                foreach ($droppedImages as $image) {
                    $ad->detachImage($image);
                }
            }
        }

        //update uploaded files
        $filesBasket = FileBasket::create($request->input('files_basket_id'));

        if ($request->filled('presentation_file')) {
            $presentationFile = $filesBasket->getFile($request->input('presentation_file'));

            $ad->attachePresentation($presentationFile);
        }

        if ($ad->isDirty()) {
            $ad->save();
        }

        return redirect()->back()->with('success', 1);
    }

    /* declarations section */

    public function declarations($estateId)
    {
        $estate = Estate::findOrFail($estateId);

        $declarations = $estate->declarations()
            ->with(['broker'])
            ->paginate(20);

        return view('admin/estate/declarations', [
            'estate' => $estate,
            'declarations' => $declarations,
        ]);
    }

    /* history section */

    public function history($estateId)
    {
        $estate = Estate::findOrFail($estateId);
        $events = $estate->events()->latest();

        //different event list for each user

        if (!Auth::user()->can('manage_estate_event_history')) {
            $events->whereCreatedBy(Auth::user()->id);
        }

        return view('admin/estate/history', [
            'estate' => $estate,
            'events' => $events->paginate(20)
        ]);
    }

    public function pushHistoryEvent($estateId, Request $request)
    {
        /**
         * @var Estate;
         */
        $estate = Estate::findOrFail($estateId);

        $this->validate($request, [
            'description' => 'required',
        ]);

        $event = $estate->pushEvent($request->all());

        return redirect()->back()->with('event_id', $event->id);
    }

    public function editHistoryEvent($estateId, $eventId)
    {
        $estate = Estate::findOrFail($estateId);
        $event = $estate->events()->findOrFail($eventId);

        return view('admin/estate/events/edit',
            [
                'estate' => $estate,
                'event' => $event,
            ]);
    }

    public function updateHistoryEvent($estateId, $eventId, Request $request)
    {
        $estate = Estate::findOrFail($estateId);
        $event = $estate->events()->findOrFail($eventId);

        $event->update($request->all());

        return redirect()->route('admin.estate.history', $estate->id)
            ->with('eventId', $event->id);
    }

    public function deleteHistoryEvent($estateId, $eventId)
    {
        $comment = Estate::findORFail($estateId)
            ->events()
            ->findOrFail($eventId)
            ->delete();

        return redirect()->route('admin.estate.history', $estateId);
    }

    /* comments section */

    public function comments($estateId)
    {
        $estate = Estate::findOrFail($estateId);

        $comments = $estate->comments();
        $comments->orders = null;
        $comments = $comments->orderBy('created_at', 'desc')
            ->get();

        $formAction = URL::route($this->routerAlias . '.comments.add', ['id' => $estate->id]);
        $access = $this->getCommentAccess($estate);

        $comments = view($this->tplWidget, compact('formAction', 'comments', 'access') + ['object' => $estate]);

        return view('admin/estate/comments', [
            'estate' => $estate,
            'comments' => $comments
        ]);
    }

    public function editComment($estateId, $commentId)
    {
        $estate = Estate::findORFail($estateId);
        $comment = $estate->comments()->findOrFail($commentId);

        return view('admin/estate/comments/edit',
            [
                'estate' => $estate,
                'comment' => $comment
            ]);
    }

    public function updateComment($estateId, $commentId, Request $request)
    {
        $estate = Estate::findORFail($estateId);
        $comment = $estate->comments()->findOrFail($commentId);

        if ($request->has('content')) {
            $comment->content = $request->input('content');
            $comment->save();
        }

        return redirect()->route('admin.estate.comments', $estate->id)
            ->with('commentId', $comment->id);
    }

    public function deleteComment($estateId, $commentId)
    {
        $comment = Estate::findORFail($estateId)
            ->comments()
            ->findOrFail($commentId)
            ->delete();

        return redirect()->route('admin.estate.comments', $estateId);
    }

}
