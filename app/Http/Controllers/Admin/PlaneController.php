<?php
/**
 * Filename: PlaneController.php
 * Creator: Vasenin Leonid {leonid.vasenin@gmail.com}
 * Date: 18.11.2019
 */

namespace App\Http\Controllers\Admin;

use App\CustomerCallingHistory;
use App\Declaration;
use App\Estate;
use App\Http\Controllers\Controller;
use App\PlaneHistory;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PlaneController extends Controller
{
    static public function route()
    {
        \Route::post('/my-plane/', 'PlaneController@store')->name('my-plane.add');
        \Route::get('/my-plane/', 'PlaneController@index')->name('my-plane.index');
    }

    private $objectTypes = [
        'estate' => Estate::class,
        'declaration' => Declaration::class,
        'calls' => CustomerCallingHistory::class,
    ];

    public function index()
    {
        $planes = PlaneHistory::whereBrokerId(Auth::user()->id)
            ->with(['action', 'estate', 'declaration', 'customer_history'])
            ->paginate(20);

        return view('admin/plane/list', [
            'planes' => $planes
        ]);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'description' => 'required',
            'action_id' => 'required',
        ]);

        $plane = new PlaneHistory();

        $plane->created_by = Auth::user()->id;
        $plane->broker_id = Auth::user()->id;
        $plane->content = $request->input('description');

        /* object relationship */

        if ($request->filled(['object_class', 'object_id'])) {
            $sourceType = $request->input('object_class');
            if (array_key_exists($sourceType, $this->objectTypes)) {
                $plane->object_id = $request->input('object_id');
                $plane->object_class = $this->objectTypes[$sourceType];
            }
        }

        /* action prerequisites */

        $plane->action_id = $request->input('action_id');

        if ($request->anyFilled(['plane_date'])) {
            $timestamp = Carbon::createFromFormat('Y-m-d', $request->input('plane_date'));
            $timestamp->setTimeFrom(Carbon::createFromTimestamp($request->input('plane_time', 0)));

            $plane->action_at = $timestamp;
        }

        $plane->save();

        return redirect()
            ->back()
            ->with('new_plane_id', $plane->id);
    }
}