<?php

namespace App\Http\Controllers\Admin;

use App\Role;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UsersController extends Controller
{
    public function index(Request $request)
    {
        $users = User::orderBy('name');

        if(!$request->filled('all')){
            $users->whereStatus('active');
        }

        $users = $users->paginate();
        $counter = $users->firstItem();

        return view('admin/users/list', compact('users', 'counter'));
    }

    public function create()
    {
        return view('admin/users/create', [
            'user' => \request()
        ]);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => ['required', 'string', 'max:255'],
            'phone' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);

        $data = $request->all();
        $data['password'] = Hash::make($data['password']);

        $user = User::create($data);

        return redirect()->route('admin.users.show', $user->id);
    }

    public function show($id)
    {
        return $this->edit($id);
    }

    public function edit($id)
    {
        $user = User::find($id);
        $roles = Role::all();

        return view('admin/users/edit', compact('user', 'roles'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => ['string', 'max:255'],
            'phone' => ['string', 'max:255'],
            'email' => ['string', 'email', 'max:255'],
        ]);

        $user = User::findOrFail($id);

        $data = $request->all();

        if ($request->filled('password')) {
            $this->validate($request, [
                'password' => ['string', 'min:8', 'confirmed'],
            ]);
            $data['password'] = Hash::make($data['password']);
        }else{
            unset($data['password']);
        }

        $user->fill($data)
            ->save();

        return redirect(route('admin.users.edit', $user->id))->with('success', 'Пользователь обновлён.');
    }

    public function showRoles($userId)
    {
        $user = User::findOrFail($userId);
        $roles = Role::all();

        return view('admin/users/roles', compact('user', 'roles'));
    }

    public function updateRoles($userId, Request $request)
    {
        $user = User::findOrFail($userId);
        $userRoles = $user->roles->pluck('id')->toArray();

        $roles = $request->input('roles');

        foreach ($roles as $roleId => $state) {
            if (in_array($roleId, $userRoles) !== (bool)$state) {
                if ($state) {
                    $user->attachRole($roleId);
                } else {
                    $user->detachRole($roleId);
                }
            }
        }

        return redirect()->route('admin.users.roles', $user->id)
            ->with('status', 'Роли обновлены.');
    }
}

