<?php

namespace App\Http\Controllers\Admin;

use App\CustomerCallingHistory;
use App\EventsLog;
use App\Http\Controllers\Controller;
use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class CustomerCallingController extends Controller
{
    static public function route()
    {
        \Route::resource('declaration', 'DeclarationController');

        \Route::get('customer-calling-history', 'CustomerCallingController@index')->name('customer.calling.index');
        \Route::get('customer-calling-history/create', 'CustomerCallingController@create')->name('customer.calling.create');
        \Route::post('customer-calling-history', 'CustomerCallingController@store')->name('customer.calling.store');
        \Route::get('customer-calling-history/{id}', 'CustomerCallingController@edit')->name('customer.calling.edit');
        \Route::put('customer-calling-history/{id}', 'CustomerCallingController@update')->name('customer.calling.update');
        \Route::get('customer-calling-history/{id}/delete', 'CustomerCallingController@delete')->name('customer.calling.delete');
    }

    public function index(Request $request)
    {
        $calls = CustomerCallingHistory::orderBy('created_at', 'desc')
            ->with(['broker', 'lastCallEvent', 'source'])
            ->filterByAccess($request->input('broker_id'), 'broker_id');

        if ($request->filled('today')) {
            $calls->whereNextCallDate(Carbon::today());
        }

        return view('admin/customer_calling/list', [
            'calls' => $calls->paginate(20)
        ]);
    }

    public function create(Request $request)
    {
        return view('admin/customer_calling/create', [
            'call' => $request
        ]);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'customer_source_id' => 'required',
            'phone' => 'required',
            'ad_text' => 'required'
        ]);

        $data = $request->all();
        $data['broker_id'] = Auth::user()->id;

        if (Auth::user()->can('edit_customer_call_history')) {
            if ($request->filled('broker_id')) {
                $broker = User::find($request->input('broker_id'));
                if ($broker) {
                    $data['broker_id'] = $broker->id;
                }
            }
        }

        $call = CustomerCallingHistory::create($data);

        $this->handleCallEvent($call, $request);

        return redirect()
            ->route('admin.customer.calling.index')
            ->with(['call_id' => $call->id]);
    }

    public function edit($callId)
    {
        $call = CustomerCallingHistory::findOrFail($callId);

        return view('admin/customer_calling/edit', [
            'call' => $call
        ]);
    }

    public function update($callId, Request $request)
    {

        $call = CustomerCallingHistory::findOrFail($callId);
        $data = $request->all();

        if ($call->isUserAccess()) {

            $this->validate($request, [
                'customer_source_id' => 'required',
                'phone' => 'required',
                'ad_text' => 'required',
            ]);

            if (Auth::user()->can('edit_customer_call_history')) {
                if ($request->filled('broker_id')) {
                    $broker = User::find($request->input('broker_id'));
                    if ($broker) {
                        $data['broker_id'] = $broker->id;
                    }
                }
            }

            $call->update($data);
        }

        $event = $this->handleCallEvent($call, $request);
        $response = redirect()->back();

        if ($event) {
            $response->with(['event_id' => $event->id]);
        }

        return $response;
    }

    /* manage event */

    public function editEvent($callId, $eventId)
    {
        $event = EventsLog::whereId($eventId)
            ->whereThreadId($callId)
            ->whereClassName(CustomerCallingHistory::class)
            ->firstOrFail();

        if (!$event->isUserAccess()) {
            abort(403);
        }

        return view('/admin/customer_calling/event/edit', [
            'event' => $event
        ]);
    }

    public function updateEvent($callId, $eventId, Request $request)
    {
        $event = EventsLog::whereId($eventId)
            ->whereThreadId($callId)
            ->whereClassName(CustomerCallingHistory::class)
            ->firstOrFail();

        if (!$event->isUserAccess()) {
            abort(403);
        }

        $event->update([
            'description' => $request->input('description'),
            'next_call_date' => $request->input('next_call_date'),
        ]);


        return redirect()->route('admin.customer.calling.edit', $callId)
            ->with('event_id', $eventId);
    }

    public function deleteEvent($callId, $eventId, Request $request)
    {
    }

    private function handleCallEvent(CustomerCallingHistory $call, Request $request)
    {
        return $call->addCallEvent(
            Auth::user()->id,
            $request->input('description'),
            $request->input('next_call_date')
        );
    }
}
