<?php


namespace App\Http\Controllers\Admin;


use App\Review;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ReviewController extends Controller
{

    public function index()
    {
        $reviews = Review::paginate(20);

        return view('admin/review/list', [
          'reviews' => $reviews
        ]);
    }

    public function create(Request $request)
    {
        return view('admin/review/create', [
          'review' => $request
        ]);
    }

    public function edit($id)
    {
        $review = Review::findOrFail($id);

        return view('admin/review/edit', [
          'review' => $review
        ]);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
          'author' => 'required',
          'content' => 'required',
        ]);

        $review = Review::create($request->all());

        return redirect()->route('admin.review.index')
          ->with('status', 'Запись успешло добавлена')
          ->with('new_declaration_id', $review->id);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
          'author' => 'required',
          'content' => 'required',
        ]);

        $review = Review::findOrFail($id);

        $review->update($request->all());

        return redirect()->route('admin.review.index')
          ->with('status', 'Запись успешло добавлена')
          ->with('new_declaration_id', $review->id);
    }

    public function destroy($id)
    {

        Review::findOrFail($id)->delete();

        return redirect()->route('admin.review.index')
          ->with('status', 'Запись успешло удалена');
    }
}
