<?php

namespace App\Http\Controllers\Admin;

use App\Article;
use App\Services\FileBasket;
use Auth;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;

class ArticlesController extends Controller
{
    public function index(Request $request)
    {
        $meta = [
            'title' => 'Все страницы',
        ];

        /**
         * @var Builder
         */
        $articles = Article::orderBy('id', 'DESC')
            ->whereNull('deleted_by')
            ->with(['owner']);

        if ($request->has('autor')) {
            $articles->where('created_by', $request->input('author'));
        }

        if ($request->filled('category_id')) {
            $articles->whereCategoryId($request->input('category_id'));
        }

        if ($request->filled('s')) {
            $articles->where('title', 'like', '%' . $request->input('s') . '%');
        }

        $articles = $articles->paginate(20);

        return view('admin/articles/list',
            [
                'articles' => $articles,
                'counter' => $articles->firstItem(),
            ], $meta);
    }

    public function create(Request $request)
    {
        $meta = [
            'title' => 'Добавление новой страницы',
            'article' => $request,
        ];

        return view('admin/articles/create', $meta);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'text' => 'required',
        ]);

        $article = new Article($request->all());

        $article->save();

        return redirect(route('admin.articles.edit', [
            'id' => $article->id
        ]))->with('status', 'Страница сохранена');
    }

    public function edit($id)
    {
        $article = Article::findOrFail($id);

        $meta = [
            'title' => 'Редактирование страницы ' . $article->title,
        ];

        return view('admin/articles/edit', compact('article'), $meta);
    }

    public function update(Request $request, $id)
    {
        $article = Article::findOrFail($id);

        if ($request->delete_article == 'delete') {
            $article->deleted_by = Auth::user()->id;
            $article->deleted_at = Carbon::now();

            $article->save();
            return redirect()->back()->with('status', 'Страница перемещена в корзину.');
        }

        $this->validate($request, [
            'title' => 'required',
            'text' => 'required',
        ]);

        $article->fill($request->all());

        $article->save();

        if ($request->has('preview')) {
            $article->preview = $request->file('preview');
        } elseif ($request->filled('remove_preview')) {
            $article->preview = null;
        }

        return redirect(route('admin.articles.edit', [
            'id' => $article->id
        ]))->with('success', 1);
    }

    public function destroy($id)
    {
        //TODO: need add destroy article action
    }
}
