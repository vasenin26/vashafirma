<?php
/**
 * Filename: FormReportClass.php
 * Creator: Vasenin Leonid {leonid.vasenin@gmail.com}
 * Date: 11.11.2019
 */

namespace App\Http\Controllers\Admin;

use Symfony\Component\HttpFoundation\Request;
use App\FormReport;
use App\Http\Controllers\Controller;
use App\Traits\CommentsController;

class FormReportController extends Controller
{
    use CommentsController;

    public function __construct()
    {
        $this->middleware('permission:manage_request_form');
        $this->tplWidget = 'admin/form_reports/comments/widget';
        $this->assignObject(FormReport::class);
        $this->assignAlias('admin.form');
    }

    public function index(Request $request)
    {
        $forms = FormReport::with(['reviewer']);

        if($request->filled('is_new')){
            $forms->whereNull('opened_by');
        }

        if($request->filled('broker_id')){
            $forms->whereOpenedBy($request->input('broker_id'));
        }

        if($request->filled('form_key')){
            $forms->whereFormKey($request->input('form_key'));
        }

        $formKeys = \App\FormReport::groupBy('form_key')->pluck('form_key')->toArray();

        array_walk($formKeys, function (&$item){
            $item = [
                'title' => __('form.'. $item),
                'id' => $item
            ];
        });

        return view('admin/form_reports/list', [
            'forms' => $forms->latest()->paginate(20),
            'form_keys' => json_encode($formKeys)
        ]);
    }

    public function show($formId)
    {
        $form = FormReport::findOrFail($formId);

        if (!$form->opened_by) {
            $form->registerBroker(\Auth::user()->id);
        }

        return view('admin/form_reports/show', [
            'form' => $form,
            'comments' => $this->renderComments($form)
        ]);
    }
}