<?php

namespace App\Http\Controllers\Admin;

use App\Declaration;
use App\Http\Controllers\Controller;
use App\Traits\CommentsController;
use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DeclarationController extends Controller
{
    use CommentsController;

    static public function route()
    {
        \Route::get('declaration/{declaration}/comments', 'DeclarationController@comments')->name('declaration.comments');
        \Route::post('declaration/{estate}/comments', 'DeclarationController@addComment')->name('declaration.comments.add');
        \Route::get('declaration/{declaration}/comments/{commentId}', 'DeclarationController@editComment')->name('declaration.comments.edit');
        \Route::put('declaration/{declaration}/comments/{commentId}', 'DeclarationController@updateComment')->name('declaration.comments.update');
        \Route::get('declaration/{declaration}/comments/{commentId}/delete', 'DeclarationController@deleteComment')->name('declaration.comments.delete');
    }

    public function __construct()
    {
        $this->tplWidget = 'admin/declaration/comments/widget';
        $this->assignObject(Declaration::class);
        $this->assignAlias('admin.declaration');
    }

    public function index(Request $request)
    {
        $declarations = Declaration::with(['broker', 'estate']);

        $filters = [
            'estate_id' => 'estate_id',
            's' => function (Builder $declarations, $value) {
                $declarations->whereHas('estate', function (Builder $estates) use ($value) {
                    $value = "%$value%";
                    $estates->where('estates.object_title', 'like', $value)
                        ->orWhere('estates.object_addr', 'like', $value);
                })->orWhereHas('broker', function (Builder $broker) use ($value) {
                    $broker->where('users.name', 'like', "%$value%")
                        ->orWhere('users.phone', 'like', "%$value%");
                })->orWhere('client_name', 'like', "%$value%")
                    ->orWhere('client_phone', 'like', "%$value%")
                    ->orWhere('client_email', 'like', "%$value%");
            },
            'today' => function (Builder $declarations, $value) {
                $declarations->whereDisplayDate(Carbon::today());
            },
        ];

        foreach ($filters as $filter => $condition) {
            if ($request->filled($filter)) {
                if (is_callable($condition)) {
                    $condition($declarations, $request->input($filter));
                } else {
                    $declarations->where($condition, $request->input($filter));
                }
            }
        }

        $declarations->filterByAccess($request->input('broker_id', null), 'broker_id');

        return view('admin/declaration/list', [
            'declarations' => $declarations->paginate(20)
        ]);
    }

    public function create(Request $request)
    {
        return view('admin/declaration/create', [
            'declaration' => $request
        ]);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'client_name' => 'required',
            'client_phone' => 'required',
            'display_date' => 'date',
            'estate_id' => 'required',
        ]);

        $data = $request->all();

        $data['is_subscription'] = !!($data['is_subscription'] ?? 0);

        if (Auth::user()->can('set_declaration_broker')) {
            if ($request->filled('broker_id')) {
                $broker = User::find($request->input('broker_id'));
                if ($broker) {
                    $data['broker_id'] = $broker->id;
                }
            }
        }

        if (empty($data['broker_id'])) {
            $data['broker_id'] = Auth::user()->id;
        }

        $declaration = Declaration::create($data);

        if ($declaration->estate_id) {
            $route = redirect()->route('admin.estate.declarations', ['estate' => $declaration->estate_id]);
        } else {
            $route = redirect()->route('admin.declaration.index');
        }

        return $route->with('status', 'Запись успешло добавлена')
            ->with('declaration_id', $declaration->id);
    }

    public function edit($id)
    {
        $declaration = Declaration::findOrFail($id);

        return view('admin/declaration/edit', [
            'declaration' => $declaration,
            'estate' => $declaration->estate
        ]);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'client_name' => 'required',
            'client_phone' => 'required',
            'display_date' => 'nullable|date',
            'estate_id' => 'required',
        ]);

        $declaration = Declaration::findOrFail($id);

        $data = $request->all();

        $data['is_subscription'] = !!($data['is_subscription'] ?? 0);

        if (!Auth::user()->can('set_declaration_broker')) {
            unset($data['broker_id']);
        }

        $declaration->update($data);

        if ($declaration->estate_id) {
            $route = redirect()->route('admin.estate.declarations', ['estate' => $declaration->estate_id]);
        } else {
            $route = redirect()->route('admin.declaration.index');
        }

        return $route->with('status', 'Запись успешло обновлена')
            ->with('declaration_id', $declaration->id);
    }

    public function destroy($id)
    {
        Declaration::findOrFail($id)->delete();

        return redirect()->route('admin.declaration.index')
            ->with('status', 'Запись успешло удалена');
    }

    /* comments section */

    public function comments($declarationId)
    {
        $declaration = Declaration::findOrFail($declarationId);

        return view('admin/declaration/comments', [
            'declaration' => $declaration,
            'comments' => $this->renderComments($declaration),
            'estate' => $declaration->estate
        ]);
    }

    public function editComment($declarationId, $commentId)
    {
        $declaration = Declaration::findORFail($declarationId);
        $comment = $declaration->comments()->findOrFail($commentId);

        return view('admin/declaration/comments/edit',
            [
                'estate' => $declaration,
                'comment' => $comment
            ]);
    }

    public function updateComment($declarationId, $commentId, Request $request)
    {
        $declaration = Declaration::findORFail($declarationId);
        $comment = $declaration->comments()->findOrFail($commentId);

        if ($request->has('content')) {
            $comment->content = $request->input('content');
            $comment->save();
        }

        return redirect()->route('admin.declaration.comments', $declaration->id)
            ->with('commentId', $comment->id);
    }

    public function deleteComment($declarationId, $commentId)
    {
        $comment = Declaration::findORFail($declarationId)
            ->comments()
            ->findOrFail($commentId)
            ->delete();

        return redirect()->route('admin.declaration.comments', $declarationId);
    }
}
