<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class DocumentsController extends Controller
{
    protected $folder = 'documents/';


    public function index()
    {
        $files = Storage::disk()->files($this->folder);

        return view('admin/documents/list', [
            'files' => $files
        ]);
    }

    public function upload(Request $request)
    {
        $uploadedFile = $request->file('document');

        Storage::disk()->putFileAs($this->folder, $uploadedFile, $uploadedFile->getClientOriginalName());

        return back();
    }

    public function delete(Request $request)
    {
        Storage::disk()->delete($this->folder . $request->input('file'));

        return back();
    }
}
