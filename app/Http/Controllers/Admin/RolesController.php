<?php

namespace App\Http\Controllers\Admin;

use App\Permission;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Role;

class RolesController extends Controller
{
    /**
                Все имеющиеся роли
     */
    public function index()
    {
        $meta = [
            'title' => 'Управление ролями пользователей',
        ];

        $roles = Role::all();
        $counter = 1;

        return view('admin/roles/list', compact('roles', 'counter'), $meta);
    }

    /**
                Шаблон создания ролей
     */
    public function create()
    {
        $meta = [
            'title' => 'Добавление роли для пользователей',
        ];

        return view('admin/roles/create', $meta);
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'display_name'=>'required',
        ]);

        $roles = new Role([
            'display_name' => $request->display_name,
            'description' => $request->description
        ]);

        $roles->save();

        return redirect(route('admin.roles.index'))->with('status', 'Роль успешно добавлена.');
    }

    public function edit($id)
    {
        $role = Role::findOrFail($id);
        $permissions = Permission::all();

        return view('admin/roles/edit', compact('role', 'permissions'));
    }

    /**
            Внесение изменений роли в БД
     */
    public function update(Request $request, $id)
    {
        $role = Role::find($id);

            Role::whereId($id)->update([
                'display_name' => $request->display_name,
                'description' => $request->description,
            ]);

            $role->permissions()->detach();
            if ($request->input('permissions')) {
                $role->permissions()->attach($request->input('permissions'));
            }

        return redirect()->back()->with('status', 'Изменения для роли сохранены.');
    }

    /**
                Удаление роли
     */
    public function destroy($id)
    {
        $role = Role::find($id);

        $role->permissions()->detach();

        Role::whereId($id)->delete();

        return redirect(route('admin.roles.index'))->with('status', 'Выбранная роль удалена.');
    }
}
