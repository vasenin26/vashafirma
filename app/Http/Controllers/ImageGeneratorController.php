<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Storage;

class ImageGeneratorController extends Controller
{
    //

    public function generate(Request $request, Response $response){

        if(!$request->has('path')){
            abort(500);
        }

        $imagePath = $request->get('path');

        if(!Storage::exists($imagePath)){
            abort(500);
        }

        $generator = new \App\Services\ImageGenerator($imagePath);

        $options = $request->toArray();
        unset($options['path']);

        if(!$generator->checkOptions($options)){
            return abort(500);
        }

        $imagePath = $generator->getImageUrl($options);

        if($imagePath === false){
            abort(500);
        }

        return redirect($imagePath);
    }

    public function flush(){
        //remove old images
        return \App\Services\ImageGenerator::flush();
    }
}
