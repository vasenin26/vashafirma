<?php

namespace App\Http\Controllers;

use App\FormReport;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class FromHandlerController extends Controller
{
    protected $forms = [
        'callback' => [
            'name' => '',
            'email' => '',
            'phone' => 'required',
        ],
        'job_request' => [
            'name' => '',
            'email' => '',
            'phone' => 'required',
            'additional' => '',
            'file' => '',
        ],
        'sale_request' => [
            'form_source' => '',
            'name' => '',
            'email' => '',
            'phone' => 'required'
        ],
        'object_request' => [
            'object_id' => 'required',
            'name' => '',
            'email' => '',
            'phone' => 'required'
        ],
        'overview' => [
            'object_id' => 'required',
            'name' => '',
            'email' => '',
            'phone' => 'required'
        ],
        'makeprice' => [
            'object_id' => 'required',
            'name' => '',
            'email' => '',
            'price' => '',
            'phone' => 'required'
        ]
    ];

    function send($formKey, Request $request)
    {
        if (!array_key_exists($formKey, $this->forms)) {
            abort(412);
        }

        $form = $this->forms[$formKey];
        $rules = array_filter($form, function ($item) {
            return !empty($item);
        });

        $request->validate($rules);

        /*
        Mail::to('lamnenid@ya.ru')
            ->view('emails/' + $form, $request->all(array_keys($form)));
        */

        $formData = $request->all(array_keys($form));

        FormReport::create([
            'form_key' => $formKey,
            'content' => $formData
        ]);

        return response()
            ->json([
                'success' => 1,
                'form_key' => $formKey
            ]);
    }
}
