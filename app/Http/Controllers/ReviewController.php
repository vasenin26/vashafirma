<?php


namespace App\Http\Controllers;

use App\Review;


class ReviewController extends Controller
{
    public function index()
    {
        $reviews = Review::all();

        return view('site/review/list', [
          'reviews' => $reviews
        ]);
    }
}