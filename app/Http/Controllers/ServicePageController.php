<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Arr;

class ServicePageController extends Controller
{
    function showModal($modalView, Request $request)
    {
        if (!$request->isXmlHttpRequest()) {
            return redirect()->route('home');
        }

        return view('modals/' . $modalView);
    }

    function special()
    {
        $route = Route::currentRouteName();
        $route = explode('.', $route);
        $template = Arr::last($route);
        $templatePath = 'site/special/' . $template;

        if(!view()->exists($templatePath)){
            abort(404);
        }

        return view($templatePath);
    }
}
