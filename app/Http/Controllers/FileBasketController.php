<?php

namespace App\Http\Controllers;

use App\Services\FileBasket;
use Illuminate\Http\Request;

class FileBasketController extends Controller
{
    public function putFile(Request $request){

        $this->validate($request, [
            'basketId' => 'required',
            'file' => 'required|max:120480'
        ]);

        $basketId = $request->input('basketId');
        $basket = FileBasket::create($basketId);

        $newFileName = $basket->putUploadedFile($request->file('file'));

        return response()->json(['file' => $newFileName]);
    }

    public function dropFile(Request $request){

        $this->validate($request, [
            'basketId' => 'required',
            'file' => 'required'
        ]);

        $basketId = $request->input('basketId');

        FileBasket::create($basketId)->deleteFile($request->input('file'));

        return response()->json();
    }
}
