<?php

use Illuminate\Database\Eloquent\Model;

if (!function_exists('classActivePath')) {
    function classActivePath($targetControllerAction, $class = 'active')
    {
        $route = Request::route();
        $controller = $route->getActionName();
        $namespace = $route->getAction('namespace');

        $controllerName = \Illuminate\Support\Str::replaceFirst($namespace, '', $controller);
        $controllerName = trim($controllerName, '\\');

        return $controllerName === $targetControllerAction ? ' ' . $class : '';
    }
}

if (!function_exists('classActiveSegment')) {
    function classActiveController($targetController, $class = 'active')
    {
        $route = Request::route();
        $controller = $route->getActionName();
        $namespace = $route->getAction('namespace');

        $controllerName = \Illuminate\Support\Str::replaceFirst($namespace, '', $controller);
        $controllerName = trim($controllerName, '\\');
        $controllerName = current(explode('@', $controllerName));

        $targetControllerName = current(explode('@', $targetController));

        return $controllerName === $targetControllerName ? ' ' . $class : '';
    }
}

if (!function_exists('classActiveUri')) {
    function classActiveUri($uri, $class = 'active')
    {
        $currentUri = Request::path();
        $targetUrl = trim($uri, '/');

        return $targetUrl === $currentUri ? ' ' . $class : '';
    }
}

if (!function_exists('classActiveRoute')) {
    function classActiveRoute($route, $class = 'active')
    {
        return Request::route()->getName() === $route ? ' ' . $class : '';
    }
}

if (!function_exists('classIsSession')) {
    function classIsSession(Model $model, $key, $class = 'active')
    {
        return $model->getKey() === Session::get($key) ? $class : '';
    }
}

if (!function_exists('checked_is')) {
    function checked_is($source, $key, $value)
    {

        $state = Illuminate\Support\Arr::get(app('request')->old(), $key);

        if (is_null($state)) {
            if ($source instanceof \Illuminate\Database\Eloquent\Model) {
                $state = Illuminate\Support\Arr::get($source->toArray(), $key);
            } elseif ($source instanceof Illuminate\Http\Request) {
                $state = Illuminate\Support\Arr::get($source->all($key), $key);
            } else {
                $state = $source;
            }
        }

        return $state == $value ? 'checked="checked"' : '';
    }
}

if (!function_exists('getCase')) {
    function getCase($_number, $_case1, $_case2, $_case3)
    {
        $base = $_number - floor($_number / 100) * 100;
        $result = null;

        if ($base > 9 && $base < 20) {
            $result = $_case3;

        } else {
            $remainder = $_number - floor($_number / 10) * 10;

            if (1 == $remainder) $result = $_case1;
            else if (0 < $remainder && 5 > $remainder) $result = $_case2;
            else $result = $_case3;
        }

        return $result;
    }
}

if (!function_exists('currentPage')) {
    function currentPage()
    {
        return $_SERVER['REQUEST_URI'];
    }
}