<?php

namespace App;

use \App\Interfaces\BelongsToUser;
use Illuminate\Database\Eloquent\Model;

class PlaneHistory extends Model implements BelongsToUser
{
    use Traits\BelongsToUser;

    protected $fillable = [
        'created_by',
        'broker_id',
        'object_id',
        'object_class',
        'action_id',
        'action_at',
    ];

    protected $dates = [
        'action_at'
    ];

    public function action()
    {
        return $this->hasOne(Dictionary::class, 'id', 'action_id');
    }

    public function estate()
    {
        return $this->hasOne(Estate::class, 'id', 'object_id');
    }

    public function declaration()
    {
        return $this->hasOne(Declaration::class, 'id', 'object_id');
    }

    public function customer_history()
    {
        return $this->hasOne(CustomerCallingHistory::class, 'id', 'object_id');
    }
}
