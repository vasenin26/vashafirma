<?php

namespace App;

use App\Traits\CommentsModel;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class FormReport extends Model
{
    use CommentsModel;

    protected $fillable = [
        'form_key',
        'content',
    ];

    protected $casts = [
        'content' => 'json'
    ];

    public function reviewer()
    {
        return $this->belongsTo('App\User', 'opened_by', 'id');
    }

    public function registerBroker($brokerId)
    {
        $this->attributes['opened_by'] = $brokerId;
        $this->attributes['opened_at'] = Carbon::now();

        self::save();
    }
}
