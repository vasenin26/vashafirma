<?php


namespace App;

use App\Traits\BelongsToUser;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Jobs extends Model
{
    protected $fillable = [
      'department_id',
      'job_name',
      'job_description',
      'is_published',
    ];

    public function departments()
    {
        return $this->hasOne('App\Departments', 'id', 'department_id');
    }

    public function getIsPublishedAttribute()
    {
        return is_null($this->attributes['deleted_at'] ?? null) && !is_null($this->attributes['published_at'] ?? null);
    }

    public function setIsPublishedAttribute($value)
    {
        $value = (bool)$value;

        if ($this->getIsPublishedAttribute() !== $value) {
            $this->attributes['published_by'] = $value ? \Auth::user()->id : null;
            $this->attributes['published_at'] = $value ? Carbon::now() : null;
        }

    }
}
