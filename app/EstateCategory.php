<?php

namespace App;

use App\Interfaces\BelongsToUser;
use App\Interfaces\DirectoryI;
use App\Interfaces\NestedStructureI;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class EstateCategory extends Model implements DirectoryI, BelongsToUser, NestedStructureI
{
    use \App\Traits\BelongsToUser;

    protected $fillable = [
        'idx',
        'title',
        'alias',
    ];

    protected static function boot()
    {
        static::creating(function (Model $model) {
            if (is_null($model->getAttribute('alias'))) {
                $model->createAlias();
            }

            if (empty($model->getAttribute('idx'))) {
                $model->calculateIdx();
            }
        });

        static::updating(function (Model $model) {
            if (is_null($model->getAttribute('alias'))) {
                $model->createAlias();
            }
        });

        parent::boot();
    }

    public function getFields(): array
    {
        return [
            'idx' => 'Порядок в списке',
            'title' => 'Название',
            'alias' => 'Ссылка на категорию',
            'created_by' => 'Автор',
            'created_at' => 'Дата создания',
        ];
    }

    public function getOrderColumn(): string
    {
        return 'idx';
    }

    public function getValuePreview(string $key)
    {
        $value = $this->getAttribute($key);

        switch ($key) {
            case 'created_by':
                if ($this->owner)
                    $value = $this->owner->name;
                break;
        }

        return $value;
    }

    public static function updateOrderStructure(array $structure)
    {
        foreach ($structure as $item) {

            $itemModel = self::find($item['id']);

            if (!empty($itemModel)) {
                $itemModel->idx = $item['order'];
                $itemModel->parent_id = $item['parent_id'];
                $itemModel->save();
            }

        }
    }

    public function createAlias()
    {
        $url = [];

        if ($this->getAttribute('parent_id')) {
            $parentCategory = self::findOrFail($this->parent_id);
            $url[] = trim($parentCategory->alias, '/');
        }

        $url[] = Str::slug($this->getAttribute('title'));

        $this->setAttribute('alias', join('/', $url));
    }

    private function calculateIdx()
    {
        $maxIdx = 0;
        $parentId = null;
        $siblings = self::whereParentId($parentId)->get();

        foreach ($siblings as $sibling) {
            if ($this->idx >= $maxIdx) {
                $maxIdx = $this->idx;
            }
        }

        $this->setAttribute('idx', ++$maxIdx);
    }

    public static function getChildren($parent)
    {
        $children = [];

        $categories = self::all();
        $find = function ($parentId) use ($categories, &$children, &$find) {
            if (in_array($parentId, $children)) {
                return;
            }

            foreach ($categories as $category) {
                if ($category->parent_id === $parentId) {
                    $children[] = $category->id;
                    $find($category->id);
                }
            }
        };

        $find($parent);
        return $children;
    }

    public function scopeToTree($scope)
    {
        $result = [];

        $collection = $scope->orderBy('idx')->get()->toArray();

        $buildTree = function ($parentId, $level = 0) use (&$collection, &$buildTree, &$result) {
            foreach ($collection as $item) {
                if ($item['parent_id'] !== $parentId) {
                    continue;
                }

                $result[$item['id']] = str_pad('', $level, '-') . ($level ? ' ' : '') . $item['title'];
                $buildTree($item['id'], $level + 1);
            }
        };

        $buildTree(null);

        return $result;
    }

    public function scopeIsPublished($scope)
    {
        return $scope->whereNotNull('ad_counters')
            ->where('ad_counters', '>', '0')
            ->whereNotNull('min_price');
    }

    public static function calculate(int $categoryId)
    {
        $category = self::whereId($categoryId)->first();

        if (empty($category)) {
            return;
        }

        $children = self::getChildren($categoryId);
        $children[] = $categoryId;

        $category->ad_counters = Ad::whereHas('adCategories', function ($builder) use ($children) {
            $builder->whereCategoryId($children);
        })->IsPublished()->count();

        $category->min_price = Ad::whereHas('adCategories', function ($builder) use ($children) {
            $builder->whereCategoryId($children);
        })->IsPublished()->minPrice()->min_price;

        $category->save();
    }
}
