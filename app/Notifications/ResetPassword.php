<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Auth\Notifications\ResetPassword as ResetPasswordNotification;

class ResetPassword extends ResetPasswordNotification
{

    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('Восстановление пароля от аккаунта: ' . config('app.name'))
            ->line('По вашему требованию мы выслали ссылку на восстановления пароля. Что бы продолжить нажмите "Восстановить".')

            ->action('Восстановить', url(config('app.url').route('password.reset', ['token' => $this->token, 'email' => $notifiable->getEmailForPasswordReset()], false)))

            ->line('Если Вы не отправляли запрос на восстановление пароля, проигнорируйте это сообщение.');
    }

}
