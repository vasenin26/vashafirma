@extends('app', [
'meta_title' => $category->title
])

@section('content')

  @include('site/parts/header', [
    'title' => $category->title
  ]);

  @if(!empty($children))
    <div class="category-business">
      <div class="container">
        <div class="row">

          @foreach($children as $category)

            @include('site/parts/category-card', [
            'category' => $category
            ])

          @endforeach
        </div>
      </div>
    </div>
  @endif

  <div class="select-business">
    <div class="container">
      @include('site/ad/search/form', [
      'activeIdx' => $searchFormTabIdx ?? 0,
      'filters' => $filters
      ])
    </div>
  </div>

  <div class="ready-business">
    <div class="container">
      <div class="d-flex flex-wrap justify-content-start align-items-left">
        <span class="list-begin">Показать сначала</span>
        <a class="list-filter" href="{{route('ad.search', ['order' => 'price'])}}">дешевле</a>
        <a class="list-filter" href="{{route('ad.search', ['order' => 'profit'])}}">прибыльнее</a>
        <a class="list-filter" href="{{route('ad.search', ['price_to' => '2000000'])}}">до 2 млн.</a>
      </div>
      <div class="row">
        @foreach($ads as $ad)
          @include('site/parts/ad-card', ['ad' => $ad])
        @endforeach
      </div>
    </div>
  </div> <!-- ready-business -->

  {!! $ads->links('site.parts.pagination') !!}

@endsection