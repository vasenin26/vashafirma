@extends('app', [
'meta_title' => 'Проданные объекты'
])

@section('content')

  @include('site/parts/header', [
    'title' => 'Каталог проданных объектов',
    'current_title' => 'Каталог проданных объектов'
  ]);

  <div class="container">
    @if($ads->count())

      <div class="ready-business">
        {{--
        <div class="row align-items-center mb-4">
          <div class="col-md d-flex justify-content-start">
            <span class="list-begin">Показать сначала</span>
            <a class="list-filter" href="{{route('ad.search', ['order' => 'price'])}}">дешевле</a>
            <a class="list-filter" href="{{route('ad.search', ['order' => 'profit'])}}">прибыльнее</a>
            <a class="list-filter" href="{{route('ad.search', ['order' => 'created'])}}">по дате</a>
          </div>
        </div>
        --}}
        <div class="row">
          @foreach($ads as $ad)
            @include('site/parts/ad-card', ['ad' => $ad])
          @endforeach
        </div>
      </div> <!-- ready-business -->

      {{$ads->links('site.parts.pagination')}}

    @else
      <div class="alert alert-info">
        К сожалению по вашему запросу ничего не найдено, скорректируйте запрос или воспользуйтесь подбором по
        категориям.
      </div>
    @endif
  </div>

@endsection