@extends('app')

@section('content')

  <div class="page-header">
    <div class="container">
      <h1 class="page-header__title">
        {{$ad->estate->title}} <span
            class="object-id">Идентификатор объекта: id{{str_pad($ad->estate->id, 6, '0', STR_PAD_LEFT)}}</span>
      </h1>
      <div class="breadcrumb-b">
        <ul class="nav">
          <li class="nav-item">
            <a class="nav-link-previous" href="/">Главная</a>
          </li>
          <li class="nav-item mr-2">
            <a class="nav-link-previous" href="{{route('ad.search')}}">Покупка</a>
          </li>
          @if($ad->mainCategory)
            <li class="nav-item">
              <a class="nav-link"
                 href="{{route('category.show', $ad->mainCategory->alias)}}">{{$ad->mainCategory->title}}</a>
            </li>
          @endif
        </ul>
      </div>
    </div>
  </div>

  <div class="business-info">
    <div class="container">

      <div class="row">

        <div class="col-sm-4 col-print-4 order-1">
          <div class="business-info__details">
            @if($ad->hasPremium())
              <div class="business-info__special"></div>
            @endif
            <div class="numbers">
              <span class="name">Цена</span>
              <span class="data">@money($ad->price) ₽</span>
            </div>
            <div class="numbers">
              <span class="name">Прибыль</span>
              <span class="data">@money($ad->profit) ₽/мес</span>
            </div>
            <div class="location">
              @if($ad->region)
                <div class="city">
                  <span class="name">Город:</span>
                  <span class="data">
                          {{$ad->region}}
                        </span>
                </div>
              @endif
              @if($ad->metro)
                <div class="city">
                  <span class="name">Метро:</span>
                  <span class="data">
                      {{$ad->metro->value}}
                  </span>
                </div>
              @endif
            </div>

            <span class="show-number no-print" data-phone="{{$ad->getPublicPhone()}}" data-module="HiddenPhone">
              <span class="name">Показать телефон</span>
              <span class="data" data-role="phone-container">{{$ad->getPublicPhoneDisguised()}}</span>
            </span>

            <div class="broker-name">
              <label>Ваш брокер:</label>
              @if($ad->hasPaid())
                <div>
                  {{$ad->contact_name}}
                </div>
                <div>
                  {{$ad->contact_email}}
                </div>
              @else
                {{$ad->getBroker('name')}}
              @endif
              <div class="print-only">
                {{$ad->getPublicPhone()}}
              </div>
            </div>

            <span class="submit-application no-print"
                  data-modal
                  data-href="{{route('special.modal', ['key' => 'object_request', 'object_id' => $ad->estate->id])}}"
            >Оставить заявку
            </span>
            <div class="additional-features no-print">
              <span class="propose-price" data-modal
                    data-href="{{route('special.modal', ['key' => 'makeprice', 'object_id' => $ad->estate->id])}}">Предложить свою цену</span>
              <br>
              <span class="arrange-viewing" data-modal
                    data-href="{{route('special.modal', ['key' => 'overview', 'object_id' => $ad->estate->id])}}">Договориться о просмотре</span>
            </div>
          </div>
        </div>

        <div class="col-sm-8 col-print-8 order-0">

          <div class="business-info__gallery simple-gallery  no-print" data-module="SingleGallery">

            <div class="simple-gallery__images gallery-box__images">
              @foreach($ad->images as $image)
                <a href="{{$image}}" class="simple-gallery__href">
                  <img alt="" src="{{$image->getSize(750, 450)}}">
                </a>
              @endforeach
            </div>

          </div> <!-- business-info__gallery -->

          <div class="business-info__categories">
            <label>Категория:</label>
            @foreach($ad->categories()->whereNotNull('alias')->get() as $category)
              @if($category->alias)
                <a class="business-info__categories-item data-business-category"
                   href="{{route('category.show', $category->alias)}}"
                >{{$category->title}}</a>
              @endif
            @endforeach
          </div>

          @if($ad->ad_description)
            <div class="business-info__content-group">
              <h4 class="title">Описание</h4>
              <div class="body">
                {!! $ad->ad_description !!}
              </div>
            </div>
          @endif

          @if($ad->video_id)

            <div class="business-inf__video mb-4">
              <iframe
                  src="https://www.youtube.com/embed/{{$ad->video_id}}"
                  frameborder="0"
                  allow="accelerometer; encrypted-media; gyroscope; picture-in-picture"
                  allowfullscreen
                  class="w-100"
              ></iframe>
            </div>

          @endif

          <div class="business-info__content-group">
            <div class="short-description">
              @if(URL::isValidUrl($ad->site_url))
                <div class="short-description__item">
                  <div class="label">Сайт:</div>
                  <div class="value">
                    <a href="{{$ad->site_url}}" rel="nofollow" target="_blank">{{$ad->site_url}}</a>
                  </div>
                </div>
              @endif
              @if(URL::isValidUrl($ad->presentation_file))
                <div class="short-description__item">
                  <div class="label">Презентация:</div>
                  <div class="value">
                    <a href="{{URL::asset($ad->presentation_path)}}" rel="nofollow" target="_blank">Скачать</a>
                  </div>
                </div>
              @endif
              @if($ad->legal_form)
                <div class="short-description__item">
                  <div class="label">Организационно-правовая форма:</div>
                  <div class="value">
                    {{$ad->legal_form}}
                  </div>
                </div>
              @endif
              @if($ad->period_of_existence)
                <div class="short-description__item">
                  <div class="label">Срок существования:</div>
                  <div class="value">
                    {{$ad->period_of_existence}} {{getCase($ad->period_of_existence, 'год', 'года', 'лет')}}
                  </div>
                </div>
              @endif
              @if($ad->number_of_staff)
                <div class="short-description__item">
                  <div class="label">Общее кличество персонала:</div>
                  <div class="value">
                    {{$ad->number_of_staff}} {{getCase($ad->number_of_staff, 'человек', 'человека', 'человек')}}
                  </div>
                </div>
              @endif
              @if($ad->reason_for_sale)
                <div class="short-description__item">
                  <div class="label">Причина продажи:</div>
                  <div class="value">
                    {{$ad->reason_for_sale}}
                  </div>
                </div>
              @endif
            </div>
          </div>

          <div class="business-info__content-group">
            <h4 class="title">Финансы</h4>

            <div class="short-description">
              @if($ad->price)
                <div class="short-description__item">
                  <span class="label">Цена:</span>
                  <span class="value">@money($ad->price) ₽</span>
                </div>
              @endif
              @if($ad->revenue)
                <div class="short-description__item">
                  <span class="label">Выручка/мес:</span>
                  <span class="value">@money($ad->revenue) ₽</span>
                </div>
              @endif
              @if($ad->profit)
                <div class="short-description__item">
                  <span class="label">Прибыль/мес:</span>
                  <span class="value">@money($ad->profit) ₽</span>
                </div>
              @endif
              @if($ad->projected_earnings)
                <div class="short-description__item">
                  <span class="label">Прогнозируемая прибыль/мес:</span>
                  <span class="value">@money($ad->projected_earnings) ₽</span>
                </div>
              @endif
              @if($ad->payback_period)
                <div class="short-description__item">
                  <span class="label">Окупаемость:</span>
                  <span
                      class="value">{{$ad->payback_period}} {{getCase($ad->payback_period, 'год', 'года', 'лет')}}</span>
                </div>
              @endif
            </div>
          </div>

          @foreach([
          'property' => 'Недвижимость',
          'means_of_production' => 'Средства производства',
          'staff' => 'Персонал',
          'benefits' => 'Выгоды для покупателя',
          'cost_structure' => 'Структура затрат',
          'documents' => 'Сертификаты, лицензии, решения',
          'additional_info' => 'Дополнительная информация',
          ] as $key => $title)
            @if($ad->getAttribute($key))
              <div class="business-info__content-group">
                <h4 class="title">{{$title}}</h4>
                <div class="body">
                  {!! $ad->$key !!}
                </div>
              </div>
            @endif
          @endforeach

        </div>
      </div>
      <div class="print-only">
        <div class="row mb-4">
          @foreach($ad->images as $image)
            <img class="col-3 mb-4" src="{{$image->getSize(350, 280)}}">
          @endforeach
        </div>
      </div>
    </div>
  </div><!-- primary-info-business -->

  <div class="help-for-business no-print">
    <div class="container">
      <div class="row">
        <div class="col-md">
          <div class="mb-4">
            <span class="title-business">Всегда готовы помочь</span>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-4 col-md-4 col-sm-6">
          <a class="help-card mix--legal-document" href="{{route('special.transaction-support')}}">
            <div class="help-description">Сопровождение сделок купли продажи бизнеса</div>
            <div class="help-price">от 25 000 ₽</div>
          </a>
        </div>

        <div class="col-lg-4 col-md-4 col-sm-6">
          <a class="help-card mix--graph" href="{{route('special.business-search')}}">
            <div class="help-description">Эксклюзивный поиск бизнеса под запрос покупателя</div>
            <div class="help-price">Бесплатно</div>
          </a>
        </div>

        <div class="col-lg-4 col-md-4 col-sm-6">
          <a class="help-card mix--secure-shield" href="{{route('special.risk-check')}}">
            <div class="help-description">Проверка рисков покупаемого бизнеса</div>
            <div class="help-price">от 25 000 ₽</div>
          </a>
        </div>
      </div>
    </div>
  </div> <!-- help -->

  @if(count($similarAds = $ad->getSimilarAdverts()))
    <div class="ready-business">
      <div class="container">
        <div class="row mb-4">
          <div class="col-md">
            <div>
              <span class="title-business">Будут интересны</span>
            </div>
          </div>
        </div>
        <div class="row">
          @foreach($similarAds as $similarAd)
            @include('site/parts/ad-card', ['ad' => $similarAd])
          @endforeach
        </div>
      </div>
    </div> <!-- ready-business -->
  @endif

@endsection
