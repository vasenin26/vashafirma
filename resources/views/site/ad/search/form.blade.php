<div class="form-business mb-4 tabs" data-module="Tabs" data-active-idx="{{$activeIdx ?? 0}}">

  <div class="form-business__nav">
    <ul class="nav">
      <li class="nav-item">
        <span class="nav-link tabs__toggle">Поиск по категории</span>
      </li>
      <li class="nav-item">
        <span class="nav-link tabs__toggle">Поиск по названию</span>
      </li>
    </ul>
  </div>

  <div class="form-business__form tab">
    {{Form::model($filters ?? [], ['method' => 'get'] + ($options ?? []))}}

    {{Form::hidden('order')}}

    <div class="form-business-show d-lg-flex p-4">
      <div class="choose-category-business">
        <span class="title-field">Выберите категорию бизнеса</span>
        {{Form::select(
          'category_id',
          [' ' => ' '] + App\EstateCategory::orderBy('idx', 'asc')->toTree(),
          null,
          [
          'class' => 'category-field'
          ]
        )}}
      </div>
      <div class="choose-price-business">
        <span class="title-field">Цена, ₽</span>
        <div class="choose-price-fields d-inline-flex align-items-center">
          {{Form::text('price_from', null, ['placeholder' => 'от', 'class' => 'choose-price-field'])}}
          <span class="black-stroke-between-fields"></span>
          {{Form::text('price_to', null, ['placeholder' => 'до', 'class' => 'choose-price-field'])}}
        </div>
      </div>
      <div class="choose-profit-business">
        <span class="title-field">Ожидаемая прибыль, ₽/мес.</span>
        {{Form::text('profit_from', null, ['placeholder' => 'от', 'class' => 'profit-field'])}}
      </div>
      <div class="show-business">
        <button type="submit" class="show-business-button">Показать</button>
      </div>
    </div>
    {{Form::close()}}
  </div>

  <div class="form-business__form tab">
    {{Form::model(app('request'), ['route' => 'ad.search', 'method' => 'get'])}}

    {{Form::hidden('order')}}

    <div class="form-business-show d-lg-flex p-4">
      <div class="choose-search-business">
        <span class="title-field">Название объекта</span>
        {{Form::text('s', null, ['placeholder' => '', 'class' => 'search-field'])}}
      </div>
      <div class="show-business">
        <button type="submit" class="show-business-button">Показать</button>
      </div>
    </div>
    {{Form::close()}}
  </div>

</div>
