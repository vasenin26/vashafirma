@extends('app', [
'meta_title' => 'Покупка бизнеса'
])

@section('content')

  @include('site/parts/header', [
    'title' => 'Покупка бизнеса',
    'current_title' => 'Поиск'
  ]);

  <div class="select-business">
    <div class="container">
      @include('site/ad/search/form', [
      'activeIdx' => $searchFormTabIdx ?? 0
      ])
    </div>
  </div>

  <div class="container">
    @if($ads->count())

      <div class="ready-business">
        <div class="row align-items-center mb-4">
          <div class="col-md d-flex justify-content-start">
            <span class="list-begin">Показать сначала</span>
            <a class="list-filter" href="{{route('ad.search', ['order' => 'price'])}}">дешевле</a>
            <a class="list-filter" href="{{route('ad.search', ['order' => 'profit'])}}">прибыльнее</a>
            <a class="list-filter" href="{{route('ad.search', ['order' => 'created'])}}">по дате</a>
          </div>
        </div>
        <div class="row">
          @foreach($ads as $ad)
            @include('site/parts/ad-card', ['ad' => $ad])
          @endforeach
        </div>
      </div> <!-- ready-business -->

      {{$ads->links('site.parts.pagination')}}

    @else
      <div class="alert alert-info">
        К сожалению по вашему запросу ничего не найдено, скорректируйте запрос или воспользуйтесь подбором по
        категориям.
      </div>
    @endif
  </div>

  <div class="container">
    <div class="content_">
      <p>
        У Вас есть желание приобрести действующий раскрученный бизнес?
      </p>
      <p>
        Позвоните нам сейчас или оставьте заявку на нашем сайте – наши специалисты бесплатно подберут наиболее
        подходящие Вам варианты, ответят на все вопросы заинтересовавших предложений, организуют просмотры, встречи с
        собственниками объектов, а так же полное юридическое и консультационное сопровождение. Мы работаем без комиссии
        для Покупателя и не гонимся за количеством объектов, для нас важнее качество сделок и наша репутация. Все
        объекты представленные в продаже – являются реальными и находятся в стадии «продажи», на все объекты выезжали
        наши бизнес-брокеры, которые обладают полной и достоверной информацией.
      </p>
    </div>
  </div>

@endsection