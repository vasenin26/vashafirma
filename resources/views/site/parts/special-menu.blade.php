<div class="special-menu">
  <ul class="nav flex-column">
    <li class="nav-item {{classActiveRoute('special.about', 'active-item')}}">
      <a class="nav-link" href="{{route('special.about')}}">О компании</a>
    </li>
    <li class="nav-item {{classActiveController('JobsController', 'active-item')}}">
      <a class="nav-link" href="{{route('jobs.index')}}">Вакансии</a>
    </li>
    <li class="nav-item {{classActiveController('ReviewController', 'active-item')}}">
      <a class="nav-link" href="{{route('reviews.index')}}">Отзывы</a>
    </li>
  </ul>
</div>