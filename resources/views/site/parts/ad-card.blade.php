<div class="col-lg-4 col-md-6 col-sm-6 col-12 mb-4">
  <div class="card-ready-business border">
    <a class="preview-ready-business" href="{{route('ad.show', $ad->id)}}">
      <img alt="" class="pic-business" src="{{$ad->getPreview()->getSize(359, 139)}}">
    </a>
    <div class="info-ready-business">
      <a class="title-ready-business" href="{{route('ad.show', $ad->id)}}">
        {{$ad->estate->title ?: $ad->estate->object_title}}
      </a>
      <span class="price-ready-business mb-2">@money($ad->price) ₽</span>
      <span class="profit-ready-business mb-2">Прибыль: @money($ad->profit) ₽/мес</span>
      <div>
        <span class="city-ready-business">{{$ad->region}}</span>
        {{--
        <span class="date-ready-business">{{$ad->created_at->format('Y-m-d')}}</span>
        --}}
      </div>
    </div>
  </div>
</div>