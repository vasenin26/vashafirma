@if ($paginator->hasPages())
  <div class="pagination-list">
    <div class="container">
      <div class="row">
        <div class="col-md col-sm">
          <nav>
            <ul class="pagination">
              {{-- Pagination Elements --}}
              @foreach ($elements as $element)
                {{-- "Three Dots" Separator --}}
                @if (is_string($element))
                  <li class="page-item dots-page-item">
                    <span class="page-link dots-page-item">...</span>
                  </li>
                @endif

                {{-- Array Of Links --}}
                @if (is_array($element))
                  @foreach ($element as $page => $url)
                    @if ($page == $paginator->currentPage())
                      <li class="page-item" aria-current="page">
                        <span class="page-link active-item">{{ $page }}</span>
                      </li>
                    @else
                      <li class="page-item">
                        <a class="page-link" href="{{ $url }}">{{ $page }}</a>
                      </li>
                    @endif
                  @endforeach
                @endif
              @endforeach
            </ul>
          </nav>
        </div>
        <div
            class="col-md col-sm-12 d-flex justify-content-sm-center justify-content-center justify-content-lg-end justify-content-md-end">
          <nav>
            <ul class="pagination">
              <li class="page-item">
              @if ($paginator->onFirstPage())
                <li class="page-item" aria-disabled="true">
                  <span class="page-link page-link--prev" aria-hidden="true">Назад</span>
                </li>
              @else
                <li class="page-item">
                  <a class="page-link page-link--prev" href="{{ $paginator->previousPageUrl() }}" rel="prev">Назад</a>
                </li>
              @endif
              <li class="page-item">
                <span class="page-link">|</span>
              </li>

              {{-- Next Page Link --}}
              @if ($paginator->hasMorePages())
                <li class="page-item">
                  <a class="page-link page-link--next" href="{{ $paginator->nextPageUrl() }}" rel="next">Далее</a>
                </li>
              @else
                <li class="page-item" aria-disabled="true">
                  <span class="page-link page-link--next" aria-hidden="true">Далее</span>
                </li>
              @endif
            </ul>
          </nav>
        </div>
      </div>
    </div>
  </div><!-- pagination-list -->
@endif