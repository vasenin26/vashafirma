<div class="col-12 col-lg-4 col-md-6 col-sm-6">
  <div class="article-card">
    <a href="{{route('articles.show', ['url' => $article->url])}}">
      <img class="article-card__preview" alt="" src="{{$article->preview->getSize(360, 207)}}">
    </a>
    <a href="{{route('articles.show', ['url' => $article->url])}}"
       class="article-card__title">{{$article->title}}</a>
  </div>
</div>