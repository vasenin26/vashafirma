<div class="help-for-business">
  <div class="container">
    <h3 class="title-business">Всегда готовы помочь</h3>

    <div class="row">

      <div class="col-lg-4 col-md-4 col-sm-6">
        <a class="help-card mix--briefcase" href="{{route('special.extracts')}}">
          <div class="help-description">Получение выписок из ЕГРЮЛ,ЕГРИП</div>
          <div class="help-price">500 ₽</div>
        </a>
      </div>

      <div class="col-lg-4 col-md-4 col-sm-6">
        <a class="help-card mix--graph" href="{{route('special.business-search')}}">
          <div class="help-description">Эксклюзивный поиск бизнеса под запрос покупателя</div>
          <div class="help-price">Бесплатно</div>
        </a>
      </div>

      <div class="col-lg-4 col-md-4 col-sm-6">
        <a class="help-card mix--secure-shield" href="{{route('special.risk-check')}}">
          <div class="help-description">Проверка рисков покупаемого бизнеса</div>
          <div class="help-price">от 25 000 ₽</div>
        </a>
      </div>

      <div class="col-lg-4 col-md-4 col-sm-6">
        <a class="help-card mix--legal-document" href="{{route('special.transaction-support')}}">
          <div class="help-description">Сопровождение сделок купли продажи бизнеса</div>
          <div class="help-price">от 25 000 ₽</div>
        </a>
      </div>

      <div class="col-lg-4 col-md-4 col-sm-6">
        <a class="help-card mix--file" href="{{route('special.registration-entities')}}">
          <div class="help-description">Регистрация юридических лиц</div>
          <div class="help-price">от 20 000 ₽</div>
        </a>
      </div>

      @if($call_btn ?? true === true)

        <div class="col-lg-4 col-md-4 col-sm-6">
          <div class="help-card-request" data-href="{{route('special.modal', 'callback')}}" data-modal>
            <span class="help-label d-inline-flex justify-content-center align-items-end">
              <span class="help-request">Оставить заявку</span>
              <div class="help-request-icon mb-2">
              </div>
            </span>
          </div>
        </div>

      @endif

    </div>
  </div>
</div> <!-- help-for-business -->