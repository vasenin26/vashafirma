<div class="page-header">
  <div class="container">
    <h1 class="page-header__title">{{$title ?? $meta_title}}</h1>

    <div class="breadcrumb-b">
      <ul class="nav">
        <li class="nav-item">
          <a class="nav-link-previous" href="/">Главная</a>
          <span class="point"></span>
        </li>
        @foreach($links ?? [] as $link)
          <li class="nav-item">
            <a class="nav-link-previous" href="{{$link['url']}}">{{$link['title']}}</a>
          </li>
        @endforeach
        <li class="nav-item">
          <span class="nav-link">{{$current_title ?? $title ?? $meta_title}}</span>
        </li>
      </ul>
    </div>

  </div>
</div><!-- title-page -->