<div class="col-md col-md-3">
  <div class="card-business mr-4 mb-4">
    <div class="categories-business mb-2">
      <a class="name-business" href="{{route('category.show', $category->alias)}}">{{$category->title}}
        <sup class="counter-business">{{$category->ad_counters}}</sup></a>
    </div>
    <span class="price-from-business">от {{$category->min_price}} ₽</span>
  </div>
</div>