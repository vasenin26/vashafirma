<div class="form mix-job-request">
  <div class="form__title">
    {{$request_form_title ?? 'Запрос на продажу бизнеса'}}
  </div>
  <form action="{{route('special.form.handler', 'sale_request')}}"
        method="POST"
        enctype="multipart/form-data"
        data-module="Form"
  >
    <input type="hidden" name="form_source" value="{{$request_form_title ?? 'Запрос на продажу бизнеса'}}">
    <div class="row">
      <div class="col-md-6 form-group">
        <input placeholder="Фамилия Имя Отчество" class="form-control" name="name">
      </div>
      <div class="col-md-6 form-group">
        <input placeholder="Телефон" class="form-control" name="phone">
      </div>
      <div class="col-md-6 form-group">
        <input placeholder="E-mail" class="form-control" name="email">
      </div>
      <div class="col-md-6 form-group">
        <button class="btn btn-block btn-primary" type="submit">Отправить</button>
      </div>
    </div>
  </form>

  <div class="personal-data-processing">
    Нажимая на кнопку "Отправить", вы даете согласие на
    <a href="#">обработку своих персональных данных</a>
  </div>
</div> <!-- form -->