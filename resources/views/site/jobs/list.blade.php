@extends('app')

@section('content')

  @include('site/parts/header', [
  'title' => 'Вакансии'
  ]);

  <div class="container">
    <div class="row">

      <div class="col-md order-1">

        @foreach($departments as $department)

          @if($department->jobs->count())

            <div class="department">
              <div class="department__title">{{$department->title}}</div>
              <div class="department__body">
                @foreach($department->jobs as $job)

                  <a class="department__job" href="{{route('jobs.show', $job->id)}}">
                    {{$job->job_name}}
                    <i></i>
                  </a>

                @endforeach
              </div>
            </div>

          @endif

        @endforeach

      </div>

      <div class="col-md-3 order-0">
        @include('site/parts/special-menu')
      </div>

    </div>
  </div><!-- content-with-vertical-list -->

@endsection
