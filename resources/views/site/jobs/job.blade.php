@extends('app')

@section('content')


  @include('site/parts/header', [
    'title' => 'Вакансии',
    'current_title' => $job->job_name,
    'links' => [
      [
        'title' => 'Вакансии',
        'url' => route('jobs.index'),
      ],
    ]
  ]);

  <div class="container">
    <div class="row">

      <div class="col-md order-1">

        <div class="job-single">
          <h2 class="job-single__title">
            {{$job->job_name}}
          </h2>
          <div class="job-single__description content_">
            {!! $job->job_description !!}
          </div>
        </div>

        <div class="form mix-job-request">
          <div class="form__title">
            Отклик на вакансию
          </div>
          <form action="{{route('special.form.handler', 'job_request')}}"
                method="POST"
                enctype="multipart/form-data"
                data-module="Form"
          >
            <div class="row">
              <div class="col-md-6 form-group">
                <input placeholder="Фамилия Имя Отчество" class="form-control" name="name">
              </div>
              <div class="col-md-6 form-group">
                <input placeholder="Телефон" class="form-control" name="phone">
              </div>
              <div class="col-md-6 form-group">
                <input placeholder="E-mail" class="form-control" name="email">
              </div>
              <div class="col-md-6 form-group">
                <input placeholder="Дополнительная информация" class="form-control" name="additional">
              </div>
              <div class="col-md-6 form-group">
                <label class="form-file">
                  Прикрепить файл
                  <input type="file" name="file">
                </label>
              </div>
              <div class="col-md-6 form-group">
                <button class="btn btn-block btn-primary" type="submit">Отправить</button>
              </div>
            </div>
          </form>

          <div class="personal-data-processing">
            Нажимая на кнопку "Отправить", вы даете согласие на
            <a href="#">обработку своих персональных данных</a>
          </div>
        </div> <!-- form -->

      </div>

      <div class="col-md-3 order-0">
        @include('site/parts/special-menu')
      </div>

    </div>
  </div><!-- content-with-vertical-list -->

@endsection