@extends('app')

@section('content')

  <div class="purchase-sale-business">
    <div class="container">
      <div class="row">
        <div class="col-lg-9 col-md">
          <div class="purchase-sale-block">
            <span class="big-tittle">
              Покупка и продажа готового <span>бизнеса</span>
            </span>
            <a class="button-buy-business d-inline-flex justify-content-center align-items-center p-2 mr-4"
               href="{{route('ad.search')}}">Купить
              бизнес
            </a>
            <a class="button-sell-business d-inline-flex justify-content-center align-items-center p-2"
               href="{{route('category.sale')}}">Продать
              бизнес
            </a>
          </div>
        </div>
        <div class="col-lg-3 col-md d-lg-flex flex-wrap">
          <div class="firm-stats-single">
            <span class="numbers-stat">181 375</span>
            <span class="text-stat">компаний доверились нам</span>
          </div>
          <div class="firm-stats-single">
            <span class="numbers-stat">1 983</span>
            <span class="text-stat">проданных бизнеса в месяц</span>
          </div>
          <div class="firm-stats-single">
            <span class="numbers-stat">10</span>
            <span class="text-stat">лет на рынке услуг</span>
          </div>
        </div>
      </div>
    </div>
  </div> <!-- Purchase-sale-business -->

  <div class="select-business">
    <div class="container">
      <div class="row">
        <div class="col">
          <div class="mt-4 mb-4">
            <span class="title-business">Подберите готовый бизнес от собственника</span>
          </div>
        </div>
      </div>
    </div>

    <div class="ready-business">
      <div class="container">
        <div class="row">
          @foreach($premium as $ad)
            @include('site/parts/ad-card', ['ad' => $ad])
          @endforeach
        </div>
      </div>
    </div>

    <div class="container">
      @include('site/ad/search/form', ['options' => ['route' => 'ad.search']]);
    </div> <!-- select-business -->

    <div class="category-business">
      <div class="container">
        <div class="row">
          <div class="col-md">
            <div class="mb-4">
              <span class="title-business">Категории бизнеса</span>
            </div>
          </div>
        </div>
        <div class="row">

          @foreach($categories as $category)
            @include('site/parts/category-card', [
            'category' => $category
            ])
          @endforeach
        </div>
      </div>
    </div> <!-- category-business -->

    <div class="ready-business">
      <div class="container">
        <div class="row align-items-right mb-4">
          <div
              class="col col-sm col-md-8 col-lg-6 col-xl-5 d-flex flex-wrap justify-content-between align-items-center">
            <span class="list-begin">Показать сначала</span>
            <a class="list-filter" href="{{route('ad.search', ['order' => 'price'])}}">дешевле</a>
            <a class="list-filter" href="{{route('ad.search', ['order' => 'profit'])}}">прибыльнее</a>
            <a class="list-filter" href="{{route('ad.search', ['price_to' => '2000000'])}}">до 2 млн.</a>
          </div>
        </div>
        <div class="row">
          @foreach($ads as $ad)
            @include('site/parts/ad-card', ['ad' => $ad])
          @endforeach
        </div>
        <div class="row justify-content-center">
          <a class="show-more" href="{{route('ad.search')}}">+ Показать все предложения</a>
        </div>
      </div>
    </div> <!-- ready-business -->

    @include('site/parts/help-for-business')

    <div class="container">
      <div class="row">
        <div class="col-md">
          <div class="mb-4">
            <span class="secondary-title">Все о покупке и продаже бизнеса</span>
          </div>
        </div>
      </div>
      <div class="row mb-4">
        @foreach($articles as $article)
          @include('site/parts/article-card', ['article' => $article])
        @endforeach
      </div>
      <div class="row">
        <div class="col-md">
          <a class="show-more" href="{{route('articles.index')}}">+ Показать все статьи</a>
        </div>
      </div>
    </div>
  </div>
@endsection