@extends('app')

@section('content')

  @include('site/parts/header', [
    'title' => $article->title,
    'current_title' =>$article->category->title
  ]);

  <div class="container">
    <div class="row">
      <div class="col-md-9 order-1">

        <div class="page-service"></div>

        <img src="{{$article->preview->getWidth(1024)}}" alt="" class="page-service__preview">

        <div class="page-service__content content_">
          {!! $article->text !!}
        </div>

        @include('site/parts/request-form')

      </div>
      <div class="col-md-3 order-0">
        <div class="special-menu">
          <ul class="nav flex-column">
            @foreach( \App\Article::whereHas( 'category', function($builder) use ($article){
            $builder->where('id', $article->category_id);
            })->get() as $menuItem)
              <li class="nav-item {{classActiveUri($menuItem->url, 'active-item')}}">
                <a class="nav-link" href="/{{$menuItem->url}}">{{$menuItem->title}}</a>
              </li>
            @endforeach
          </ul>
        </div>
      </div>
    </div>

  </div>

@endsection