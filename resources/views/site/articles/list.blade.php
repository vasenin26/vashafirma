@extends('app')

@section('content')

  @include('site/parts/header', [
    'title' => 'Статьи'
  ]);

  <div class="container">
    <div class="row">
      @foreach($articles as $article)
        @include('site/parts/article-card', ['article' => $article])
      @endforeach
    </div>
  </div>

  {!! $articles->links('site.parts.pagination') !!}

@endsection