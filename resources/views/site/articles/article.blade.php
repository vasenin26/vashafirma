@extends('app')

@section('content')

  @include('site/parts/header', [
    'title' => $article->title,
    'links' => [
      [
        'title' => 'Статьи',
        'url' => route('articles.index'),
      ],
    ]
  ]);

  <div class="image-article">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="pic-article">
            <img alt="" src="{{$article->preview->getSize(1139, 276)}}">
          </div>
        </div>
      </div>
    </div>
  </div><!-- image-article -->

  <div class="paragraph-article">
    <div class="container">
      <div class="row">
        <div class="col-md-8">
          {!! $article->text !!}
        </div>
        <div class="col-md-4">

          <div class="help-card mix--briefcase">
            <div class="help-description">Получение выписок из ЕГРЮЛ,ЕГРИП</div>
            <div class="help-price">500 ₽</div>
          </div>

        </div>
      </div>
    </div>
  </div><!-- paragraph-article -->

  <div class="articles-about-business">
    <div class="container">
      <div class="row">
        <div class="col-md">
          <div class="mb-4">
            <span class="title-business">Будут интересны</span>
          </div>
        </div>
      </div>
      <div class="row">

        @foreach($article->getSimilarArticles() as $similarArticle)
          @include('site/parts/article-card', ['article' => $similarArticle])
        @endforeach

      </div>
    </div>
  </div> <!-- articles-about-business -->

@endsection