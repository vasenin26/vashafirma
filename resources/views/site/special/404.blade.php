@extends('app')

@section('content')

  <div class="error-page">
    <div class="container">
      <div class="not-found-title">
        <span>404</span> такой страницы не существует
      </div>
      <div class="error-page__proposal">
        <div class="error-page__proposal-label secondary-title">Вероятно вы хотели</div>

        <div class="error-page__proposal-actions">
          <a class="btn btn-primary btn--large"
             href="{{route('ad.search')}}">Купить
            бизнес
          </a>
          <a class="btn btn-outline-primary btn--large"
             href="{{route('category.sale')}}">Продать
            бизнес
          </a>
        </div>

      </div>
    </div>
  </div>

  <div class="category-business category-business--single">
    <div class="container">
      <div class="row">
        <div class="col-md">
          <div class="mb-4">
            <span class="title-business">Выберите категорию бизнеса</span>
          </div>
        </div>
      </div>
      <div class="row">

        @foreach($categories as $category)

          <div class="col-md col-md-3">
            <div class="card-business mr-4 mb-4">
              <div class="categories-business mb-2">
                <a class="name-business" href="{{route('category.show', $category->id)}}">{{$category->title}}
                  <sup class="counter-business">{{$category->ad_counter}}</sup></a>
              </div>
              <span class="price-from-business">от {{$category->min_price}} ₽</span>
            </div>
          </div>

        @endforeach
      </div>
    </div>
  </div> <!-- category-business -->

  <div class="container">
    <div class="row">
      <div class="col-md">
        <div class="mb-4">
          <span class="secondary-title">Все о покупке и продаже бизнеса</span>
        </div>
      </div>
    </div>
    <div class="row mb-4">
      @foreach($articles as $article)
        @include('site/parts/article-card', ['article' => $article])
      @endforeach
    </div>
    <div class="row">
      <div class="col-md">
        <a class="show-more" href="{{route('articles.index')}}">+ Показать все статьи</a>
      </div>
    </div>
  </div>

@endsection
