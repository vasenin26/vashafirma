@extends('app')

@section('content')

  @include('site/parts/header', [
  'title' => 'Получение выписок из ЕГРЮЛ, ЕГРИП (срочных и обычных).',
  'links' => [
      [
      'title' => 'Продажа бизнеса',
      'url' => '/catalog/sale'
      ]
    ]
  ]);

  <div class="container">
    <div class="row">

      <div class="col-md order-1">

        <div class="page-service"></div>

        <img src="/examples/sales-support.jpg" alt="" class="page-service__preview">

        <div class="page-service__content content_">

          <div class="price-line">
            <div class="row">
              <div class="col">
                Стоимость
              </div>
              <div class="col">
                <del>3 500 р</del>
                <span>
                 от 2 000 р.
                  </span>
              </div>
            </div>
          </div>

          <p>
            Наши специалисты оперативно подготовят запрос в МИФНС на получения выписки из ЕГРЮЛ, ЕРГИП, оплатят государственную пошлину, получать в регистрирующем органе выписку.
          </p>

        </div>

        @include('site/parts/request-form', [
        'request_form_title' => 'Получить выписки',
        ])

      </div>

      <div class="col-md-3 order-0">
        @include('site/special/services/menu')
      </div>

    </div>
  </div><!-- content-with-vertical-list -->

@endsection
