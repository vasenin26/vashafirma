@extends('app')

@section('content')

  @include('site/parts/header', [
  'title' => 'Сопровождение сделок купли продажи бизнеса',
  'links' => [
      [
      'title' => 'Продажа бизнеса',
      'url' => '/catalog/sale'
      ]
    ]
  ]);

  <div class="container">
    <div class="row">

      <div class="col-md order-1">

        <div class="page-service"></div>

        <img src="/examples/sales-support.jpg" alt="" class="page-service__preview">

        <div class="page-service__content content_">

          <div class="price-line">
            <div class="row">
              <div class="col">
                Стоимость
              </div>
              <div class="col">
                <del>45 000 р</del>
                <span>
                от 25 000 р.
                  </span>
              </div>
            </div>
          </div>

          <p>
            Вы самостоятельно нашли бизнес, который захотели приобрести. Не спешите расставаться с деньгами,
            минимизируйте свои риски. Закажите профессиональное юридическое сопровождение
            сделки купли - продажи бизнеса в Агентстве Бизнеса «ВАША ФИРМА».
          </p>

        </div>

        @include('site/parts/request-form', [
        'request_form_title' => 'Сопровождение сделок',
        ])

      </div>

      <div class="col-md-3 order-0">
        @include('site/special/services/menu')
      </div>

    </div>
  </div><!-- content-with-vertical-list -->

@endsection
