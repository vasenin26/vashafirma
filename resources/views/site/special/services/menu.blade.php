<div class="special-menu">
  <ul class="nav flex-column">
    <li class="nav-item {{classActiveRoute('special.extracts', 'active-item')}}">
      <a class="nav-link" href="{{route('special.extracts')}}">Получение выписок из ЕГРЮЛ,ЕГРИП</a>
    </li>
    <li class="nav-item {{classActiveRoute('special.business-search', 'active-item')}}">
      <a class="nav-link" href="{{route('special.business-search')}}">Эксклюзивный поиск бизнеса под запрос
        покупателя</a>
    </li>
    <li class="nav-item {{classActiveRoute('special.risk-check', 'active-item')}}">
      <a class="nav-link" href="{{route('special.risk-check')}}">Проверка рисков покупаемого бизнеса</a>
    </li>
    <li class="nav-item {{classActiveRoute('special.transaction-support', 'active-item')}}">
      <a class="nav-link" href="{{route('special.transaction-support')}}">Сопровождение сделок купли продажи бизнеса</a>
    </li>
    <li class="nav-item {{classActiveRoute('special.registration-entities', 'active-item')}}">
      <a class="nav-link" href="{{route('special.registration-entities')}}">Регистрация юридических лиц</a>
    </li>
  </ul>
</div>