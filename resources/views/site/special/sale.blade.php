@extends('app')

@section('content')

  @include('site/parts/header');

  <div class="page-information">
    <div class="container content_">
      <p>
        У Вас есть желание быстро и выгодно продать свой бизнес?
      </p>
      <p>
        Оставьте заявку на нашем сайте или позвоните нам сейчас – наши специалисты могут предложить гибкие условия сотрудничества, обширную рекламную кампанию по продаже Вашего бизнеса, полное юридическое и консультационное сопровождение, персонального бизнес-брокера и огромную клиентскую базу потенциальных Покупателей.
      </p>
    </div>
  </div>

  <div class="container">
    <div class="row">
      <div class="col-md-8">
        @include('site/parts/request-form')
      </div>
    </div>
  </div>

  @include('site/parts/help-for-business', ['call_btn' => false])

@endsection