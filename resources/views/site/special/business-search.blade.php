@extends('app')

@section('content')

  @include('site/parts/header', [
  'title' => 'Эксклюзивный поиск бизнеса под запрос',
  'links' => [
      [
      'title' => 'Продажа бизнеса',
      'url' => '/catalog/sale'
      ]
    ]
  ]);

  <div class="container">
    <div class="row">

      <div class="col-md order-1">

        <div class="page-service"></div>

        <img src="/examples/sales-support.jpg" alt="" class="page-service__preview">

        <div class="page-service__content content_">

          <div class="price-line">
            <div class="row">
              <div class="col">
                Стоимость
              </div>
              <div class="col">
                <span>
                Бесплатно
                  </span>
              </div>
            </div>
          </div>

          <p>
            Наша компания подберет подходящие варианты, согласно Вашим запросам и критериям. Мы организует просмотры, встречи с собственниками объектов, а так же полное юридическое и консультационное сопровождение.
          </p>

        </div>

        @include('site/parts/request-form', [
        'request_form_title' => 'Запросить поиск бизнеса',
        ])

      </div>

      <div class="col-md-3 order-0">
        @include('site/special/services/menu')
      </div>

    </div>
  </div><!-- content-with-vertical-list -->

@endsection
