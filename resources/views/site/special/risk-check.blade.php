@extends('app')

@section('content')

  @include('site/parts/header', [
  'title' => 'Проверка рисков покупаемого бизнеса',
  'links' => [
      [
      'title' => 'Продажа бизнеса',
      'url' => '/catalog/sale'
      ]
    ]
  ]);

  <div class="container">
    <div class="row">

      <div class="col-md order-1">

        <div class="page-service"></div>

        <img src="/examples/sales-support.jpg" alt="" class="page-service__preview">

        <div class="page-service__content content_">

          <div class="price-line">
            <div class="row">
              <div class="col">
                Стоимость
              </div>
              <div class="col">
                <del>40 000 р </del>
                <span>
                от 25 000 ₽
                  </span>
              </div>
            </div>
          </div>

          <p>
            Не покупайте кота в мешке. Закажите оценку найденного самостоятельно предложения бизнеса профессионалам компании «Ваша Фирма». Мы проведем всестороннюю правовую экспертизы учредительных документов компании, правоустанавливающих документов. Проведем финансовый анализ . Определим риски и сформируем рекомендации по их минимизации.
          </p>

        </div>

        @include('site/parts/request-form', [
        'request_form_title' => 'Проверить риски покупаемого бизнеса',
        ])

      </div>

      <div class="col-md-3 order-0">
        @include('site/special/services/menu')
      </div>

    </div>
  </div><!-- content-with-vertical-list -->

@endsection
