@extends('app')

@section('content')

  @include('site/parts/header', [
  'title' => 'Регистрация юридических лиц',
  'links' => [
      [
      'title' => 'Продажа бизнеса',
      'url' => '/catalog/sale'
      ]
    ]
  ]);

  <div class="container">
    <div class="row">

      <div class="col-md order-1">

        <div class="page-service"></div>

        <img src="/examples/sales-support.jpg" alt="" class="page-service__preview">

        <div class="page-service__content content_">

          <div class="price-line">
            <div class="row">
              <div class="col">
                Стоимость
              </div>
              <div class="col">
                <del>30 000 р</del>
                <span>
                 от 20 000 р.
                  </span>
              </div>
            </div>
          </div>

          <p>
            Регистрация юридических лиц (ООО, ИП), внесение изменений в учредительные документы, отчуждение долей в ООО через нотариальную контору.
          </p>
          <p>
            Наши специалисты подготовят комплект документов необходимый для подачи в регистрирующие органы в кратчайший срок.
          </p>

        </div>

        @include('site/parts/request-form', [
        'request_form_title' => 'Регистрация юридических лиц',
        ])

      </div>

      <div class="col-md-3 order-0">
        @include('site/special/services/menu')
      </div>

    </div>
  </div><!-- content-with-vertical-list -->

@endsection
