@extends('app')

@section('content')

  @include('site.parts.header', [
    'title' => 'Контакты'
  ]);

  <div class="contacts-content">
    <div class="container">
      <div class="row">
        <div class="col-md">
          <div class="contacts-map"
               data-module="Map"
               data-center="59.941550,30.352584"
               data-pointer="59.941550,30.352584"
               data-zoom="17"
          ></div>
        </div>
      </div>
    </div>
  </div><!-- contacts-content -->

  <div class="contacts-info">
    <div class="container">
      <div class="row">
        <div class="col-md-5">
          <div class="contacts-data">
            <span class="header-contacts mb-2">Адрес</span>
            <span class="city mb-2">Санкт-Петербург, ул. Артиллерейская, д. 1, литер А, офис 511</span>
            <div class="street">
              <span class="point mr-1"></span>
              <span class="address mb-4">Чернышевская</span>
            </div>
          </div>
        </div>
        <div class="col-md-3">
          <div class="contacts-data">
            <span class="header-contacts mb-2">Телефон</span>
            <span class="phone-number mb-2">+7 (812) 923-13-77</span>
          </div>
        </div>
        <div class="col-md-4 d-flex justify-content-sm-center justify-content-center justify-content-lg-end">
          <span data-href="{{route('special.modal', 'callback')}}" data-modal class="write-message">
            Написать сообщение
          </span>
        </div>
      </div>
    </div>
  </div><!-- contacts-info -->

@endsection