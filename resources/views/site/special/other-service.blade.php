@extends('app', [
'meta_title' => 'Услуги'
])

@section('content')

  @include('site/parts/header', [
  'meta_title' => 'Услуги'
  ])

  <div class="page-information">
    <div class="container content_">
      <p>
        По статистике большая часть собсвеннимков не знает как правильно оценить, сформировать интересное предложение
        для
        продажи своего бинеса и провести необходимую предпродажную подготовку.
      </p>
      <p>
        Наша компания готова придти Вам на помощь и взять на себя всю самую сложную работу по оценке, подготовке и
        продаже
        Вашего бизнеса
      </p>
    </div>
  </div>

  <div class="container">
    <div class="row">
      <div class="col-md-8">
        @include('site/parts/request-form')
      </div>
    </div>
  </div>

  @include('site/parts/help-for-business', ['call_btn' => false])

@endsection