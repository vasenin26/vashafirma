@extends('app')

@section('content')

  @include('site/parts/header', [
    'title' => 'Отзывы'
  ]);

  <div class="container">
    <div class="row">

      <div class="col-md order-1">
        @foreach ($reviews as $review)
          <div class="review-b">
            {{--<span class="review-b__download"></span>--}}
            <div class="review-b__content">
              {{$review->content}}
            </div>
            <div class="review-b__author">
              {{$review->author}}
            </div>
          </div>
        @endforeach
      </div>

      <div class="col-md-3 order-0">
        @include('site/parts/special-menu')
      </div>

    </div>
  </div>

@endsection