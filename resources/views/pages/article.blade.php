@extends('app')

@section('content')

  <div class="title-page">
    <div class="container title-block">
      <div class="row">
        <div class="col-md">
          <div class="mt-4 mb-2">
            <span class="name">Изменения для бизнеса с 1 января 2019 года</span>
          </div>
          <div class="mb-4">
            <ul class="nav">
              <li class="nav-item mr-2">
                <a class="nav-link-previous mr-1" href="#">Главная</a>
                <span class="point"></span>
              </li>
              <li class="nav-item mr-2">
                <a class="nav-link" href="#">Статьи</a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div><!-- title-articles -->

  <div class="image-article">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="pic-article">
            <img alt="" src="{{asset('examples/changes-for-business.jpg')}}">
          </div>
        </div>
      </div>
    </div>
  </div><!-- image-article -->

  <div class="paragraph-article">
    <div class="container">
      <div class="row">
        <div class="col-md-8 col-lg-8">
          <span class="header mb-2"><span class="number-header-paragraph mr-1">1.</span>Зарегистрировать ИП или ООО через интернет можно будет бесплатно</span>
          <span class="text-paragraph mb-2">С 1 января 2019 года при отправке документов в налоговую через интернет не придется платить за госрегистрацию:</span>
          <ul class="enumeration-paragraph-article mb-2">
            <li class="item-enumeration-paragraph mb-1">
              <span class="dash-enumeration mr-4"></span>
              <span class="text-enumeration">юрлиц</span>
            </li>
            <li class="item-enumeration-paragraph mb-1">
              <span class="dash-enumeration mr-4"></span>
              <span class="text-enumeration">изменений в учредительных документах</span>
            </li>
            <li class="item-enumeration-paragraph mb-1">
              <span class="dash-enumeration mr-4"></span>
              <span class="text-enumeration">ликцидации компании вне процедуры банкротства</span>
            </li>
            <li class="item-enumeration-paragraph mb-1">
              <span class="dash-enumeration mr-4"></span>
              <span class="text-enumeration">ИП</span>
            </li>
            <li class="item-enumeration-paragraph mb-1">
              <span class="dash-enumeration mr-4"></span>
              <span class="text-enumeration">прекращение деятельности ИП</span>
            </li>
          </ul>
          <span class="text-paragraph mb-2">В настоящее время размер пошлины за регистрацию юрлица составляет 4 тысячи рублей, изменений в учредительных документах/ликвидации компании/ИП - 800 рублей, прекращения деятельности ИП - 160 рублей</span>
        </div>
        <div class="col-md-4 col-lg-4">
          <div class="help-card p-4 mr-4">
            <div class="help-icon mb-2">
              <img alt="" src="{{asset('imgs/icons/briefcase.png')}}">
            </div>
            <span class="help-description mb-2">Сопровождение сделок купли продажи бизнеса</span>
            <span class="help-price">от 25 000 ₽</span>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-8">
          <div class="list-paragraph-article mt-4">
            <span class="header mb-2"><span class="number-header-paragraph mr-1">2.</span>Государство будет страховать банковские вклады и счета малого бизнеса</span>
            <span class="text-paragraph mb-2">С января 2019 года действие системы страхования вкладов распространится на микро и малые предприятия. Если Центробанк отзовет
            у банка лицензию или введет мораторий на удовлетворение требований кредиторов, компания сможет быстро вернуть деньги в пределах 1,4 млн рублей вне зависимости от того,
            где размещены средства: во вкладе или на счете. Страховое агенство переведет деньги на счет, который компания укажет в заявлении о выплате. В настоящее время система страхования
            работает только для физлиц и ИП.</span>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-8 col-lg-8">
          <div class="list-paragraph-article mt-4">
            <span class="header mb-2"><span class="number-header-paragraph mr-1">3.</span>Основная ставка НДС вырастет до 20%</span>
            <span class="text-paragraph mb-2">С нового года для отгрузок будут применяться следующие ставки:</span>
            <ul class="enumeration-paragraph-article mb-2">
              <li class="item-enumeration-paragraph mb-1">
                <span class="dash-enumeration mr-4"></span>
                <span class="text-enumeration">20% - основная ставка (вместо 18%)</span>
              </li>
              <li class="item-enumeration-paragraph mb-1">
                <span class="dash-enumeration mr-4"></span>
                <span class="text-enumeration">20/120 - расчетная ставка (вместо 18/118)</span>
              </li>
              <li class="item-enumeration-paragraph mb-1">
                <span class="dash-enumeration mr-4"></span>
                <span class="text-enumeration">16,67% - расчетная ставка (вместо 15,25%)</span>
              </li>
            </ul>
            <span class="text-paragraph mb-2">Другие ставки НДС, 10% и 0%) не изменятся. Подробнее о том, как считать НДС в переходный период - в статье.</span>
          </div>
        </div>
        <div class="col-md-4 col-lg-4">
          <div class="help-card p-4 mr-4">
            <div class="help-icon mb-2">
              <img alt="" src="{{asset('imgs/icons/legal-document.png')}}">
            </div>
            <span class="help-description mb-2">Проверка рисков покупаемого бизнеса</span>
            <span class="help-price mb-2">от 25 000 ₽</span>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-8">
          <div class="list-paragraph-article mt-4">
            <span class="header mb-2"><span class="number-header-paragraph mr-1">4.</span>Компании больше не будут платить налог на движимое имущество</span>
            <span class="text-paragraph mb-2">Неважно, подпадало ли до 2019 года ваше движимое имущество под льготу или нет. С января налог на имущество организаций будет применяться только в отношении недвижимости.</span>
          </div>
        </div>
      </div>
    </div>
  </div><!-- paragraph-article -->

  <div class="articles-about-business">
    <div class="container">
      <div class="row">
        <div class="col-md">
          <div class="mb-4">
            <span class="title-business">Будут интересны</span>
          </div>
        </div>
      </div>
      <div class="row mb-4">
        <div class="col-12 col-lg-4 col-md-6 col-sm-6">
          <img alt="preview-articles-about-business" class="preview-articles-about-business"
               src="{{asset('examples/review-of-changes.jpg')}}">
          <span class="title-articles">Обзор изменений бизнес-законодательства в 2018 и 2019 годах</span>
        </div>
        <div class="col-12 col-lg-4 col-md-6 col-sm-6">
          <img alt="preview-articles-about-business" class="preview-articles-about-business"
               src="{{asset('examples/how-to-do-business.jpg')}}">
          <span class="title-articles">Как вести дела с налоговой и не потерять деньги?</span>
        </div>
        <div class="col-12 col-lg-4 col-md-6 col-sm-6">
          <img alt="preview-articles-about-business" class="preview-articles-about-business"
               src="{{asset('examples/how-to-do-business.jpg')}}">
          <span class="title-articles">Как вести дела с налоговой и не потерять деньги?</span>
        </div>
      </div>
    </div>
  </div> <!-- articles-about-business -->

@endsection