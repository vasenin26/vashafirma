@extends('app')

@section('content')

  <div class="title-page">
    <div class="container title-block">
      <div class="row">
        <div class="col-md">
          <div class="mt-4 mb-2">
            <span class="name">Услуги</span>
          </div>
          <div class="mb-4">
            <ul class="nav">
              <li class="nav-item mr-2">
                <a class="nav-link-previous mr-1" href="#">Главная</a>
                <span class="point"></span>
              </li>
              <li class="nav-item mr-2">
                <a class="nav-link-previous mr-1" href="#">Услуги</a>
                <span class="point"></span>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#">Сопровождене сделок купли и продажи бизнеса</a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div><!-- title-page -->

  <div class="content-with-vertical-list">
    <div class="container">
      <div class="row">
        <div class="col-md-4">
          <div class="vertical-list">
            <ul class="nav flex-column">
              <li class="nav-item active-item">
                <a class="nav-link" href="#">Сопровождение сделок купли продажи бизнеса</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#">Поиск бизнеса под запрос покупателя</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#">Проверка рисков покупаемого бизнеса</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#">Регистрация юридических лиц</a>
              </li>
              <li class="nav-item final-item">
                <a class="nav-link" href="#">Получение выписок из ЕГРЮЛ, ЕГРИП</a>
              </li>
            </ul>
          </div>
        </div>
        <div class="col-md">
          <span class="title-content mb-4">Сопровождение сделок купли продажи бизнеса</span>
          <div class="pic-content mb-3">
            <img alt="" src="{{asset('examples/sales-support.jpg')}}">
          </div>
          <div class="price-and-request mb-4">
            <div class="price-service p-2">
              <span class="cost-service">Стоимость</span>
              <span class="price-number-service">от 25 000 ₽</span>
            </div>
          </div>
          <div class="row">
            <div class="col-md">
              <span class="primary-content-text">
                 Вы самостоятельно нашли бизнес, который захотели приобрести. Не спешите расставаться с деньгами,
                 минимизируйте свои риски. Закажите профессиональное юридическое сопровожедие
                 сделки купли - продажи бизнеса в Агенстве Бизнеса «ВАША ФИРМА».
              </span>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div><!-- content-with-vertical-list -->

  <div class="service-request-form">
    <div class="container">
      <div class="row d-flex justify-content-end">
        <div class="col-md-9">
          <div class="service-request-form-block p-4">
            <form action="#" method="POST">
              <span class="title-service-request-form">
                Оставьте заявку на услугу
              </span>
              <div class="fields-service-request-form">
                <div class="line-fields-service-request-form mb-4">
                  <input placeholder="Ваше имя" type="text" class="form-control">
                  <input placeholder="Телефон" type="text" class="form-control">
                </div>
                <div class="line-fields-service-request-form mb-4">
                  <input placeholder="E-mail" type="text" class="form-control">
                  <button class="submit-request" type="submit">Отправить</button>
                </div>
              </div>
            </form>
            <span class="personal-data-processing">
                Нажимая на кнопку "Задать вопрос", вы даете согласие на
                <a href="#">обработку своих персональных данных</a>
              </span>
          </div>
        </div>
      </div>
    </div>
  </div><!-- service-request-form -->

@endsection