@extends('app')

@section('content')

  <div class="title-page">
    <div class="container title-block">
      <div class="row">
        <div class="col-md">
          <div class="mt-4 mb-2">
            <span class="name">Студия ногтевого сервиса</span>
          </div>
          <div class="mb-4">
            <ul class="nav">
              <li class="nav-item mr-2">
                <a class="nav-link mr-1" href="#">Главная</a>
                <span class="point mr-1"></span>
              </li>
              <li class="nav-item mr-2">
                <a class="nav-link mr-1" href="#">Покупка</a>
                <span class="point mr-1"></span>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#">Медицина и косметология</a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div><!-- title-card -->

  <div class="primary-info-business">
    <div class="container business-preview-block">
      <div class="row">
        <div class="col-md-10 col-lg-8">
          <div class="slider-photo-business">
            <a href="#" class="arrow-slider-photo-business left-arrow-icon">
              <span class="left-arrow-slider"></span>
            </a>
            <a href="#" class="arrow-slider-photo-business right-arrow-icon">
              <span class="right-arrow-slider"></span>
            </a>
            <img alt="" src="{{asset('examples/nail-business-photo.jpg')}}">
          </div>
          <div class="row">
            <div class="col-md">
              <div class="business-categories">
                <span class="name-business-category">Категория:</span>
                <span class="data-business-category">Медицина и косметологоия</span>
              </div>
              <div class="business-categories">
                <span class="name-business-category">Сфера:</span>
                <span class="data-business-category">Ногтевой сервис</span>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-6 col-lg-4 col-sm-8 col-11">
          <div class="short-info-business-sold">
            <div class="primary-short-info-business">
              <div class="numbers">
                <span class="name">Цена</span>
                <span class="data">4 950 000 ₽</span>
              </div>
              <div class="numbers">
                <span class="name">Прибыль</span>
                <span class="data">300 000 ₽/мес</span>
              </div>
              <div class="location">
                <div class="city">
                  <span class="name">Город:</span>
                  <span class="data">
                          Санкт-Петербург
                        </span>
                </div>
                <div class="street">
                  <span class="point mr-1"></span>
                  <span class="address">Пр.Ветеранов</span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div><!-- primary-info-business -->

  <div class="description-business">
    <div class="container">
      <div class="row">
        <div class="col-md-8">
          <span class="header mt-4">Описание</span>
          <span class="text mb-2">К продаже предлагается два бутика кожгалантереи, чемоданов, сумов и аксессуаров под известным, брендом, с эксклюзивным правом продажи товаров. Оба магазина расположены в популярных ТРК Санкт-Петербурга</span>
          <div class="short-description">
            <div class="mb-1">
              <span class="name">Организационно-правовая форма:</span>
              <span class="text">ООО</span>
            </div>
            <div class="mb-1">
              <span class="name">Срок существования:</span>
              <span class="text">15 лет</span>
            </div>
            <div class="mb-1">
              <span class="name">Общее кличество персонала:</span>
              <span class="text">6 человек</span>
            </div>
            <div class="mb-1">
              <span class="name">Причина продажи:</span>
              <span class="text">переезд собственника</span>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div><!-- description-business -->

  <div class="finance-description-business">
    <div class="container">
      <div class="row">
        <div class="col-md">
          <span class="header mt-4">Финансы</span>
          <div class="short-description">
            <div class="mb-1">
              <span class="name">Цена:</span>
              <span class="text">2 000 000 ₽</span>
            </div>
            <div class="mb-1">
              <span class="name">Выручка/мес:</span>
              <span class="text">1 250 000 ₽</span>
            </div>
            <div class="mb-1">
              <span class="name">Прибыль/мес:</span>
              <span class="text">200 000 ₽</span>
            </div>
            <div class="mb-1">
              <span class="name">Окупаемость:</span>
              <span class="text">2 года</span>
            </div>
            <div class="mb-1">
              <span class="name">Среднемесячные обороты:</span>
              <span class="text">5 000 000 ₽</span>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div><!-- finance-description-business -->

  <div class="property-description-business">
    <div class="container">
      <div class="row">
        <div class="col-lg-8">
          <span class="header mt-4 mb-2">Недвижимость</span>
          <span class="text mb-4">1-й бутик расположен в районе ст.м Площадь Александра Невского. Занимаемая площадь магазина 78,6 кв.м, арендная ставка с КУ в месяц составляет 212 т.р. Срок заключения договора аренды 11 месяцев с пролонгацией</span>
          <span class="text">2-й бутик распроложен в районе с.тм Парк Победы. Занимаемая площадь магазина 48 кв.м, арендная ставка с КУ в месяц составляет 190 т.р. Срок заключения договора аренды 5 лет с пролонгацией.</span>
        </div>
        <div class="col-lg-4 col-md-8 col-sm-8 col-8">
          <img class="preview-articles-about-business" alt="" src="{{asset('examples/how-to-do-business.jpg')}}">
          <span class="title-articles">Как вести дела с налоговой и не потерять деньги?</span>
        </div>
      </div>
    </div>
  </div><!-- property-description-business -->

  <div class="production-description-business">
    <div class="container">
      <div class="row">
        <div class="col-md-8">
          <span class="header mt-4">Средства производства</span>
          <span class="text">Оба бутика оснащены всем необходимым мебель, торговое, кассовое оборудование и т.п.</span>
        </div>
      </div>
    </div>
  </div><!-- production-description-business -->

  <div class="personal-description-business">
    <div class="container">
      <div class="row">
        <div class="col-md-8">
          <span class="header mt-4">Персонал</span>
          <span class="text">В штате компании работает 15 сотрудников</span>
        </div>
      </div>
    </div>
  </div><!-- personal-description-business -->

  <div class="additional-info-description-business">
    <div class="container">
      <div class="row">
        <div class="col-md-8">
          <span class="header mt-4 mb-2">Дополнительная информация</span>
          <span class="text">В стоимость бизнеса входит: товарный остаток на сумму 1,4 млн. рублей по закупочной стоимоти, депозит по аренде в размере 400 т.р. Собственник бизнеса окажет всю необходимую помощь и поддержку покупателю.</span>
        </div>
      </div>
    </div>
  </div><!-- additional-info-description-business -->

  <div class="benefit-description-business">
    <div class="container">
      <div class="row">
        <div class="col-md-8">
          <div class="list-benefit">
            <span class="header">Выгоды для покупателя</span>
            <ul class="benefits">
              <li class="item-benefit">
                Стабильная и высокая прибыль;
              </li>
              <li class="item-benefit">
                Узнаваемый бренд и лояльность покупателей;
              </li>
              <li class="item-benefit">
                Неработанная клиентская база
              </li>
              <li class="item-benefit">
                Отличное местоположение магазинов, как в самих комплексах, так и популярность и известность ТК!
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div><!-- benefit-description-business -->

  <div class="help-for-business">
    <div class="container">
      <div class="row">
        <div class="col-md">
          <div class="mb-4">
            <span class="title-business">Всегда готовы помочь</span>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-4 col-md-4 col-sm-6">
          <div class="help-card p-4 mb-4">
            <div class="help-icon mb-2">
              <img alt="" src="{{asset('imgs/icons/file.png')}}">
            </div>
            <span class="help-description mb-2">Получение выписок из ЕГРЮЛ,ЕГРИП</span>
            <span class="help-price">500 ₽</span>
          </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-6">
          <div class="help-card p-4 mb-4">
            <div class="help-icon mb-2">
              <img alt="" src="{{asset('imgs/icons/graph.png')}}">
            </div>
            <span class="help-description mb-2">Эксклюзивный поиск бизнеса под запрос покупателя</span>
            <span class="help-price">Бесплатно</span>
          </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-6">
          <div class="help-card p-4 mb-4">
            <div class="help-icon mb-2">
              <img alt="" src="{{asset('imgs/icons/secure-shield.png')}}">
            </div>
            <span class="help-description mb-2">Проверка рисков покупаемого бизнеса</span>
            <span class="help-price">от 25 000 ₽</span>
          </div>
        </div>
      </div>
    </div>
  </div> <!-- help -->

  <div class="ready-business">
    <div class="container">
      <div class="row mb-4">
        <div class="col-md">
          <div>
            <span class="title-business">Будут интересны</span>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-4 col-md-6 col-sm-6 col-12 mb-4">
          <div class="card-ready-business border">
            <div class="preview-ready-business">
              <img alt="" class="pic-business" src="{{asset('examples/door-business.jpg')}}">
            </div>
            <div class="info-ready-business">
              <span class="title-ready-business">Производство мебели и дверей из массива</span>
              <span class="price-ready-business mb-2">4 950 000 ₽</span>
              <span class="profit-ready-business mb-2">Прибыль: 300 000 ₽/мес</span>
              <div>
                <span class="city-ready-business">Санкт-Петербург</span>
                <span class="date-ready-business">6.06.2019</span>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-6 col-12 mb-4">
          <div class="card-ready-business border">
            <div class="preview-ready-business">
              <div class="best-offer-business">
                Лучшее предложение
              </div>
              <img alt="" class="pic-business" src="{{asset('examples/nails-business.jpg')}}">
            </div>
            <div class="info-ready-business">
              <span class="title-ready-business">Студия ногтевого сервиса</span>
              <span class="price-ready-business mb-2">450 000 ₽</span>
              <span class="profit-ready-business mb-2">Прибыль: 300 000 ₽/мес</span>
              <div>
                <span class="city-ready-business">Санкт-Петербург</span>
                <span class="date-ready-business">6.06.2019</span>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-6 col-12 mb-4">
          <div class="card-ready-business border">
            <div class="preview-ready-business">
              <img alt="" class="pic-business" src="{{asset('examples/depilation-business.jpg')}}">
            </div>
            <div class="info-ready-business">
              <span class="title-ready-business">Салон депиляции</span>
              <span class="price-ready-business mb-2">1 950 000 ₽</span>
              <span class="profit-ready-business mb-2">Прибыль: 230 000 ₽/мес</span>
              <div>
                <span class="city-ready-business">Санкт-Петербург</span>
                <span class="date-ready-business">6.06.2019</span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div> <!-- ready-business -->

@endsection