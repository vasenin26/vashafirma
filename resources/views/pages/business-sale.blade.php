@extends('app')

@section('content')

  <div class="title-page">
    <div class="container title-block">
      <div class="row">
        <div class="col-md">
          <div class="mt-4 mb-2">
            <span class="name">Продажа бизнеса</span>
          </div>
          <div class="mb-4">
            <ul class="nav">
              <li class="nav-item mr-2">
                <a class="nav-link-previous mr-1" href="#">Главная</a>
                <span class="point"></span>
              </li>
              <li class="nav-item mr-2">
                <a class="nav-link mr-1" href="#">Продажа-бизнеса</a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div><!-- title-page -->

  <div class="description-business-sale">
    <div class="container">
      <div class="row">
        <div class="col-md-8">
          <div class="description-text">
            <span class="text mb-2">По статистике большая часть собственников не знает как правильно оценить, сформировать интересное предложение для продажи своего бизнеса и провести необходимую предпродажную подготовку
          </span>
            <span class="text">Наша компания готова придти Вам на помощь и взять на себя всю самую сложную работу по оценке, подготовке и продаже Вашего бизнеса</span>
          </div>
        </div>
      </div>
    </div>
  </div><!-- description-business-sale -->

  <div class="help-for-business">
    <div class="container">
      <div class="row">
        <div class="col-lg-4 col-md-4 col-sm-6">
          <div class="help-card p-4 mb-4">
            <div class="help-icon mb-2">
              <img alt="" src="{{asset('imgs/icons/file.png')}}">
            </div>
            <span class="help-description mb-2">Получение выписок из ЕГРЮЛ,ЕГРИП</span>
            <span class="help-price">500 ₽</span>
          </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-6">
          <div class="help-card p-4 mb-4">
            <div class="help-icon mb-2">
              <img alt="" src="{{asset('imgs/icons/graph.png')}}">
            </div>
            <span class="help-description mb-2">Эксклюзивный поиск бизнеса под запрос покупателя</span>
            <span class="help-price">Бесплатно</span>
          </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-6">
          <div class="help-card p-4 mb-4">
            <div class="help-icon mb-2">
              <img alt="" src="{{asset('imgs/icons/secure-shield.png')}}">
            </div>
            <span class="help-description mb-2">Проверка рисков покупаемого бизнеса</span>
            <span class="help-price">от 25 000 ₽</span>
          </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-6">
          <div class="help-card p-4 mb-4">
            <div class="help-icon mb-2">
              <img alt="" src="{{asset('imgs/icons/briefcase.png')}}">
            </div>
            <span class="help-description mb-2">Сопровождение сделок купли продажи бизнеса</span>
            <span class="help-price">от 25 000 ₽</span>
          </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-6">
          <div class="help-card p-4 mb-4">
            <div class="help-icon mb-2">
              <img alt="" src="{{asset('imgs/icons/legal-document.png')}}">
            </div>
            <span class="help-description mb-2">Регистрация юридических лиц</span>
            <span class="help-price">от 20 000 ₽</span>
          </div>
        </div>
      </div>
    </div>
  </div> <!-- primary-sale-business -->

  <div class="service-request-form">
    <div class="container">
      <div class="row">
        <div class="col-md-8">
          <div class="service-request-form-block p-4">
            <form action="#" method="POST">
              <span class="title-service-request-form">
                Оставьте заявку на услугу
              </span>
              <div class="fields-service-request-form">
                <div class="line-fields-service-request-form mb-4">
                  <input placeholder="Ваше имя" type="text" class="form-control">
                  <input placeholder="Телефон" type="text" class="form-control">
                </div>
                <div class="line-fields-service-request-form mb-4">
                  <input placeholder="E-mail" type="text" class="form-control">
                  <button class="submit-request" type="submit">Отправить</button>
                </div>
              </div>
            </form>
            <span class="personal-data-processing">
                Нажимая на кнопку "Задать вопрос", вы даете согласие на
                <a href="#">обработку своих персональных данных</a>
              </span>
          </div>
        </div>
      </div>
    </div>
  </div><!-- service-request-form -->

@endsection