@extends('app')

@section('content')

  <div class="title-page">
    <div class="container title-block">
      <div class="row">
        <div class="col-md">
          <div class="mt-4 mb-2">
            <span class="name">Каталог проданных объектов</span>
          </div>
          <div class="mb-4">
            <ul class="nav">
              <li class="nav-item mr-2">
                <a class="nav-link-previous mr-1" href="#">Главная</a>
                <span class="point"></span>
              </li>
              <li class="nav-item mr-2">
                <a class="nav-link mr-1" href="#">Каталог проданных объектов</a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div><!-- title-page -->

  <div class="ready-business">
    <div class="container">
      <div class="row align-items-center mb-4">
        <div class="col-md d-flex justify-content-start">
          <span class="list-begin mr-3">Показать сначала</span>
          <span class="list-filter mr-2">дешевле</span>
          <span class="list-filter activated mr-2">прибыльнее</span>
          <span class="list-filter">по дате</span>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-4 col-md-6 col-sm-6 col-12 mb-4">
          <div class="card-ready-business border">
            <div class="preview-ready-business">
              <img alt="" class="pic-business" src="{{asset('examples/door-business.jpg')}}">
            </div>
            <div class="info-ready-business">
              <span class="title-ready-business">Производство мебели и дверей из массива</span>
              <span class="price-ready-business mb-2">4 950 000 ₽</span>
              <span class="profit-ready-business mb-2">Прибыль: 300 000 ₽/мес</span>
              <div>
                <span class="city-ready-business">Санкт-Петербург</span>
                <span class="date-ready-business">6.06.2019</span>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-6 col-12 mb-4">
          <div class="card-ready-business border">
            <div class="preview-ready-business">
              <div class="best-offer-business">
                Лучшее предложение
              </div>
              <img alt="" class="pic-business" src="{{asset('examples/nails-business.jpg')}}">
            </div>
            <div class="info-ready-business">
              <span class="title-ready-business">Студия ногтевого сервиса</span>
              <span class="price-ready-business mb-2">450 000 ₽</span>
              <span class="profit-ready-business mb-2">Прибыль: 300 000 ₽/мес</span>
              <div>
                <span class="city-ready-business">Санкт-Петербург</span>
                <span class="date-ready-business">6.06.2019</span>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-6 col-12 mb-4">
          <div class="card-ready-business border">
            <div class="preview-ready-business">
              <img alt="" class="pic-business" src="{{asset('examples/depilation-business.jpg')}}">
            </div>
            <div class="info-ready-business">
              <span class="title-ready-business">Салон депиляции</span>
              <span class="price-ready-business mb-2">1 950 000 ₽</span>
              <span class="profit-ready-business mb-2">Прибыль: 230 000 ₽/мес</span>
              <div>
                <span class="city-ready-business">Санкт-Петербург</span>
                <span class="date-ready-business">6.06.2019</span>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-6 col-12 mb-4">
          <div class="card-ready-business border">
            <div class="preview-ready-business">
              <img alt="" class="pic-business" src="{{asset('examples/mini-hotel-business.jpg')}}">
            </div>
            <div class="info-ready-business">
              <span class="title-ready-business">Миниотель в центре Санкт-петербурга</span>
              <span class="price-ready-business mb-2">2 700 000 ₽</span>
              <span class="profit-ready-business mb-2">Прибыль: 300 000 ₽/мес</span>
              <div>
                <span class="city-ready-business">Санкт-Петербург</span>
                <span class="date-ready-business">6.06.2019</span>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-6 col-12 mb-4">
          <div class="card-ready-business border">
            <div class="preview-ready-business">
              <img alt="" class="pic-business" src="{{asset('examples/produce-furniture-business.jpg')}}">
            </div>
            <div class="info-ready-business">
              <span class="title-ready-business">Производство мебели и дверей из массива</span>
              <span class="price-ready-business mb-2">4 950 000</span>
              <span class="profit-ready-business mb-2">Прибыль: 300 000 ₽/мес</span>
              <div>
                <span class="city-ready-business">Санкт-Петербург</span>
                <span class="date-ready-business">6.06.2019</span>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-6 col-12 mb-4">
          <div class="card-ready-business border">
            <div class="preview-ready-business">
              <img alt="" class="pic-business" src="{{asset('examples/coffee-house-business.jpg')}}">
            </div>
            <div class="info-ready-business">
              <span class="title-ready-business">Кофейня Take a Way</span>
              <span class="price-ready-business mb-2">800 000 ₽</span>
              <span class="profit-ready-business mb-2">Прибыль: 90 000 ₽/мес</span>
              <div>
                <span class="city-ready-business">Санкт-Петербург</span>
                <span class="date-ready-business">6.06.2019</span>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-6 col-12 mb-4">
          <div class="card-ready-business border">
            <div class="preview-ready-business">
              <img alt="" class="pic-business" src="{{asset('examples/produce-furniture-business.jpg')}}">
            </div>
            <div class="info-ready-business">
              <span class="title-ready-business">Производство мебели и дверей из массива</span>
              <span class="price-ready-business mb-2">4 950 000</span>
              <span class="profit-ready-business mb-2">Прибыль: 300 000 /мес</span>
              <div>
                <span class="city-ready-business">Санкт-Петербург</span>
                <span class="date-ready-business">6.06.2019</span>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-6 col-12 mb-4">
          <div class="card-ready-business border">
            <div class="preview-ready-business">
              <div class="best-offer-business">
                Лучшее предложение
              </div>
              <img alt="" class="pic-business" src="{{asset('examples/nails-business-2.jpg')}}">
            </div>
            <div class="info-ready-business">
              <span class="title-ready-business">Студия ногтевого сервиса</span>
              <span class="price-ready-business mb-2">450 000 ₽</span>
              <span class="profit-ready-business mb-2">Прибыль: 300 000 ₽/мес</span>
              <div>
                <span class="city-ready-business">Санкт-Петербург</span>
                <span class="date-ready-business">6.06.2019</span>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-6 col-12 mb-4">
          <div class="card-ready-business border">
            <div class="preview-ready-business">
              <img alt="" class="pic-business" src="{{asset('examples/depilation-business.jpg')}}">
            </div>
            <div class="info-ready-business">
              <span class="title-ready-business">Салон депиляции</span>
              <span class="price-ready-business mb-2">1 950 000 ₽</span>
              <span class="profit-ready-business mb-2">Прибыль: 230 000 ₽/мес</span>
              <div>
                <span class="city-ready-business">Санкт-Петербург</span>
                <span class="date-ready-business">6.06.2019</span>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-6 col-12 mb-4">
          <div class="card-ready-business border">
            <div class="preview-ready-business">
              <img alt="" class="pic-business" src="{{asset('examples/produce-furniture-business.jpg')}}">
            </div>
            <div class="info-ready-business">
              <span class="title-ready-business">Производство мебели и дверей из массива</span>
              <span class="price-ready-business mb-2">4 950 000</span>
              <span class="profit-ready-business mb-2">Прибыль: 300 000 /мес</span>
              <div>
                <span class="city-ready-business">Санкт-Петербург</span>
                <span class="date-ready-business">6.06.2019</span>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-6 col-12 mb-4">
          <div class="card-ready-business border">
            <div class="preview-ready-business">
              <div class="best-offer-business">
                Лучшее предложение
              </div>
              <img alt="" class="pic-business" src="{{asset('examples/nails-business-2.jpg')}}">
            </div>
            <div class="info-ready-business">
              <span class="title-ready-business">Студия ногтевого сервиса</span>
              <span class="price-ready-business mb-2">450 000 ₽</span>
              <span class="profit-ready-business mb-2">Прибыль: 300 000 ₽/мес</span>
              <div>
                <span class="city-ready-business">Санкт-Петербург</span>
                <span class="date-ready-business">6.06.2019</span>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-6 col-12 mb-4">
          <div class="card-ready-business border">
            <div class="preview-ready-business">
              <img alt="" class="pic-business" src="{{asset('examples/depilation-business.jpg')}}">
            </div>
            <div class="info-ready-business">
              <span class="title-ready-business">Салон депиляции</span>
              <span class="price-ready-business mb-2">1 950 000 ₽</span>
              <span class="profit-ready-business mb-2">Прибыль: 230 000 ₽/мес</span>
              <div>
                <span class="city-ready-business">Санкт-Петербург</span>
                <span class="date-ready-business">6.06.2019</span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div> <!-- ready-business -->

  <div class="pagination-list">
    <div class="container">
      <div class="row">
        <div class="col-md col-sm">
          <nav>
            <ul class="pagination">
              <li class="page-item">
                <a class="page-link active-item" href="#">1</a>
              </li>
              <li class="page-item">
                <a class="page-link" href="#">2</a>
              </li>
              <li class="page-item">
                <a class="page-link" href="#">3</a>
              </li>
              <li class="page-item">
                <a class="page-link" href="#">4</a>
              </li>
              <li class="page-item">
                <a class="page-link" href="#">5</a>
              </li>
              <li class="page-item dots-page-item">
                <span class="page-link dots-page-item">...</span>
              </li>
              <li class="page-item last-page-item">
                <a class="page-link" href="#">151</a>
              </li>
            </ul>
          </nav>
        </div>
        <div class="col-md col-sm-12 d-flex justify-content-sm-center justify-content-center justify-content-lg-end justify-content-md-end">
          <nav>
            <ul class="pagination">
              <li class="page-item">
                <a class="page-link" href="#"><</a>
              </li>
              <li class="page-item">
                <a class="page-link" href="#">Назад</a>
              </li>
              <li class="page-item">
                <span class="page-link">|</span>
              </li>
              <li class="page-item">
                <a class="page-link next" href="#">Далее</a>
              </li>
              <li class="page-item">
                <a class="page-link next" href="#">></a>
              </li>
            </ul>
          </nav>
        </div>
      </div>
    </div>
  </div><!-- pagination-list -->

@endsection