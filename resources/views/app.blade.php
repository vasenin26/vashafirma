<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="csrf-token" content="{{ csrf_token() }}"/>

  <title>{{$meta_title ?? 'ВашаФирма'}}</title>

  <!-- Styles -->
  <link href="{{env('APP_DEBUG') ? asset('css/app.bundle.css') : mix('css/app.bundle.css')}}" rel="stylesheet">
</head>
<body>

<div class="main-wrapper">

  <div class="content-b">

    <div class="container-fluid nav-header">
      <div class="row align-items-center">
        <div class="col-lg-auto col-print-6 col-md">
          <a class="your-firm-logo" href="/">
            <img alt="your-firm-pic" src="{{asset('imgs/logo.png')}}" class="your-firm-pic">
            <span class="your-firm-text">Ваша<span class="your-firm-text-second-part">Фирма</span></span>
          </a>
        </div>
        <div class="col-lg col-md-12 no-print">
          <div class="container">
            <div class="row">
              <div class="col">
                <ul class="nav ">
                  <li class="nav-item">
                    <a class="nav-link" href="{{route('ad.search')}}">Покупка</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="{{route('category.sale')}}">Продажа</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="{{route('special.other-service')}}">Услуги</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="{{route('category.sold')}}">Каталог проданных объектов</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="{{route('articles.index')}}">Статьи</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="{{route('special.about')}}">О компании</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="{{route('special.contacts')}}">Контакты</a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-auto d-flex align-items-center col-print-6">
          <a class="number-phone-support" href="tel:88129231377">+ 7 (812) 923-13-77</a>


          <form action="{{route('ad.search')}}"
                class="header-search no-print"
                data-module="InteractiveSearch"
          >
            <div class="header-search__input-box">
              <input name="s">
            </div>
            <div class="header-search__button" data-role="action-btn">
            </div>
          </form>

        </div>
      </div>
    </div><!-- Nav-header -->

    @yield('content')

  </div>

  <div class="footer-b">

    <div class="nav-footer no-print">
      <div class="container">
        <div class="row">
          <div class="col-md-4">
            <ul class="nav flex-column mb-4">
              <li class="nav-item">
                <a class="nav-link" href="{{route('ad.search')}}">Покупка бизнеса</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="{{route('category.sale')}}">Продажа бизнеса</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="{{route('category.sold')}}">Каталог проданных объектов</a>
              </li>
            </ul>
          </div>
          <div class="col-md ">
            <ul class="nav flex-column mb-4">
              <li class="nav-item">
                <a class="nav-link" href="{{route('special.other-service')}}">Услуги</a>
              </li>
              <li class="nav-item">
                <a class="nav-link nav-link-light" href="{{route('special.transaction-support')}}">Сопровождение сделок</a>
              </li>
              <li class="nav-item">
                <a class="nav-link nav-link-light" href="{{route('special.business-search')}}">Поиск бизнеса под запрос</a>
              </li>
              <li class="nav-item">
                <a class="nav-link nav-link-light" href="{{route('special.risk-check')}}">Проверка рисков</a>
              </li>
              <li class="nav-item">
                <a class="nav-link nav-link-light" href="{{route('special.registration-entities')}}">Регистрация юр.лиц</a>
              </li>
              <li class="nav-item">
                <a class="nav-link nav-link-light" href="{{route('special.extracts')}}">Получение выписок ЕГРЮЛ, ЕГРИП</a>
              </li>
            </ul>
          </div>
          <div class="col-md col-print">
            <ul class="nav flex-column mb-4">
              <li class="nav-item">
                <a class="nav-link" href="{{route('articles.index')}}">Статьи</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="{{route('special.about')}}">О компании</a>
              </li>
              <li class="nav-item">
                <a class="nav-link nav-link-light" href="{{route('reviews.index')}}">Отзывы</a>
              </li>
              <li class="nav-item">
                <a class="nav-link nav-link-light" href="{{route('jobs.index')}}">Вакансии</a>
              </li>
              <li class="nav-item">
                <a class="nav-link nav-link-light" href="{{route('special.contacts')}}">Контакты</a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div><!-- Nav Footer -->
    <div class="nav-gray-footer pt-3 pb-3">
      <div class="container-fluid">
        <div class="row">
          <div class="container">
            <div class="row align-items-center">
              <div class="col-md col-print-4 d-flex justify-content-start">
                <div class="firm-company mr-1"></div>
                <span class="your-firm">Ваша<span>Фирма</span></span>
              </div>
              <div class="col-md col-print-4 d-flex justify-content-start">
                <a class="number" href="tel:88129231377">+ 7 (812) 923-13-77</a>
                <span class="call-request  no-print" data-modal
                      data-href="{{route('special.modal', 'callback')}}">Заказать звонок</span>
              </div>
              <div class="col-md col-print-4">
                <a class="mail" href="mailto:info@vashafirma.ru">spb@vashafirma.ru</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div><!-- Nav gray Footer -->
    <div class="nav-footer pt-3 pb-3 print-text-center">
      <div class="container">
        <div class="row align-content-center">
          <div class="col-md">
            <span class="rights-reserved">@ Все права защищены, 2019</span>
          </div>
          <div class="col-md  no-print">
            <a class="policy" href="#">Политика конфиденциальности</a>
          </div>
          <div class="col-md  no-print">
            <a class="agreement" href="#">Пользовательское соглашение</a>
          </div>
        </div>
      </div>
    </div><!-- Nav Footer -->

  </div>
</div>

<script src="{{mix('js/app.bundle.js')}}"></script>

<!-- Fonts -->
<link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Roboto:300,500&display=swap" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700&display=swap" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,600,500,700&display=swap" rel="stylesheet">

</body>
</html>