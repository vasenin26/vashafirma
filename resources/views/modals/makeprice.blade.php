<form class="form v_form mix--callback" action="{{route('special.form.handler', 'makeprice')}}" data-module="Form">
  <input type="hidden" name="object_id" value="{{app('request')->input('object_id')}}">
  <div class="form-group">
    <label>Контактное лицо</label>
    <input name="name" class="form-control" placeholder="Контактное лицо" required>
  </div>
  <div class="form-group">
    <label>Телефон</label>
    <input name="phone" class="form-control" data-type="phone" required>
  </div>
  <div class="form-group">
    <label>Email</label>
    <input name="email" class="form-control" placeholder="Enter email">
  </div>
  <div class="form-group">
    <label>Ваше ценовое предложение</label>
    <input name="want_price" class="form-control" placeholder="укажите цену">
  </div>
  <button type="submit" class="btn btn-primary">Отправить</button>
</form>