@extends('admin/layout', [
'hideTitleTag' => 1
])

@section('content')

  <div class="row mb-4">
    <div class="col-4">
      <h1 class="text-nowrap text-truncate">{{$title}}</h1>
    </div>
    <div class="col-4">
      <form class="form-group row" action="{{route('admin.dashboard.search')}}">
        <div class="input-group">
          <input type="text" class="form-control" name="s" placeholder="Быстрый поиск">
          <div class="input-group-append">
            <button class="btn btn-primary btn-block">
              <i class="fab fa-searchengin"></i>
            </button>
          </div>
        </div>
      </form>
    </div>
    @if(Auth::user()->can('user_data_access'))
      <form class="col-4">
        <div class="input-group flex-nowrap">
          @include('/admin/parts/directory', [
            'placeholder' => 'Фильтр данных по брокер',
            'name' => 'broker_id',
            'directoryIndex' => 'brokers',
            'key' => 'name',
            'value' => request()->input('broker_id')
          ])
          <div class="input-group-append">
            <button class="btn btn-primary btn-block">
              <i class="fas fa-filter"></i>
            </button>
          </div>
        </div>
      </form>
    @endif
  </div>

  <div class="row">

    <div class="col-6">

      @if($unreadFormCounter)
        <div class="mb-4 alert alert-info">
          <i class="fab fa-wpforms"></i>
          Новые заявки: {{$unreadFormCounter}} <a
              href="{{ route('admin.form.index' , ['new' => 1]) }}">перейти к списку</a>
        </div>
      @endif

      @if($estateToday)
        <div class="mb-4 alert alert-info">
          <i class="fas fa-building"></i>
          Объекты на сегодня: {{$estateToday}} <a
              href="{{ route('admin.estate.index' , ['today' => 1]) }}">перейти к списку</a>
        </div>
      @endif

      @if($declarationsToday)
        <div class="mb-4 alert alert-info">
          <i class="far fa-address-card"></i>
          Деклараций на сегодня: {{$declarationsToday}} <a
              href="{{ route('admin.declaration.index', ['today' => 1]) }}">перейти к списку</a>
        </div>
      @endif

      @if($callsToday)
        <div class="mb-4 alert alert-info">
          <i class="fas fa-headset"></i>
          Обзвонов на сегодня: {{$callsToday}} <a href="{{ route('admin.customer.calling.index', ['today' => 1]) }}">перейти
            к списку</a>
        </div>
      @endif

      @if(Auth::user()->can('broker_planning'))
        @include('admin/dashboard/broker-planes/form')
      @endif

      @if(Auth::user()->can('personal_planning'))

        @if(session('new_plane_id'))
          <div class="alert alert-success">
            Новая запись добавлена
          </div>
        @endif

        <form class="mb-4 pb-4 border-bottom"
              action="{{route('admin.my-plane.add')}}"
              method="post"> @csrf

          <h4>
            Запланировать выезд
          </h4>

          @include('admin/plane/form')

          <button class="btn btn-outline-primary">Добавить</button>
        </form>

        @if($brokerPlane)
          <h4>
            Планы на сегодня
          </h4>

          @forelse($brokerPlane as $planeItem)
            @include('admin/plane/card', ['item' => $planeItem])
          @empty
            <div class="alert alert-secondary">На сегодня ничего не запланировано</div>
          @endforelse
        @endif

      @endif

    </div>

    <div class="col-3">

      <div class="mb-4">
        <h4>
          Статус публикации
        </h4>
        <div class="list-group">
          @foreach(DirectoryService::getSelectArray('publication_state') as $status)
            <a class="list-group-item list-group-item-action d-flex justify-content-between align-items-center"
               href="{{route('admin.estate.index', $addOwnerKey(['publication_state' => $status->id]) )}}"
               @if($status->color)
               style="background-color: {{$status->color}}"
                @endif
            >
              {{$status->value}}
              <span class="badge badge-primary badge-pill">
                {{$ownerFilter(App\Estate::whereHas('ad', function($ad) use ($status){
                  $ad->wherePublicationState($status->id);
                }), 'broker_id')->count()}}
              </span>
            </a>
          @endforeach
          <a class="list-group-item list-group-item-action d-flex justify-content-between align-items-center"
             href="{{route('admin.estate.index', $addOwnerKey([]))}}">
            Все
            <span
                class="badge badge-secondary badge-pill">{{$ownerFilter(App\Estate::orderBy('id'), 'broker_id')->count()}}</span>
          </a>
          <a class="list-group-item list-group-item-action d-flex justify-content-between align-items-center"
             href="{{route('admin.estate.index', $addOwnerKey(['is_archived' => 1]))}}">
            Архивные
            <span class="badge badge-secondary badge-pill">
              {{$ownerFilter(App\Estate::where('is_archived', 1), 'broker_id')->count()}}
            </span>
          </a>
        </div>
      </div>

      <div class="mb-4">
        <h4>
          Статус объявления
        </h4>
        <div class="list-group">
          @foreach(DirectoryService::getSelectArray('ad_state') as $status)
            <a class="list-group-item list-group-item-action d-flex justify-content-between align-items-center"
               href="{{route('admin.estate.index', $addOwnerKey(['ad_state' => $status->id]))}}"
               @if($status->color)
               style="background-color: {{$status->color}}"
                @endif
            >
              {{$status->value}}
              <span class="badge badge-primary badge-pill">
                {{$ownerFilter(App\Estate::whereHas('ad', function($ad) use ($status){
                  $ad->whereAdState($status->id);
                }), 'broker_id')->count()}}
              </span>
            </a>
          @endforeach
        </div>
      </div>
    </div>

    <div class="col-3">

      <div class="mb-4">
        <h4>
          Документы
        </h4>
        <div class="list-group">
          @foreach(Storage::disk()->files('documents') as $file)
            <a class="list-group-item list-group-item-action d-flex justify-content-between align-items-center"
               href="{{Storage::disk()->url($file)}}" target="_blank">
              <span class="d-block w-100  text-truncate">
              {{\File::basename($file)}}
              </span>
            </a>
          @endforeach
        </div>
      </div>

    </div>
  </div>

  @if(Auth::user()->can('user_data_access'))
    <div class="mb-4">
      <h4 class="mb-4">
        Сводка по брокерам
      </h4>
      <div class="row">
        @foreach($users as $user)
          <div class="col-3 mb-4">
            <h6 class="mb-1">{{$user->name}}</h6>
            <div class="list-group">
              @foreach(DirectoryService::getSelectArray('ad_state') as $status)
                <a class="list-group-item list-group-item-action d-flex justify-content-between align-items-center"
                   href="{{route('admin.estate.index', ['ad_state' => $status->id, 'broker_id' => $user->id])}}"
                   @if($status->color)
                   style="background-color: {{$status->color}}"
                    @endif
                >
                  {{$status->value}}
                  <span class="badge badge-primary badge-pill">
                {{$ownerFilter(App\Estate::whereHas('ad', function($ad) use ($status){
                  $ad->whereAdState($status->id);
                }), 'broker_id', $user->id)->count()}}
              </span>
                </a>
              @endforeach
            </div>
          </div>
        @endforeach
      </div>
    </div>
  @endif


@stop
