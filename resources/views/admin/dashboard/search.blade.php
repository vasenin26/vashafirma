@extends('admin/layout', [
'title' => 'Быстрый поиск'
])

@section('content')
  <div class="mb-4 border-bottom">
    <form class="form-group row" action="{{route('admin.dashboard.search')}}">
      <div class="col-sm-12 input-group flex-nowrap">
        <input type="text" class="form-control" name="s" value="{{request('s')}}">
        <div class="input-group-append">
          <button class="btn btn-primary btn-block">
            <i class="fab fa-searchengin"></i>
          </button>
        </div>
      </div>
    </form>
  </div>

  @if($estates)
    <div class="mb-4">
      <h4>Обекты</h4>

      @if($estates->count())
        @include('admin/estate/table', [
          'estates' => $estates
        ])
      @else
        <div class="alert alert-info">Объекты не найдены</div>
      @endif
    </div>
  @endif

  @if($declarations)
    <div class="mb-4">
      <h4>Декларации</h4>

      @if($declarations->count())
        @include('admin/declaration/table', [
        'declarations' => $declarations
        ])
      @else
        <div class="alert alert-info">Декларации не найдены</div>
      @endif
    </div>
  @endif

  @if($call_history)
    <div class="mb-4">
      <h4>История обзвона</h4>

      @if($call_history->count())
        @foreach($call_history as $call_history_item)
          @include('admin/customer_calling/card', ['call' => $call_history_item, 'actions' => 1])
        @endforeach
      @else
        <div class="alert alert-info">Обзвоны не найдены</div>
      @endif
    </div>
  @endif

@endsection