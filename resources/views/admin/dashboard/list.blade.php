@extends('admin/layout', [
'title' => 'Планирование брокеров'
])

@section('content')

  <div class="row">
    <div class="col-6">
      @include('admin/dashboard/broker-planes/form')
    </div>
  </div>

  @forelse($planes as $planeItem)
    @include('admin/plane/card', ['item' => $planeItem])
  @empty
    <div class="alert alert-info">
      Нет записей подходящих под фильтр
    </div>
  @endforelse

  {{$planes->links()}}

@endsection