<div class="mb-4 pb-4 border-bottom">
  <h4>
    Планирование броекров "на выезде"
  </h4>

  {{Form::model(request(), ['route' => 'admin.dashboard.broker-planes', 'class' => 'row', 'method' => 'GET'])}}
  <div class="col-6">
    <div class="form-group">
      <label>Брокер</label>
      @include('/admin/parts/directory',
      [
      'name' => 'broker_id',
      'directoryIndex' => 'brokers',
      'key' => 'name',
      'value' => request()->input('broker_id', null)
      ])
    </div>
  </div>
  <div class="col-4">
    <div class="form-group">
      <label>Прошедший период</label>
      {!! Form::select('period', [
      '' => ' ',
      'today' => 'Сегодня',
      'week' => 'Неделя',
      '2weeks' => '2 недели',
      'month' => 'Месяц',
      ], null, ['class' => 'form-control']) !!}
    </div>
  </div>
  <div class="col-2">
    <label>&nbsp;</label>
    <button class="btn btn-primary btn-block">
      Поиск
    </button>
  </div>
  {{Form::close()}}
</div>