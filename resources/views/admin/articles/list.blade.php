@extends('admin/layout')

@section('content')

  <nav class="navbar navbar-light bg-light mb-4">
    <a class="btn btn-outline-primary" href="{{ route('admin.articles.create') }}">
      <i class="fas fa-plus"></i> Добавить страницу
    </a>
    {!! Form::model(request(), ['class' => 'form-inline d-block', 'method' => 'GET']) !!}
    {!! Form::select('category_id',
['' => 'Все категории'] +
                    \App\ArticleCategory::all()->pluck('title', 'id')->toArray(),
                    null, [
                        'class' => 'form-control mr-2',
                        'style' => 'width: 320px'
                    ]) !!}
    {!! Form::text('s', null, ['class'=>'form-control mr-2', 'placeholder' => 'Поиск']) !!}
    <button class="btn btn-outline-success my-2 my-sm-0">Найти</button>
    {!! Form::close() !!}
  </nav>

  @if(session('status'))
    <div class="alert alert-success alert-dismissable">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
      {{session('status')}}
    </div>
  @endif

  @if($articles->count() == 0)
    <h5>Ничего не найдено</h5>
  @else
    <table class="table table-bordered table-hover">
      <thead>
      <tr>
        <th>№</th>
        <th>Название</th>
        <th>Автор</th>
        <th>Публикация</th>
        <th>Действия</th>
      </tr>
      </thead>
      <tbody>
      @foreach($articles as $article)
        <tr>
          <td>{{ $counter++ }}</td>
          <td class="article_title">

            <a href="{{ route('admin.articles.edit', $article->id) }}">
              {{ $article->title }}
            </a>

            <a href="{{ url($article->url) }}" target="_blank" class="btn btn-light btn-sm"
               title="Подробнее">
              <i class="fas fa-eye"></i>
            </a>

          </td>
          <td>
            @if($article->owner->name ?? null)
              <a href="{{ route('admin.articles.index', ['author' => $article->created_by]) }}">
                {{ $article->owner->name}}
              </a>
            @endif
          </td>
          <td class="published">
            @if($article->published_at)
              {{ $article->published_at }}
            @else
              <div class="text-muted">Не опубликована</div>
            @endif
          </td>
          <td class="actions">
            <form action="{{ route('admin.articles.update', $article->id) }}" method="post">
              @csrf
              @method('put')
              <a href="{{ route('admin.articles.edit', $article->id) }}" class="btn btn-info btn-sm"
                 title="Редактировать">
                <i class="fas fa-edit"></i>
              </a>
              <a href="" class="action_delete" title="Переместить в корзину">
                <button type="submit" class="btn btn-danger btn-sm" name="delete_article"
                        value="delete">
                  <i class="fas fa-trash-alt"></i>
                </button>
              </a>
            </form>
          </td>

        </tr>
      @endforeach
      @endif
      </tbody>
    </table>
    {{  $articles->links() }}
@stop
