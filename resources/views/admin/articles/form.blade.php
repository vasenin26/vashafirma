<div class="row">
  <div class="col-9">

    <div class="form-group">
      <label>Название *</label>
      <input type="text" class="form-control" name="title" value="{{old('title', $article->title)}}" required>
      @if ($errors->has('title'))
        <div class="alert alert-danger">
          {{ $errors->first('title') }}
        </div>
      @endif
    </div>

    <div class="form-group">
      <label>Группа статей</label>
      @include('/admin/parts/directory', [
        'name' => 'category_id',
         'directoryIndex' => 'article_category',
         'value' => $article->category_id ?? 'null'
      ])
    </div>

    <div class="form-group">
      <label>Ссылка</label>
      <input type="text" class="form-control" name="url" value="{{old('url', $article->url)}}">
      @if ($errors->has('url'))
        <div class="alert alert-danger">
          {{ $errors->first('url') }}
        </div>
      @endif
    </div>

    <div class="form-group">
      <label>Мета заголовок (тег title)</label>
      <input type="text" class="form-control" name="meta_title" value="{{old('meta_title', $article->meta_title)}}">
      @if ($errors->has('meta_title'))
        <div class="alert alert-danger">
          {{$errors->first('meta_title')}}
        </div>
      @endif
    </div>

    <div class="form-group">
      <label>Описание (meta_description)</label>
      <input type="text" class="form-control" name="description"
             value="{{ $article->description ?? old('description') }}">
      @if ($errors->has('description'))
        <div class="alert alert-danger">
          {{ $errors->first('description') }}
        </div>
      @endif
    </div>

    <div class="form-group">
      <label>Ключевые слова (meta_keywords)</label>
      <input type="text" class="form-control" name="keywords" value="{{ $article->keywords ?? old('keywords') }}">
      @if ($errors->has('keywords'))
        <div class="alert alert-danger">
          {{ $errors->first('keywords') }}
        </div>
      @endif
    </div>

  </div>
  <div class="col-3">

    <div class="form-group">
      <label>Заголовочное изображение</label>
      <br>
      @if($article instanceof App\Article)
        <img src="{{$article->preview->getWidth(400)}}" alt="" class="img-thumbnail mb-4">
      @endif
      <input type="file" class="form-control" name="preview">

      Удалить {{Form::checkbox('remove_preview')}}
    </div>

  </div>
</div>

<div class="form-group">
  <label>Публикация страницы</label>
  <div class="form-check">
    <input type="hidden" name="is_published" value="0">
    <input type="checkbox"
           class="form-check-input"
           id="is_published"
           name="is_published" {{checked_is($article->is_published, 'is_published', 1)}}
           value="1">
    <label class="form-check-label" for="is_published">Опубликовано</label>
  </div>
</div>

<div class="form-group">
  <reachtext
      name="text"
      value="{{  old('text', $article->text ?? '') }}"
  ></reachtext>
  @if ($errors->has('text'))
    <span class="help-block">
                                    <strong>{{ $errors->first('text') }}</strong>
                                </span>
  @endif
</div>