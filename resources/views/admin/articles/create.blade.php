@extends('admin/layout', [
'title' => 'Добавить страницу'
])

@section('content')
  @if(session('status'))
    <div class="alert alert-success alert-dismissable">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
      {{session('status')}}
    </div>
  @endif

  <form action="{{ route('admin.articles.store') }}" method="post">
    @csrf
    @method('post')

    @include('admin/articles/form', ['article' => $article])

    <button type="submit" class="btn btn-success">Сохранить</button>
  </form>
@stop
