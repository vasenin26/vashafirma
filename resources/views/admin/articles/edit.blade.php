@extends('admin/layout')

@section('content')

  @if(session('status'))
    <div class="alert alert-success alert-dismissable">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
      {{session('status')}}
    </div>
  @endif

  {!! Form::model($article, ['files' => true, 'route' => ['admin.articles.update', $article->id], 'method'=>'PUT']) !!}

  @include('admin/articles/form', ['article' => $article])

  <button type="submit" class="btn btn-success">Сохранить</button>

  {!! Form::close() !!}

@stop
