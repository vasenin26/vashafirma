@extends('admin/layout', $library)

@section('content')

  <nav aria-label="breadcrumb">
    <ol class="breadcrumb">
      <li class="breadcrumb-item">
        <a href="{{route('admin.directory.index')}}">Справочники</a>
      </li>
      <li class="breadcrumb-item">
        <a href="{{route('admin.directory.show', $directoryIndex)}}">{{$library['title']}}</a>
      </li>
      <li class="breadcrumb-item active">Редактирование записи</li>
    </ol>
  </nav>

  {!! Form::model($item, ['route' => ['admin.directory.item.update', $directoryIndex, $item->id], 'method'=>'PUT']) !!}

  {!! Form::hidden('back', $request->input('back')) !!}

  @include('admin/directories/item/form', ['item' => $item])

  {{ Form::submit('Сохранить', ['class' => 'btn btn-success'])}}

  {!! Form::close() !!}

@endsection
