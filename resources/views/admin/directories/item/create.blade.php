@extends('admin/layout', [
'title' => 'Новый эелемент - ' . $library['title']
])

@section('content')

  <nav aria-label="breadcrumb">
    <ol class="breadcrumb">
      <li class="breadcrumb-item">
        <a href="{{route('admin.directory.index')}}">Справочники</a>
      </li>
      <li class="breadcrumb-item">
        <a href="{{route('admin.directory.show', $directoryIndex)}}">{{$library['title']}}</a>
      </li>
      <li class="breadcrumb-item active">Создание записи</li>
    </ol>
  </nav>

  {!! Form::open(['route' => ['admin.directory.item.store', $directoryIndex], 'method' => 'POST']) !!}

  @include('admin/directories/item/form', ['item' => $item])

  {{ Form::submit('Добавить', ['class' => 'btn btn-success']) }}

  {!! Form::close() !!}

@endsection
