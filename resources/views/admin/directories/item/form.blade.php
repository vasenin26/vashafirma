@foreach($item->getFields() as $key => $declaration)

  <div class="form-group row">
    <label class="col-form-label col-md-4">{{is_array($declaration) ? $declaration['title'] : $declaration}}</label>
    <div class="col-md-8">
      @if($item->isFillable($key))

        @if(is_array($declaration))
          {!! $declaration['field']($item->$key) !!}
        @else
          {!! Form::input('text', $key, null, ['class' => 'form-control']) !!}
        @endif

      @else
        <label class="col-form-label col-md-4">
          {{$item->getValuePreview($key) ?: 'N/A'}}
        </label>
      @endif
    </div>
  </div>

@endforeach
