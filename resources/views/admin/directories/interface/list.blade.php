<table class="table table-bordered table-hover">
  <thead>
  <tr>
    <th style="width: 40px;">&nbsp;</th>
    @foreach($cols as $col)
      <th>{{$col}}</th>
    @endforeach
    <th>
      Действия
    </th>
  </tr>
  </thead>
  <tbody>
  @foreach($items as $item)
    <tr class="@if(session('newItemId') == $item->id) table-success @endif">
      <td>
        @if($item->special_key)
          <span class="with-tooltip">
            <i class="fas fa-exclamation-triangle text-info"></i>
            <span class="tooltip bs-tooltip-right">
              <b class="arrow"></b>
              <span class="tooltip-inner"> {{$item->description}}
              </span>
            </span>
          </span>
        @endif
      </td>
      @foreach($cols as $key => $title)
        <td>
          {{$item->getValuePreview($key)}}
        </td>
      @endforeach
      <td>
        <a href="{{route('admin.directory.item', [$directoryIndex, $item->getKey() ])}}"
           class="btn btn-primary btn-sm"><i class="fas fa-edit"></i></a>
        @if(!$item->special_key)
          <a href="{{route('admin.directory.item.delete', [$directoryIndex, $item->getKey() ])}}"
             class="btn btn-danger btn-sm"><i class="fas fa-trash-alt"></i></a>
        @endif
      </td>
    </tr>
  @endforeach
  </tbody>
</table>

@if($items instanceof Illuminate\Contracts\Pagination\LengthAwarePaginator)
  {{$items->links()}}
@endif