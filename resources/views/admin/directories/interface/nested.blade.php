@if(session('success') === 1)
  <div class="alert alert-success">
    Структура обновлена
  </div>
@endif

{!! Form::open(['route' => ['admin.directory.nested.update', $directoryIndex], 'method' => 'PUT']) !!}

<div class="form-group">
  <nested
      name="nested_structure"
      v-bind:items='{!! $items !!}'
      link="{!! route('admin.directory.item', [$directoryIndex, '_id_' ]) !!}"
  >
  </nested>
</div>

{{ Form::submit('Сохранить', ['class' => 'btn btn-success']) }}

{!! Form::close() !!}