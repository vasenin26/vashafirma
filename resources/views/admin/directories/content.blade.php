@extends('admin/layout', $library)

@section('content')

  <nav aria-label="breadcrumb">
    <ol class="breadcrumb">
      <li class="breadcrumb-item">
        <a href="{{route('admin.directory.index')}}">Справочники</a>
      </li>
      <li class="breadcrumb-item active">{{$library['title']}}</li>
    </ol>
  </nav>

  <nav class="navbar navbar-light bg-light mb-4">
    <a class="btn btn-outline-primary" href="{{route('admin.directory.item.create', $directoryIndex) }}">
      <i class="fas fa-plus"></i> Добавить элемент
    </a>
  </nav>

    @include('admin/directories/interface/' . $type, ['items' => $items])

@endsection
