@extends('admin/layout', [
'title' => 'Управление справочниками'
])

@section('content')

  <div class="row">
    <div class="col-md-6">
      <div class="list-group">
        @foreach($libraries as $index => $library)
          <a class="list-group-item list-group-item-action"
             href="{{ route('admin.directory.show', $index) }}">
            {{$library['title']}}
          </a>
        @endforeach
      </div>
    </div>
  </div>

@stop
