<div class="card mb-4">
  <div class="card-header {{classIsSession($call, 'call_id', 'bg-success')}}">
    <div class="row">
      <div class="col-sm-6">
        #{{$call->id}}
      </div>
      <div class="col-sm-6 text-right">
        {{$call->created_at}}
      </div>
    </div>
  </div>
  <div class="card-body">
    <div class="row border-bottom pb-4 mb-4">
      <div class="col-sm-4">
        Брокер: {{$call->broker ? $call->broker->name : ''}}
      </div>
      <div class="col-sm-4">
        Источник: {{$call->source->value}}
      </div>
      <div class="col-sm-4">
        Телефон: {{$call->phone}}
      </div>
    </div>
    <div class="row">
      <div class="col-sm-6">
        <label class="d-block">Результат звонка:</label>
        @if($call->lastCallEvent)
          {{$call->lastCallEvent->description ?: 'N\A'}}
          <hr>
          Дата созвона: {{$call->lastCallEvent->next_call_date ?
              $call->lastCallEvent->next_call_date->format('Y-m-d')
              : 'N\A'}}
        @else
          <div class="alert alert-warning">
            результать звонка не указан
          </div>
        @endif
      </div>
      <div class="col-sm-6 border-left">
        <label class="d-block">Объявление:</label>
        {{$call->ad_text}}
        @if($call->link)
          <hr>
          @if(URL::isValidUrl($call->link))
            Источник: <a href="{{$call->link}}" target="_blank">{{$call->link}}</a>
          @else
            Источник: {{$call->link}}
          @endif
        @endif
      </div>
    </div>
  </div>
  @if($actions ?? null)
    <div class="card-footer text-right">
      <a class="btn btn-outline-primary" href="{{route('admin.customer.calling.edit', $call->id)}}">открыть</a>
    </div>
  @endif
</div>