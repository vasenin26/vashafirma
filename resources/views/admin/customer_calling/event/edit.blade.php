@extends('admin/layout', [
  'title' => 'Редактировнаие события'
])

@section('content')

  <nav aria-label="breadcrumb">
    <ol class="breadcrumb">
      <li class="breadcrumb-item">
        <a href="{{route('admin.customer.calling.index')}}">Обзвон</a>
      </li>
      <li class="breadcrumb-item">
        <a href="{{route('admin.customer.calling.edit', $event->thread_id)}}">Карточка обзвона</a>
      </li>
      <li class="breadcrumb-item active">Редактировнаие события</li>
    </ol>
  </nav>

  @if($accessDelay = $event->accessDelay())
    <div class="alert alert-info">
      Доступ будет ограничен через: {{$accessDelay}}
    </div>
  @endif


  {!! Form::model($event, ['route' => ['admin.customer.calling.event.update', $event->thread_id, $event->id], 'method'=>'PUT']) !!}

  <div class="row">
    <div class="col-sm-6">
      <div class="form-group">
        <label>Результат обзвона</label>
        {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
      </div>
      @if ($errors->has('description'))
        <div class="alert alert-danger">
          {{ $errors->first('description') }}
        </div>
      @endif
    </div>
    <div class="col-sm-6">
      <div class="form-group">
        <label>Дата созвона</label>
        <datepicker value="{{old('next_call_date', $event->next_call_date)}}"
                    name="next_call_date"
        ></datepicker>
      </div>
    </div>
  </div>

  {{ Form::submit('сохранить', ['class'=>'btn btn-success'])}}

  {!! Form::close() !!}

@endsection