@extends('admin/layout', [
'title' => 'Комментарии. Обзвон номер ' . $call->id
])

@section('content')

  @include('admin/customer_calling/menu', ['call' => $call])

  {!! $comments !!}

@endsection