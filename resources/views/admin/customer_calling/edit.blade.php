@extends('admin/layout', [
'title' => 'Карточка обзвона номер: ' . $call->id
])

@section('content')

  {!! Form::model($call, ['route' => ['admin.customer.calling.update', $call->id], 'method'=>'PUT']) !!}

  @if($call->isUserAccess())
    @include('admin/customer_calling/form', ['call' => $call])
  @else
    @include('admin/customer_calling/card', ['call' => $call])
  @endif

  @include('admin/customer_calling/event')

  {{ Form::submit('сохранить', ['class'=>'btn btn-success'])}}

  {!! Form::close() !!}

  <div class="mt-4">

    <h2>История изменений</h2>

    @forelse($call->callEvents as $event)
      <div class="card mb-4 {{classIsSession($call, 'call_id', 'bg-success')}}">
        <div class="card-header">
          <div class="row">
            <div class="col-sm-6">
              Автор: {{$event->owner->name ?? ''}}
            </div>
            <div class="col-sm-6 text-right">
              Создана: {{$event->created_at}}
            </div>
          </div>
        </div>
        <div class="card-body">
          {!! $event->description ?: '<span class="text-muted font-italic">(Описание отсутствует)</span>' !!}
        </div>
        <div class="card-footer">
          <div class="row">
            <div class="col-6">
              Назначенная дата созвона: {{$event->next_call_date ? $event->next_call_date->format('Y-m-d') : 'N\A'}}
            </div>
            <div class="col-6 text-right">
              @if($event->isUserAccess())
                @if($accessDelay = $event->accessDelay())
                  Доступ будет ограничен через: {{$accessDelay}}
                @endif
                <a href="{{route('admin.customer.calling.event', [$call->id, $event->id])}}"
                   class="btn btn-outline-primary btn-sm">редактировать</a>
              @endif
            </div>
          </div>
        </div>
      </div>
      @empty
        <div class="alert alert-warning">
         Результать звонка не указан
        </div>
    @endforelse
  </div>

@stop
