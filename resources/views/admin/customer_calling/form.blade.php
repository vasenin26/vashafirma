<div class="row">
  <div class="col-sm-6">

    @if(Auth::user()->can('edit_customer_call_history'))
      <div class="form-group">
        <label>Брокер</label>
        @include('/admin/parts/directory', [
            'name' => 'broker_id',
            'directoryIndex' => 'brokers',
            'key' => 'name',
            'value' => $call->broker_id ?? null
          ])
        @if ($errors->has('broker_id'))
          <div class="form-alert alert alert-danger">
            {{ $errors->first('broker_id') }}
          </div>
        @endif
      </div>
    @endif

    <div class="form-group">
      <label>Источник</label>
      @include('/admin/parts/directory', [
          'name' => 'customer_source_id',
          'directoryIndex' => 'customer_source',
          'key' => 'value',
          'value' => $call->customer_source_id ?? null
        ])
      @if ($errors->has('customer_source_id'))
        <div class="form-alert alert alert-danger">
          {{ $errors->first('customer_source_id') }}
        </div>
      @endif
    </div>


    <div class="form-group">
      <label>Ссылка</label>
      {!! Form::text('link', null, ['class' => 'form-control']) !!}
      @if ($errors->has('link'))
        <div class="form-alert alert alert-danger">
          {{ $errors->first('link') }}
        </div>
      @endif
    </div>

    <div class="form-group">
      <label>Контактный телефон *</label>
      <maskedinput
          class="form-control"
          mask="\+\7 (111) 111-11-11"
          name="phone"
          value="{{old('phone', $call->phone ?? null)}}"></maskedinput>
      @if ($errors->has('phone'))
        <div class="form-alert alert alert-danger">
          {{ $errors->first('phone') }}
        </div>
      @endif
    </div>
  </div>
  <div class="col-sm-6">

    <div class="form-group">
      <label>Текст объявления</label>
      {!! Form::textarea('ad_text', null, ['class' => 'form-control']) !!}
    </div>
    @if ($errors->has('ad_text'))
      <div class="alert alert-danger">
        {{ $errors->first('ad_text') }}
      </div>
    @endif

  </div>
</div>