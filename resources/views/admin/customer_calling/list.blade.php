@extends('admin/layout', [
'title' => request()->filled('today') ? 'Обзвоны на сегодя' : 'История обзвона'
])

@section('content')

  <nav class="navbar navbar-light bg-light mb-4">
    <a class="btn btn-outline-primary" href="{{ route('admin.customer.calling.create') }}">
      <i class="fas fa-plus"></i> Добавить запись
    </a>
    {!! Form::model(request(), ['class' => 'form-inline d-flex flex-nowrap', 'method' => 'GET']) !!}

    {{--
    <div class="form-group mr-2">
      <label class="mr-1">Показывать все</label>
      {!! Form::checkbox('all', '1', null, ['class' => 'd-inline']) !!}
    </div>
    --}}

    {!! Form::hidden('today') !!}

    @if(Auth::user()->can('user_data_access'))
      @include('/admin/parts/directory', [
        'class' => 'mr-2',
        'style' => 'width: 420px',
        'placeholder' => 'Брокер',
        'name' => 'broker_id',
        'key' => 'name',
        'directoryIndex' => 'brokers',
        'value' => request()->input('broker_id') ?: 'null'
      ])
    @endif

    {!! Form::text('s', null, ['class'=>'form-control mr-2', 'placeholder' => 'Поиск']) !!}
    <button class="btn btn-outline-success my-2 my-sm-0">Найти</button>
    {!! Form::close() !!}
  </nav>

  @if(session('status'))
    <div class="alert alert-success">
      {{session('status')}}
    </div>
  @endif

  @foreach($calls as $call)

    @include('admin/customer_calling/card', ['call' => $call, 'actions' => 1])

  @endforeach

  {{$calls->links()}}

  {{--
  {!! Form::open(['route' => ['admin.customer.calling.store'], 'method'=>'POST']) !!}

  @include('admin/customer_calling/form')

  {{ Form::submit('Добавить',['class'=>'btn btn-success'])}}

  {!! Form::close() !!}
  --}}

  <div class="mt-4 alert alert-secondary">
    @if(!request()->filled('today'))
      <a href="{{route('admin.customer.calling.index', ['today' => 1])}}">Обзвоны на сегодня</a>
    @else
      Показаны обзвоны на сегодняшную дату. <a href="{{route('admin.customer.calling.index', [])}}">Весь список</a>
    @endif
  </div>

@endsection
