@extends('admin/layout', [
'title' => 'Фиксация обзвона'
])

@section('content')
  {!! Form::model($call, ['route' => ['admin.customer.calling.store'], 'method'=>'POST']) !!}

  @include('admin/customer_calling/form', ['call' => $call])
  @include('admin/customer_calling/event', ['call' => $call])

  {{ Form::submit('Добавить',['class'=>'btn btn-success'])}}

  {!! Form::close() !!}
@stop
