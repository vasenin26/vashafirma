<div class="row">
  <div class="col-sm-6">
    <div class="form-group">
      <label>Результат обзвона</label>
      {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
    </div>
    @if ($errors->has('description'))
      <div class="alert alert-danger">
        {{ $errors->first('description') }}
      </div>
    @endif
  </div>
  <div class="col-sm-6">
    <div class="form-group">
      <label>Дата созвона</label>
      <datepicker value="{{old('next_call_date')}}"
                  name="next_call_date"
      ></datepicker>
    </div>
  </div>
</div>