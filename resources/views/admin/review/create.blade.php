@extends('admin/layout', [
'title' => 'Новый отзыв'
])

@section('content')
  {!! Form::open(['action' => ['Admin\ReviewController@store'], 'method'=>'POST']) !!}

  @include('admin/review/form', ['review' => $review])

  {{ Form::submit('Добавить',['class'=>'btn btn-success'])}}

  {!! Form::close() !!}
@stop

