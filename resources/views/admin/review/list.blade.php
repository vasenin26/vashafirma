@extends('admin/layout', [
'title' => 'Список отзывов'
])

@section('content')

  <nav class="navbar navbar-light bg-light mb-4">

    <a class="btn btn-outline-primary" href="{{route('admin.review.create')}}">
      <i class="fas fa-plus"></i>
      Добавить отзыв
    </a>
  </nav>

  @if(session('status'))
    <div class="alert alert-success alert-dismissable">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
      {{session('status')}}
    </div>
  @endif

  @if($reviews->count() == 0)
    <h5>Пока нет ни одной страницы</h5>
  @else
    <table class="table table-bordered table-hover">
      <thead>
      <tr>
        <th>Автор</th>
        <th>Текст</th>
        <th>Действия</th>
      </tr>
      </thead>
      <tbody>
      @foreach($reviews as $review)
        <tr>
          <td class="article_title">
              {{ $review->author }}
          </td>
          <td class="published w-75">
           <span>
             <a href="{{ route('admin.review.edit', $review->id) }}">
               {{$review->content}}
             </a>
           </span>
          </td>
          <td class="actions">
            <form class="d-inline" action="{{ route('admin.review.update', $review->id) }}" method="post">
              @csrf
              @method('put')
              <a href="{{ route('admin.review.edit', $review->id) }}" class="btn btn-info btn-sm"
                 title="Редактировать">
                <i class="fas fa-edit"></i>
              </a>
            </form>
            <form class="d-inline" method="POST" action="{{route('admin.review.destroy', $review->id)}}">
              @csrf
              @method('DELETE')
              <a href="" class="action_delete" title="Переместить в корзину">
                <button type="submit" class="btn btn-danger btn-sm" name="delete_review"
                        value="delete">
                  <i class="fas fa-trash-alt"></i>
                </button>
              </a>
            </form>

          </td>

        </tr>
      @endforeach
      @endif
      </tbody>
    </table>


@endsection