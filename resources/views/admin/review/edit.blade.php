@extends('admin/layout', [
'title' => 'Отзыв номер: ' . $review->id
])

@section('content')
  <div id="pages_content">
    <div class="add_article">
      {!! Form::model($review, ['action' => ['Admin\ReviewController@update', $review->id], 'method'=>'PUT']) !!}

      @include('admin/review/form', ['review' => $review])

      {{ Form::submit('сохранить',['class'=>'btn btn-success'])}}

      {!! Form::close() !!}
    </div>
  </div>
@stop
