<div class="row">
  <div class="col-md-6">
    <div class="form-group">
      <label>Автор</label>
      {!! Form::input('text', 'author', null, ['class' => 'form-control', 'required' => 'required']) !!}
      @if ($errors->has('title'))
        <div class="alert alert-danger">
          {{ $errors->first('title') }}
        </div>
      @endif
    </div>
    <div class="form-group">
      <label>Текст</label>
      {!! Form::textarea('content', null, ['class' => 'form-control']) !!}
      @if ($errors->has('content'))
        <div class="alert alert-danger">
          {{ $errors->first('content') }}
        </div>
      @endif
    </div>
  </div>
</div>