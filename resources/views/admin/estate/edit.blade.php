@extends('admin/layout', [
'title' => 'Объект: ' . ($estate->title ?: $estate->object_title) . ' #' . $estate->id
])

@section('content')

  @include('admin/estate/menu', ['estate' => $estate])

  @if(session('success'))
    <div class="alert alert-success">
      Сохранено
    </div>
  @endif

  {!! Form::model($estate, ['route' => ['admin.estate.update', $estate->id], 'method' => 'PUT']) !!}

  @include('admin/estate/form', ['estate' => $estate])

  <div class="fixed-bottom admin-panel-bottom">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-10 offset-2">
          {{ Form::submit('Сохранить',['class'=>'btn btn-success']) }}
        </div>
      </div>
    </div>
  </div>

  <div class="panel-spacer"></div>

  {!! Form::close() !!}

@endsection