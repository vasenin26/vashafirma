@extends('admin/layout', [
'title' => 'История работы с объектом ' . $estate->object_title . ' #' . $estate->id
])

@section('content')

  @include('admin/estate/menu', ['estate' => $estate])

  @foreach($events as $event)

    <div class="border-bottom mb-4">
      <div class="row">
        <div class="col-sm-6 pb-4">
          @if(session('eventId') === $event->id)
            <div class="alert alert-success">
              Запись обновлена
            </div>
          @endif
          {{$event->description}}
        </div>
        <div class="col-sm-4">
          <ul>
            @if($event->next_call_date)
              <li>
                дата созвона
                <span class="badge badge-info">
                {{$event->next_call_date->format('Y-m-d')}}
              </span>
              </li>
            @endif
            <li>
              добавлено: {{$event->created_at}}
            </li>
            <li>
              автор: {{$event->owner->name}}
            </li>
          </ul>
        </div>
        <div class="col-sm-2">
          @if($event->isUserAccess() || Auth::user()->can('manage_estate_comments'))
            <div class="mt-2 mb-4 text-right">
              <a class="btn btn-outline-primary btn-sm"
                 href="{{route('admin.estate.history.edit', ['estateId' => $estate->id, 'commentId' => $event->id])}}">редактровать</a>
              <a class="btn btn-outline-danger btn-sm"
                 href="{{route('admin.estate.history.delete', ['estateId' => $estate->id, 'commentId' => $event->id])}}">
                <i class="fas fa-trash-alt"></i>
              </a>
            </div>
          @endif
        </div>
      </div>
    </div>

  @endforeach

  {!! Form::open(['route' => ['admin.estate.history.add', $estate->id], 'method'=>'POST']) !!}
  <div class="row">
    <div class="col-sm-6">
      <div class="form-group">
        {!! Form::textarea('description', null, ['class' => 'form-control', 'placeholder' => 'Описание события']) !!}
        @if ($errors->has('description'))
          <div class="form-alert alert alert-danger">
            {{ $errors->first('description') }}
          </div>
        @endif
      </div>
      {{ Form::submit('Добавить',['class'=>'btn btn-success'])}}
    </div>

    <div class="col-sm-6">
      <div class="form-group">
        <label>Дата созвона</label>
        <datepicker name="next_call_date"
                    value="{{old('next_call_date', $estate->next_call_date ?? null)}}"></datepicker>
      </div>
    </div>
  </div>

  {!! Form::close() !!}

@endsection