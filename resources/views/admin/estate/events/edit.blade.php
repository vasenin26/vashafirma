@extends('admin/layout', [
'title' => 'История работы с объектом ' . $estate->object_title . ' #' . $estate->id
])

@section('content')

  @include('admin/estate/menu', ['estate' => $estate])

  {!! Form::model($event, ['route' => ['admin.estate.history.update', $event->estate_id, $event->id], 'method'=>'PUT']) !!}
  <div class="row">
    <div class="col-sm-6">
      <div class="form-group">
        {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
      </div>
      {{ Form::submit('Сохранить',['class'=>'btn btn-success'])}}
    </div>

    <div class="col-sm-6">
      <div class="form-group">
        <label>Дата созвона</label>
        <datepicker value="{{old('next_call_date', $event->next_call_date ?? null)}}"
                    name="next_call_date"
        ></datepicker>
      </div>
    </div>
  </div>
  {!! Form::close() !!}

@endsection