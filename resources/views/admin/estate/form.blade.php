<div class="form-group">
  <label>Название объекта</label>
  {!! Form::input('text', 'title', null, ['class' => 'form-control']) !!}
</div>
@error('title')
<div class="alert alert-danger">
  Название обязательно
</div>
@enderror

<div class="row">
  <div class="col-md-6">
    <div class="form-group">
      <label>Бизнес брокер</label>
      @include('/admin/parts/directory', ['name' => 'broker_id', 'directoryIndex' => 'brokers', 'key' => 'name', 'value' => old('broker_id', $estate->broker_id)])
    </div>

    <div class="form-group">
      <label>Маркетинг</label>
      @include('/admin/parts/directory', ['name' => 'marketing_type', 'directoryIndex' => 'marketing_type', 'key' => 'value', 'value' => old('marketing_type', $estate->marketing_type)])
    </div>

  </div>
  <div class="col-md-6">
    <div class="form-group">
      <label>Дата создания</label>
      {!! Form::input('text', 'created_at', null, ['class' => 'form-control', 'disabled' => 'disabled']) !!}
    </div>

    <div class="form-group">
      <label>Дата обновления</label>
      {!! Form::input('text', 'updated_at', null, ['class' => 'form-control', 'disabled' => 'disabled']) !!}
    </div>

    <div class="form-group">
      <label>Первичная публикация</label>
      {!! Form::input('text', 'first_published_at', null, ['class' => 'form-control', 'disabled' => 'disabled']) !!}
    </div>

    <div class="form-group form-check">
      {!! Form::hidden('is_archived', 0) !!}
      {!! Form::checkbox('is_archived', 1, null, ['class' => 'form-check-input', 'id' => 'field_is_subscription']) !!}
      <label class="form-check-label" for="field_is_subscription">Архивный</label>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-6">
    <h3>
      Информация по договору
    </h3>

    <div class="form-group">
      <label>Вид договора</label>
      @include('/admin/parts/directory', ['name' => 'contract_type', 'directoryIndex' => 'contract_type', 'key' => 'value', 'value' => $estate->contract_type])
    </div>

    <div class="form-group">
      <label>Дата подписания договора</label>
      <datepicker value="{{old('contract_signing_at', $estate->contract_signing_at ?? null)}}"
                  name="contract_signing_at"
      ></datepicker>
    </div>

    <div class="form-group">
      <label>Дата окончания договора</label>
      <datepicker value="{{old('contract_expiring_at', $estate->contract_expiring_at ?? null)}}"
                  name="contract_expiring_at"
      ></datepicker>
    </div>
  </div>
  <div class="col-md-6">
    <h3>
      Реквизиты объекта
    </h3>

    <div class="form-group">
      <label>Юридическое название</label>
      {!! Form::textarea('object_title', null, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group">
      <label>Точный адрес</label>
      {!! Form::textarea('object_addr', null, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group">
      <label>Источник</label>
      {!! Form::input('text', 'information_source_link', null, ['class' => 'form-control']) !!}
    </div>
  </div>
</div>