<table class="table table-bordered table-hover">
  <thead>
  <tr>
    <th>ID</th>
    <th class="text-center">Договор</th>
    <th>Заголовок</th>
    <th>Юридическое название</th>
    <th class="text-center">Дата созвона</th>
    <th class="text-center">
      Создан <br> Обновлён
    </th>
    <th>Брокер</th>
    <th>Действия</th>
  </tr>
  </thead>
  <tbody>

  @forelse ($estates as $estate)
    <tr>
      <td class="text-center">
        {{$estate->id}}
        <div class="font-weight-bold">
          {{$estate->marketing->value ?? ''}}
        </div>
      </td>
      <td class="text-center">
        @if($estate->contract)
          <div
              class="badge" {!! !$estate->contract->color ?'': " style='background-color:{$estate->contract->color}'" !!}>
            {{$estate->contract->value}}
          </div>
        @endif
        @if($estate->contract_expiring_at)
          <div class="badge badge-success">
            до {{$estate->contract_expiring_at->format('Y-m-d')}}
          </div>
        @endif
      </td>
      <td>
        <a href="{{ route('admin.estate.edit', $estate->id) }}">
          {{ $estate->title ?: 'N\A' }}
        </a>
      </td>
      <td>
        {{$estate->object_title}}
      </td>
      <td class="text-center">
        @if($estate->next_call_date)
          {{ \Carbon\Carbon::parse($estate->next_call_date)->format('Y-m-d') }}
        @else
          <div class="text-muted">Не назначена</div>
        @endif
      </td>
      <td class="text-center">
        {{$estate->created_at->format('Y-m-d')}}<br>
        {{$estate->updated_at->format('Y-m-d')}}
      </td>
      <td class="published">
        @if($estate->broker)
          {{$estate->broker->name}}
        @else
          <div class="text-muted">Не назначен</div>
        @endif
      </td>

      <td class="actions">
        <form action="{{ route('admin.estate.update', $estate->id) }}" method="post">
          @csrf
          @method('put')
          <a href="{{ route('admin.estate.edit', $estate->id) }}" class="btn btn-info btn-sm"
             title="Редактировать">
            <i class="fas fa-edit"></i>
          </a>
          @if($estate->is_archived)
            <confirmation classes="btn btn-success btn-sm"
                          name="is_archived"
                          value="0"
                          title="Востановить"
            >
              <i class="fas fa-trash-restore-alt"></i>
            </confirmation>
            <confirmation classes="btn btn-danger btn-sm"
                          name="is_delete"
                          value="1"
                          title="Удалить"
            >
              <i class="fas fa-trash-alt"></i>
            </confirmation>
          @else
            <confirmation classes="btn btn-danger btn-sm"
                          name="is_archived"
                          value="1"
                          title="Переместить в архив"
            >
              <i class="fas fa-file-archive"></i>
            </confirmation>
          @endif
        </form>
      </td>
    </tr>

  @empty

    <tr>
      <td colspan="10">
        Ничего не найдено
      </td>
    </tr>
  @endforelse
  </tbody>
</table>
