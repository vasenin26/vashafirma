@extends('admin/layout', [
'title' => 'Удаление объекта'
])

@section('content')

  <div class="alert alert-info">
  Вы действительно хотите удалить объект "{{$estate->title}}"
  </div>

  {{Form::open(['route' => ['admin.estate.destroy', $estate->id], 'method' => 'delete'])}}

  <a class="btn btn-success" href="{{URL::previous()}}">Нет</a>
  <button class="btn btn-danger">Удалить</button>

  {{Form::close()}}

@endsection