@extends('admin/layout', [
'title' => 'Комментарии брокеров. Объект ' . $estate->object_title . ' #' . $estate->id
])

@section('content')

  @include('admin/estate/menu', ['estate' => $estate])

  {!! $comments !!}

@endsection