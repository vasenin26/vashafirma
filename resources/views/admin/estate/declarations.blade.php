@extends('admin/layout', [
'title' => 'Декларации объекта ' . $estate->object_title . ' #' . $estate->id
])

@section('content')

  @include('admin/estate/menu', ['estate' => $estate])

  <nav class="navbar navbar-light bg-light mb-4">

    <a class="btn btn-outline-primary" href="{{route('admin.declaration.create', [
    'estate_id' => $estate->id
    ])}}">
      <i class="fas fa-plus"></i>
      Добавить декларацию
    </a>

    <!--
    <form class="form-inline">
      <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
      <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Найти</button>
    </form>
    -->
  </nav>

  @if(session('status'))
    <div class="alert alert-success">
      {{session('status')}}
    </div>
  @endif

  @if($declarations->count() == 0)
    <h5>Пока нет ни одной записи</h5>
  @else
    <table class="table table-bordered table-hover">
      <thead>
      <tr>
        <th>№</th>
        <th>Дата показа</th>
        <th>Брокер</th>
        <th>Ф.И.О.</th>
        <th>Телефон</th>
        <th>Действия</th>
      </tr>
      </thead>
      <tbody>
      @foreach($declarations as $declaration)
        <tr class="{{classIsSession($declaration, 'declaration_id', 'table-success')}}">
          <td>{{ $declaration->id }}</td>
          <td class="published">
            {{ $declaration->display_date }}
          </td>
          <td>
            {{ $declaration->broker->name ?? 'N/A'}}
            <a href="{{ route('admin.directory.item', [
              'index' => 'brokers',
              'id' => $declaration->broker,
              'back' => currentPage()
              ]) }}"
               title="Редактировать">
              <i class="fas fa-edit"></i>
            </a>
          </td>
          <td>
            {{$declaration->client_name}}
          </td>
          <td>
            {{ $declaration->client_phone }}
          </td>
          <td class="actions">
            <form action="{{ route('admin.declaration.update', $declaration->id) }}" method="post">
              @csrf
              @method('delete')
              <a href="{{ route('admin.declaration.edit', $declaration->id) }}" class="btn btn-info btn-sm"
                 title="Редактировать">
                <i class="fas fa-edit"></i>
              </a>
              <a href="" class="action_delete" title="Удалить">
                <button type="submit" class="btn btn-danger btn-sm" name="delete_article"
                        value="delete">
                  <i class="fas fa-trash-alt"></i>
                </button>
              </a>
            </form>
          </td>

        </tr>
      @endforeach
      </tbody>
    </table>

    {{  $declarations->links() }}

  @endif

@endsection