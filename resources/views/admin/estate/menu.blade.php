<ul class="nav nav-tabs mb-4">
  <li class="nav-item">
    <a class="nav-link {{classActivePath('EstateController@edit')}}"
       href="{{route('admin.estate.edit', ['estate' => $estate->id])}}">Параметры объекта</a>
  </li>
  <li class="nav-item">
    <a class="nav-link {{classActivePath('EstateController@showAd')}}"
       href="{{route('admin.estate.ad', ['estate' => $estate->id])}}">Объявление</a>
  </li>
  <li class="nav-item">
    <a class="nav-link {{classActivePath('EstateController@comments')}}"
       href="{{route('admin.estate.comments', ['estate' => $estate->id])}}">Коментарии брокеров
      @if($commentsCount = $estate->comments()->count())
        <span class="badge badge-info badge-pill">{{$commentsCount}}</span>
      @endif
    </a>
  </li>
  <li class="nav-item">
    <a class="nav-link {{classActivePath('EstateController@declarations')}}"
       href="{{route('admin.estate.declarations', ['estate' => $estate->id])}}">
      Декларации
      @if($declarationsCount = $estate->declarations()->count())
        <span class="badge badge-info badge-pill">{{$declarationsCount}}</span>
      @endif
    </a>
  </li>
  <li class="nav-item">
    <a class="nav-link {{classActivePath('EstateController@history')}}"
       href="{{route('admin.estate.history', ['estate' => $estate->id])}}">
      История работы с клиентом
    </a>
  </li>
</ul>