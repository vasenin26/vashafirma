@extends('admin/layout', [
'title' => 'Новый объект недвижимости'
])

@section('content')

  {!! Form::open(['route' => ['admin.estate.store'], 'method'=>'POST']) !!}

  @include('admin/estate/form', ['estate' => app('request')])


  <div class="fixed-bottom admin-panel-bottom">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-10 offset-2">
          {{ Form::submit('Добавить', ['class'=>'btn btn-success'])}}
        </div>
      </div>
    </div>
  </div>

  <div class="panel-spacer"></div>


  {!! Form::close() !!}

@endsection