@extends('admin/layout', [
'title' => request()->filled('today') ? 'Объекты на сегодя' : 'Список объектов недвижимости'
])

@section('content')

  @if(app('request')->input('is_archived'))

    <nav aria-label="breadcrumb">
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="{{route('admin.estate.index')}}">Объекты</a>
        </li>
        <li class="breadcrumb-item active">Архив</li>
      </ol>
    </nav>

  @endif

  <nav class="navbar navbar-light bg-light mb-4">

    <a class="btn btn-outline-primary" href="{{route('admin.estate.create')}}">
      <i class="fas fa-plus"></i>
      Добавить объект
    </a>
    {!! Form::model(request(), ['class' => 'form-inline d-flex flex-nowrap', 'method' => 'GET']) !!}

    @include('/admin/parts/directory', [
                'class' => 'mr-2',
                'style' => 'width: 320px',
                'placeholder' => 'Статус публикации',
                'name' => 'publication_state',
                'directoryIndex' => 'publication_state',
                'key' => 'value',
                'value' => request()->input('publication_state') ?: 'null'
                ])

    @include('/admin/parts/directory', [
                'class' => 'mr-2',
                'style' => 'width: 320px',
                'placeholder' => 'Статус объявления',
                'name' => 'ad_state',
                 'directoryIndex' => 'ad_state',
                 'key' => 'value',
                 'value' => request()->input('ad_state') ?: 'null'
              ])

    @if(Auth::user()->can('user_data_access'))
      @include('/admin/parts/directory', [
        'class' => 'mr-2',
        'style' => 'width: 320px',
        'placeholder' => 'Брокер',
        'name' => 'broker_id',
        'key' => 'name',
        'directoryIndex' => 'brokers',
        'value' => request()->input('broker_id') ?: 'null'
      ])
    @endif

    {!! Form::text('s', null, ['class'=>'form-control mr-2', 'placeholder' => 'Поиск']) !!}
    <button class="btn btn-outline-success my-2 my-sm-0">Найти</button>
    {!! Form::close() !!}
  </nav>

  @include('admin/estate/table', [
    'estates' => $estates
  ])

  <div class="text-right">
    <i class="fas fa-file-archive"></i> <a href="{{ route('admin.estate.archive') }}">Архив</a>
  </div>

@endsection