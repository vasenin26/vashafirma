@extends('admin/layout', [
'title' => 'Объявление для объекта ' . $estate->object_title . ' #' . $estate->id
])

@section('content')

  @include('admin/estate/menu', ['estate' => $estate])

  @if(session('success'))
    <div class="alert alert-success">
      Запись успешно сохранена.
    </div>
  @endif

  @if ($errors->any())
    <div class="alert alert-danger">
      <ul>
        @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
        @endforeach
      </ul>
    </div>
  @endif

  {!! Form::model($ad, ['route' => ['admin.estate.ad.update', $estate->id], 'method' => 'PUT']) !!}

  <input type="hidden" name="gallery_basket_id" value="{{old('gallery_basket_id', $galleryBasket->getBasketId())}}">
  <input type="hidden" name="files_basket_id" value="{{old('files_basket_id', $filesBasket->getBasketId())}}">

  <div class="row mb-4">

    <div class="col-md-6">

      <div class="card">
        <div class="card-header">
          Статистика по проекту
        </div>
        <div class="card-body">

          <div class="form-group row">
            <label class="col-form-label col-md-4">Статус публикации</label>
            <div class="col-form-label col-md-8">
              @include('/admin/parts/directory', [
                'name' => 'publication_state',
                'directoryIndex' => 'publication_state',
                'key' => 'value',
                'value' => old('publication_state', $ad->publication_state)]
                )
            </div>
          </div>

          <div class="form-group row">
            <label class="col-form-label col-md-4">Статус объявления</label>
            <div class="col-form-label col-md-8">
              @include('/admin/parts/directory', [
                'name' => 'ad_state',
                 'directoryIndex' => 'ad_state',
                 'key' => 'value',
                 'value' => $ad->ad_state ?? 'null'
              ])
            </div>
          </div>

          <div class="form-group row">
            <label class="col-form-label col-md-4">Дата создания</label>
            <div class="col-md-8">
              <input type="text" readonly class="form-control-plaintext" value="{{$estate->created_at}}">
            </div>
          </div>

          <div class="form-group row">
            <label class="col-form-label col-md-4">Всего просмотров</label>
            <div class="col-md-8">
              <input type="text" readonly class="form-control-plaintext" value="{{$estate->all_views ?? 0}}">
            </div>
          </div>

          <div class="form-group row">
            <label class="col-form-label col-md-4">Просмотров за сегодня</label>
            <div class="col-md-8">
              <input type="text" readonly class="form-control-plaintext" value="{{$estate->today_views ?? 0}}">
            </div>
          </div>
          @if($ad->id)
            <a href="{{route('ad.show', $ad->id)}}" class="btn btn-outline-primary" target="_blank">Посмотреть
              объявление на сайте</a>
          @endif
        </div>
      </div>

    </div>

    <div class="col-md-6">

      <div class="card">
        <div class="card-header">
          Контактная информация
        </div>
        <div class="card-body">

          <div class="form-group row">
            <label class="col-form-label col-md-4">Имя контактного лица:</label>
            <div class="col-md-8">
              {!! Form::input('text', 'contact_name', null, ['class' => 'form-control', 'required' => 'required']) !!}
            </div>
            @if ($errors->has('contact_name'))
              <div class="form-alert alert alert-danger">
                {{ $errors->first('contact_name') }}
              </div>
            @endif
          </div>

          <div class="form-group row">
            <label class="col-form-label col-md-4">Контакнтый телефон:</label>
            <div class="col-md-8">
              <maskedinput
                  class="form-control"
                  mask="\+\7 (111) 111-11-11"
                  name="contact_phone"
                  value="{{old('contact_phone', $ad->contact_phone ?? null)}}"/>
            </div>
          </div>

          <div class="form-group row">
            <label class="col-form-label col-md-4">Контакнтый e-mail:</label>
            <div class="col-md-8">
              {!! Form::input('text', 'contact_email', null, ['class' => 'form-control']) !!}
            </div>
            @if ($errors->has('contact_email'))
              <div class="form-alert alert alert-danger">
                {{ $errors->first('contact_email') }}
              </div>
            @endif
          </div>

          <div class="form-group row">
            <div class="col-form-label">&nbsp;</div>
          </div>

        </div>
      </div>

    </div>

  </div>

  <h3>Категория объекта</h3>

  <categoryselect
      name="categories"
      class="mb-4"
      v-bind:value='{!! $ad->categories !!}'
      :list='{!! $categories !!}'
  ></categoryselect>

  <h3>Фотографии</h3>

  <multiimage
      basket-id="{{old('gallery_basket_id', $galleryBasket->getBasketId())}}"
      v-bind:attached='{!! json_encode($gallery) !!}'
  ></multiimage>

  <h3>Основная информация</h3>

  <div class="form-group">
    <label>Краткое описание</label>
    <reachtext
        name="ad_description"
        value="{{$ad->ad_description ?? ''}}"
    ></reachtext>
    @if ($errors->has('ad_description'))
      <div class="form-alert alert alert-danger">
        {{ $errors->first('ad_description') }}
      </div>
    @endif
  </div>

  <div class="row">
    <div class="col-md-8">

      <div class="form-group row">
        <label class="col-form-label col-md-4">Презентация:</label>
        <div class="col-md-8">
          <filepicker
              basket-id="{{old('files_basket_id', $filesBasket->getBasketId())}}"
              name="presentation_file"
              file="{{$ad->presentation_file}}"
          >Презентация загружена
          </filepicker>
        </div>
      </div>

      <div class="form-group row">
        <label class="col-form-label col-md-4">Видео: <span class="label-dsc">Укажите ссылку на youtube</span></label>
        <div class="col-md-8">
          {!! Form::input('text', 'video_link', null, ['class' => 'form-control']) !!}
        </div>
      </div>

      <div class="form-group row">
        <label class="col-form-label col-md-4">Метро:</label>
        <div class="col-md-8">
          @include('/admin/parts/directory', [
            'name' => 'metro_id',
             'directoryIndex' => 'metro',
             'key' => 'value',
             'value' => old('metro_id', $ad->metro_id ?? 'null')
          ])
        </div>
      </div>

      <div class="form-group row">
        <label class="col-form-label col-md-4">Правовая форма:</label>
        <div class="col-md-8">
          {!! Form::input('text', 'legal_form', null, ['class' => 'form-control']) !!}
        </div>
      </div>

      <div class="form-group row">
        <label class="col-form-label col-md-4">Сайт:</label>
        <div class="col-md-8">
          {!! Form::input('text', 'site_url', null, ['class' => 'form-control']) !!}
        </div>
      </div>

      <div class="form-group row">
        <label class="col-form-label col-md-4">Срок существования:</label>
        <div class="col-md-2">
          <numberinput name="period_of_existence" :value="{{$ad->period_of_existence ?? 0}}"></numberinput>
        </div>
        <label class="col-form-label col-md-6">(в годах)</label>
      </div>

      <div class="form-group row">
        <label class="col-form-label col-md-4">Количество персонала:</label>
        <div class="col-md-2">
          <numberinput name="number_of_staff" :value="{{$ad->number_of_staff ?? 0}}"></numberinput>
        </div>
        <label class="col-form-label col-md-6">чел.</label>
      </div>

      <div class="form-group row">
        <label class="col-form-label col-md-4">Причина продажи:</label>
        <div class="col-md-8">
          {!! Form::input('text', 'reason_for_sale', null, ['class' => 'form-control']) !!}
        </div>
      </div>

    </div>
  </div>

  <h3>Финансовая картина</h3>

  <div class="row">
    <div class="col-md-8">

      <div class="form-group row">
        <label class="col-form-label col-md-4">Цена:</label>
        <div class="col-md-2">
          <price value="{{$ad->price ?? 0}}" name="price"></price>
        </div>
        <label class="col-form-label col-md-6">руб.</label>
      </div>

      <div class="form-group row">
        <label class="col-form-label col-md-4">Выручка/мес.:</label>
        <div class="col-md-2">
          <price value="{{$ad->revenue ?? 0}}" name="revenue"></price>
        </div>
        <label class="col-form-label col-md-6">руб.</label>
      </div>

      <div class="form-group row">
        <label class="col-form-label col-md-4">Прибыль/мес.:</label>
        <div class="col-md-2">
          <price value="{{$ad->profit ?? 0}}" name="profit"></price>
        </div>
        <label class="col-form-label col-md-6">руб.</label>
      </div>

      <div class="form-group row">
        <label class="col-form-label col-md-4">Прогнозируемая прибыль/мес.:</label>
        <div class="col-md-2">
          <price value="{{$ad->projected_earnings ?? 0}}" name="projected_earnings"></price>
        </div>
        <label class="col-form-label col-md-6">руб.</label>
      </div>

      <div class="form-group row">
        <label class="col-form-label col-md-4">Срок окупаемости:</label>
        <div class="col-md-2">
          <numberinput name="payback_period" :value="{{$ad->payback_period ?? 0}}"></numberinput>
        </div>
        <label class="col-form-label col-md-6">лет</label>
      </div>

    </div>
  </div>

  <div class="form-group">
    <label>Структура затрат</label>
    <reachtext
        name="cost_structure"
        value="{{$ad->cost_structure ?? ''}}"
    ></reachtext>
  </div>

  <div class="form-group">
    <label>Недвижимость</label>
    <reachtext
        name="property"
        value="{{$ad->property ?? ''}}"
    ></reachtext>
  </div>

  <div class="form-group">
    <label>Средства производства</label>
    <reachtext
        name="means_of_production"
        value="{{$ad->means_of_production ?? ''}}"
    ></reachtext>
  </div>

  <div class="form-group">
    <label>Сертификаты, лицензии, решения</label>
    <reachtext
        name="documents"
        value="{{$ad->documents ?? ''}}"
    ></reachtext>
  </div>

  <div class="form-group">
    <label>Персонал</label>
    <reachtext
        name="staff"
        value="{{$ad->staff ?? ''}}"
    ></reachtext>
  </div>

  <div class="form-group alert alert-info">
    <label>Выгоды для покупателя</label>
    <reachtext
        name="benefits"
        value="{{$ad->benefits ?? ''}}"
    ></reachtext>
  </div>

  <div class="form-group">
    <label>Дополнительная информация</label>
    <reachtext
        name="additional_info"
        value="{{$ad->additional_info ?? ''}}"
    ></reachtext>
  </div>

  <div class="fixed-bottom admin-panel-bottom">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-10 offset-2">
          {{ Form::submit('Сохранить',['class'=>'btn btn-success']) }}
        </div>
      </div>
    </div>
  </div>

  <div class="panel-spacer"></div>

  {!! Form::close() !!}

@endsection