<div class="row">
  <div class="col-6">

    <div class="form-group">
      <label>Имя пользователя</label>
      {!! Form::input('text', 'name', null, ['class' => 'form-control']) !!}
      @if ($errors->has('name'))
        <div class="alert alert-danger">
          {{ $errors->first('name') }}
        </div>
      @endif
    </div>

    <div class="form-group">
      <label>E-mail</label>
      {!! Form::input('text', 'email', null, ['class' => 'form-control']) !!}
      @if ($errors->has('email'))
        <div class="alert alert-danger">
          {{ $errors->first('email') }}
        </div>
      @endif
    </div>

    <div class="form-group">
      <label>Контактный телефон *</label>
      <maskedinput
          class="form-control"
          mask="\+\7 (111) 111-11-11"
          name="phone"
          value="{{old('phone', $user->phone ?? null)}}"/>
      @if ($errors->has('phone'))
        <div class="alert alert-danger">
          {{ $errors->first('phone') }}
        </div>
      @endif
    </div>

    <hr>

    <div class="form-group">
      <label>Пароль</label>
      {!! Form::password('password', ['class' => 'form-control']) !!}
      @if ($errors->has('password'))
        <div class="alert alert-danger">
          {{ $errors->first('password') }}
        </div>
      @endif
    </div>

    <div class="form-group">
      <label>Подтверждение пароля</label>
      {!! Form::password('password_confirmation', ['class' => 'form-control']) !!}
      @if ($errors->has('password_confirmation'))
        <div class="alert alert-danger">
          {{ $errors->first('password_confirmation') }}
        </div>
      @endif
    </div>

  </div>
</div>