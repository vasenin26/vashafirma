@extends('admin/layout', [
          'title' => 'Редактирование пользователя',
        ])

@section('content')

  @include('admin/users/menu', ['user' => $user])

  @if(session('success'))
    <div class="alert alert-success">
      {{session('success')}}
    </div>
  @endif

  {!! Form::model($user, ['route' => ['admin.users.update', $user->id], 'method' => 'PUT']) !!}

  <div class="row">
    <div class="col-6">
      <div class="form-group">
        <label>Статус профиля</label>
        <combobox
            class="w-100"
            name="status"
            label="label"
            value='{{$user->status ?? 'null'}}'
            :list='[{
        "label": "Активный",
        "id": "active"
        },{
        "label": "Заблокирован",
        "id": "blocked"
        }
]'
        ></combobox>
      </div>
    </div>
  </div>

  @include('admin/users/form', ['user' => $user])

  <button class="btn btn-success" type="submit" name="submit" value="user">Сохранить</button>

  {!! Form::close() !!}

@endsection

