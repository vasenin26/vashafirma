@extends('admin/layout',
[
  'title' => 'Управление пользователями',
])

@section('content')

  <nav class="navbar navbar-light bg-light mb-4">
    <a class="btn btn-outline-primary" href="{{ route('admin.users.create') }}">
      <i class="fas fa-plus"></i> Добавить пользователя
    </a>
  </nav>

  @if(session('status'))
    <div class="alert alert-success alert-dismissable">
      {{session('status')}}
    </div>
  @endif

  <table class="table table-bordered table-hover">
    <thead>
    <tr>
      <th>№</th>
      <th>Имя</th>
      <th>Регистрация</th>
      <th>Роли</th>
      <th>Действия</th>
    </tr>
    </thead>
    <tbody>
    @foreach($users as $user)
      <tr>
        <td>{{ $counter++ }}</td>
        <td>
          <a href="{{ route('admin.users.edit', $user->id) }}">
            {{ $user->name }}
          </a>
        </td>
        <td>{{ $user->created_at->format("d.m.Y") }}</td>
        <td>
          @if($user->roles->count() == 0)
            <p>У пользователя нет ни одной роли.
              <a href="{{ route('admin.users.edit', $user->id) }}">Добавьте</a>
            </p>
          @else
            {{ $user->roles->pluck('display_name')->implode(', ') }}
          @endif
        </td>
        <td class="actions">
          <a href="{{ route('admin.users.edit', $user->id) }}" class="action_edit btn btn-primary"
             title="Редактировать">
            <i class="fas fa-edit"></i>
          </a>
        </td>
      </tr>
    @endforeach
    </tbody>
  </table>

  <div class="text-right mb-4">
    <i class="fas fa-file-archive"></i> <a href="{{route('admin.users.index', ['all' => 1])}}">Все пользователи</a>
  </div>

  {{ $users->links() }}

@stop
