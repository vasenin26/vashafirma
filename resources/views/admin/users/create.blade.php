@extends('admin/layout', [
          'title' => 'Создание пользователя',
        ])

@section('content')

  @if(session('status'))
    <div class="alert alert-success">
      {{session('status')}}
    </div>
  @endif

  {!! Form::model($user, ['route' => ['admin.users.store', $user->id], 'method' => 'POST']) !!}

  @include('admin/users/form', ['user' => $user])

  <button class="btn btn-success" type="submit" name="submit" value="user">Создать</button>

  {!! Form::close() !!}


@endsection
