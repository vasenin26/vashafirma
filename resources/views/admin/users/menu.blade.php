<ul class="nav nav-tabs mb-4">
  <li class="nav-item">
    <a class="nav-link {{classActivePath('UsersController@edit')}}"
       href="{{route('admin.users.edit', ['user' => $user->id])}}">Основная информация</a>
  </li>
  <li class="nav-item">
    <a class="nav-link {{classActivePath('UsersController@showRoles')}}"
       href="{{route('admin.users.roles', ['user' => $user->id])}}">Роли</a>
  </li>
</ul>