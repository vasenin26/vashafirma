@extends('admin/layout', [
          'title' => 'Назначенные роли',
        ])

@section('content')

  @include('admin/users/menu', ['user' => $user])

  @if(session('status'))
    <div class="alert alert-success">
      {{session('status')}}
    </div>
  @endif

  {!! Form::model($user, ['route' => ['admin.users.updateRoles', $user->id], 'method' => 'PUT']) !!}

  <div class="row">
    <div class="col-sm-6">
      <h3>Роли</h3>
    </div>
    <div class="col-sm-6 text-right">
      <a href="{{route('admin.roles.index')}}">Управление ролями</a>
    </div>
  </div>

  <div class="list-group mb-4">
    @foreach($roles as $role)
      <label class="list-group-item list-group-item-action">
        <input type="hidden" name="roles[{{ $role->id }}]" value="0">
        <input type="checkbox" name="roles[{{ $role->id }}]" value="1"
               @if($user->hasRole($role->name))
               checked="checked"
            @endif
        >
        {{ $role->display_name }}
      </label>
    @endforeach
  </div>

  <button class="btn btn-success" type="submit" name="submit" value="user">Сохранить</button>

  {!! Form::close() !!}

@endsection

