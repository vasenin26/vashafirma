<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
  <title>{{ $title }}</title>
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <!--[if lt IE 11]>
  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
  <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
  <![endif]-->

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
  <meta http-equiv="X-UA-Compatible" content="IE=edge"/>

  <link rel="stylesheet" href="{{ asset('admin/css/app.bundle.css') }}">

  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css"
        integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">

</head>

<body>
<div class="container-fluid admin_main" id="app">
  <div class="row flex-nowrap">

    <div class="col admin-sidebar"
         v-bind:class="{ 'admin-sidebar--closed': !isOpenSideBar }"
    >
      <div class="admin-sidebar__body">
        <form class="data_admin" action="{{route('logout')}}" method="post">
          @csrf
          <span v-if="isOpenSideBar">
          {{ Auth::user()->name }}
            </span>
          <button class="btn btn-link"><i class="fas fa-sign-out-alt"></i></button>
        </form>
        <div class="list-group list-group-flush">
          <a class="list-group-item list-group-item-action {{classActiveController('DashboardController')}}"
             href="{{ route('admin.dashboard') }}" title="Главная">
            <i class="fas fa-home col-2 text-align-center"></i>
            <span class="admin-sidebar__menu-label">Главная</span>
          </a>
          <a class="list-group-item list-group-item-action {{classActiveController('EstateController')}}"
             href="{{ route('admin.estate.index') }}" title="Объекты">
            <i class="fas fa-building col-2 text-align-center"></i>
            <span class="admin-sidebar__menu-label">Объекты</span>
          </a>
          <a class="list-group-item list-group-item-action {{classActiveUri('admin/estate/archive')}}"
             href="{{ route('admin.estate.archive') }}" title="Объекты в архиве">
            <i class="fa fa-file-archive col-2 text-align-center"></i>
            <span class="admin-sidebar__menu-label">Объекты в архиве</span>
          </a>
          @if(Auth::user()->can('personal_planning'))
            <a class="list-group-item list-group-item-action {{classActiveController('PlaneController')}}"
               href="{{ route('admin.my-plane.index') }}" title="Планирование">
              <i class="fas fa-user-clock col-2 text-align-center"></i>
              <span class="admin-sidebar__menu-label">Планирование</span>
            </a>
          @endif
          <a class="list-group-item list-group-item-action {{classActiveController('DeclarationController')}}"
             href="{{ route('admin.declaration.index', ['today' => 1]) }}" title="Декларации">
            <i class="far fa-address-card col-2 text-align-center"></i>
            <span class="admin-sidebar__menu-label">Декларации</span>
          </a>
          <a class="list-group-item list-group-item-action {{classActiveController('CustomerCallingController')}}"
             href="{{ route('admin.customer.calling.index', ['today' => 1]) }}" title="Обзвон">
            <i class="fas fa-headset col-2 text-align-center"></i>
            <span class="admin-sidebar__menu-label">Обзвон</span>
          </a>
          @if(Auth::user()->can('manage_article'))
            <a class="list-group-item list-group-item-action {{classActiveController('ArticlesController')}}"
               href="{{ route('admin.articles.index') }}" title="Статьи">
              <i class="fas fa-file-alt col-2 text-align-center"></i>
              <span class="admin-sidebar__menu-label">Статьи</span>
            </a>
          @endif
          @if(Auth::user()->can('manage_review'))
            <a class="list-group-item list-group-item-action {{classActiveController('ReviewController')}}"
               href="{{ route('admin.review.index') }}" title="Отзывы">
              <i class="far fa-comment-dots col-2 text-align-center"></i>
              <span class="admin-sidebar__menu-label">Отзывы</span>
            </a>
          @endif
          @if(Auth::user()->can('manage_vacancy'))
            <a class="list-group-item list-group-item-action {{classActiveController('JobsController')}}"
               href="{{ route('admin.jobs.index') }}" title="Вакансии">
              <i class="fas fa-user-edit col-2 text-align-center"></i>
              <span class="admin-sidebar__menu-label">Вакансии</span>
            </a>
          @endif
          @if(Auth::user()->can('manage_user'))
            <a class="list-group-item list-group-item-action {{classActiveController('UsersController')}}"
               href="{{ route('admin.users.index') }}" title="Пользователи">
              <i class="fas fa-users col-2 text-align-center"></i>
              <span class="admin-sidebar__menu-label">Пользователи</span>
            </a>
          @endif
          @if(Auth::user()->can('manage_documents'))
            <a class="list-group-item list-group-item-action {{classActiveController('DocumentsController')}}"
               href="{{ route('admin.documents.index') }}" title="Документы">
              <i class="fas fa-file-download col-2 text-align-center"></i>
              <span class="admin-sidebar__menu-label">Документы</span>
            </a>
          @endif
          @if(Auth::user()->can('manage_request_form'))
            <a class="list-group-item list-group-item-action {{classActiveController('FormReportController')}}"
               href="{{ route('admin.form.index') }}" title="Заявки с сайта">
              <i class="fab fa-wpforms col-2 text-align-center"></i>
              <span class="admin-sidebar__menu-label">Заявки с сайта</span>
            </a>
          @endif
          @if(Auth::user()->can('manage_dictionary'))
            <a class="list-group-item list-group-item-action {{classActiveController('DirectoryController')}}"
               href="{{ route('admin.directory.index') }}" title="Справочники">
              <i class="fas fa-book-open col-2 text-align-center"></i>
              <span class="admin-sidebar__menu-label">Справочники</span>
            </a>
          @endif
        </div>
      </div>
      <div class="admin-sidebar__footer">
        <div class="list-group list-group-flush">
          <span class="list-group-item list-group-item-action admin-sidebar__action"
                @click="toggleSideBar">
            <i class="far fa-caret-square-left col-2 text-align-center" v-if="isOpenSideBar"></i>
            <i class="far fa-caret-square-right col-2 text-align-center" v-if="!isOpenSideBar"></i>
            <span class="admin-sidebar__menu-label">Свернуть панель</span>
          </span>
        </div>
      </div>
    </div>
    <div class="col pt-4 pb-4">

      @if(($title ?? null) && (!isset($hideTitleTag)))
        <h1 class="mb-4 text-nowrap text-truncate">{{$title}}</h1>
      @endif

      @yield('content')
    </div>
  </div>
</div>

<script src="{{ asset('admin/js/app.bundle.js') }}"></script>
</body>
</html>
