@extends('admin/layout', [
'title' => 'Планирование'
])

@section('content')
  <div class="row">
    <div class="col-6">
      @forelse($planes as $planeItem)
        @include('admin/plane/card', ['item' => $planeItem])
      @empty
        Пока планов нет
      @endforelse

      {{$planes->links()}}
    </div>
    <div class="col-6">
      <form class="mb-4 pb-4 border-bottom"
            action="{{route('admin.my-plane.add')}}"
            method="post"> @csrf

        <h4>
          Запланировать выезд
        </h4>

        @include('admin/plane/form')

        <button class="btn btn-outline-primary">Добавить</button>
      </form>
    </div>
  </div>

@endsection