<div class="card mb-2 comment">
  <div class="card-header"
       id="{{$item->id}}"
       data-created_on="{{$item->action_at}}">
    @if(Auth::user()->id !== $item->broker_id)
      {{$item->owner->name}} -
    @endif
    {{$item->action_at ?? 'Без даты'}} - {{$item->action->value ?? 'Не указано'}}
  </div>
  <div class="card-body" data-role="content">
    {{$item->content}}
  </div>
  <div class="card-footer">
    @switch($item->object_class)
      @case(App\Estate::class)
      <i class="fas fa-building"></i>
      {{$item->estate->title}}
      <a href="{{route('admin.estate.history', $item->object_id)}}"><i class="fas fa-sign-out-alt"></i></a>
      @break
      @case(App\Declaration::class)
      <i class="far fa-address-card"></i>
      {{$item->declaration ? $item->declaration->client_name : 'N\A'}}
      <a href="{{route('admin.declaration.edit', $item->object_id)}}"><i
            class="fas fa-sign-out-alt"></i></a>
      @break
      @case(App\CustomerCallingHistory::class)
      <i class="far fa-address-card"></i>
      {{$item->customer_history->prevew_label}}
      <a href="{{route('admin.customer.calling.edit', $item->object_id)}}"><i
            class="fas fa-sign-out-alt"></i></a>
      @break
    @endswitch
  </div>
</div>