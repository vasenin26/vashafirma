<div class="form-row">
  <div class="col-6 form-group">
    <label>Запланированное время</label>
    <div class="row">
      <div class="col-6">
        <datepicker
            name="plane_date"
        ></datepicker>
      </div>
      <div class="col-6">
        <timepicker
            name="plane_time"
        ></timepicker>
      </div>
    </div>
  </div>
  <div class="col-6 form-group">
    <label>Действие</label>
    @include('/admin/parts/directory', [
      'name' => 'action_id',
      'directoryIndex' => 'broker_actions',
      'key' => 'value',
      'value' => request()->input('action_id')
      ])
  </div>
</div>
<div class="form-row">
  <div class="col-12 form-group">
    <label>Описание</label>
    <textarea rows="3" class="form-control" name="description"></textarea>
  </div>
</div>
<objectselector
    object-url="{{route('admin.dashboard.my-objects')}}"
></objectselector>