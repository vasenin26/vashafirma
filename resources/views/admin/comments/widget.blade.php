<div class="comments" id="comments">
  @foreach($comments as $comment)
    <div class="card mb-2 comment">
      <div class="card-header"
           id="{{$comment->id}}"
           data-created_on="{{$comment->created_on}}">
        <div class="row">
          <div class="col-6">
            <i class="far fa-comment"></i> {{$comment->owner->name}} <span class="text-muted">({{$comment->created_at}})</span>
          </div>
          <div class="col-6 text-right">
          </div>
        </div>
      </div>
      <div class="card-body" data-role="content">
        {{$comment->content}}
      </div>
    </div>
  @endforeach
</div>

@if($access)

  @auth

    <form action="{{$formAction}}" method="POST">

      {{ csrf_field() }}

      <div class="row">
        <div class="col-md-6">
          <div class="form-group">
            <label>Добавить коментарий:</label>
            <textarea rows="4"
                      name="content"
                      class="form-control"
            ></textarea>
          </div>
        </div>
      </div>

      <button type="submit" class="btn btn-primary">
        Добавить
      </button>

    </form>

  @endauth

  @guest

    <div class="alert alert-info">
      Аторизуйтесь чтобы оставлять комментарии.
      <hr>
      Войти с помощью <a href="{{ route('login') }}">номера телефона <i class="fas fa-key"></i></a>
    </div>

  @endguest

@else

  <div class="alert alert-info">
    Вы не можете оставлять комментарии к этой записи.
  </div>

@endif