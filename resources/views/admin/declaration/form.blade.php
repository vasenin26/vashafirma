<div class="row">
  <div class="col-md-6">

    <div class="form-group">
      <label>Ф.И.О. клиента *</label>
      {!! Form::input('text', 'client_name', null, ['class' => 'form-control']) !!}
      @if ($errors->has('client_name'))
        <div class="form-alert alert alert-danger">
          {{ $errors->first('client_name') }}
        </div>
      @endif
    </div>

    <div class="form-group">
      <label>Контактный телефон *</label>
      <maskedinput
          class="form-control"
          mask="\+\7 (111) 111-11-11"
          name="client_phone"
          value="{{old('client_phone', $declaration->client_phone ?? null)}}"></maskedinput>
      @if ($errors->has('client_phone'))
        <div class="form-alert alert alert-danger">
          {{ $errors->first('client_phone') }}
        </div>
      @endif
    </div>

    <div class="form-group">
      <label>Контактный email</label>
      {!! Form::input('text', 'client_email', null, ['class' => 'form-control']) !!}
      @if ($errors->has('client_email'))
        <div class="form-alert alert alert-danger">
          {{ $errors->first('client_email') }}
        </div>
      @endif
    </div>

    {{--
    <div class="form-group form-check">
      {!! Form::checkbox('is_subscription', 1, null, ['class' => 'form-check-input', 'id' => 'field_is_subscription']) !!}
      <label class="form-check-label" for="field_is_subscription">Участие в рассылке</label>
    </div>
    --}}

    <div class="form-group">
      <label>Дата подписания</label>
      <datepicker
          name="display_date"
          value="{{$declaration->display_date ? $declaration->display_date->format('Y-m-d') : ''}}"></datepicker>
      @if ($errors->has('display_date'))
        <div class="form-alert alert alert-danger">
          {{ $errors->first('display_date') }}
        </div>
      @endif
    </div>

  </div>
  <div class="col-md-6">

    <div class="form-group">
      <label>Объект</label>
      @include('/admin/parts/directory', [
        'name' => 'estate_id',
        'key' => 'object_title',
        'route' => 'admin.estate.index',
        'directoryIndex' => 'App\Estate',
        'value' =>  old('estate_id', $declaration->estate_id ?? 'null')
      ])
      @if ($errors->has('estate_id'))
        <div class="form-alert alert alert-danger">
          {{ $errors->first('estate_id') }}
        </div>
      @endif
    </div>

    @if(Auth::user()->can('set_declaration_broker'))
      <div class="form-group">
        <label>Брокер</label>
        @include('/admin/parts/directory', [
          'name' => 'broker_id',
          'directoryIndex' => 'brokers',
          'key' => 'name',
          'value' => old('broker_id', $declaration->broker_id ?? 'null')
        ])
      </div>
    @endif

  </div>
</div>