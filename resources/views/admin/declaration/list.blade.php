@extends('admin/layout', [
'title' => request()->filled('today') ? 'Декларации на сегодя' : 'Управление декларациями'
])

@section('content')

  <nav class="navbar navbar-light bg-light mb-4">
    <a class="btn btn-outline-primary" href="{{ route('admin.declaration.create') }}">
      <i class="fas fa-plus"></i> Создать декларацию
    </a>
    {!! Form::model(request(), ['class' => 'form-inline d-flex flex-nowrap', 'method' => 'GET']) !!}

    {!! Form::hidden('today') !!}

    @if(Auth::user()->can('user_data_access'))
      @include('/admin/parts/directory', [
        'class' => 'mr-2',
        'style' => 'width: 420px',
        'name' => 'broker_id',
        'key' => 'name',
        'directoryIndex' => 'brokers',
        'value' => request()->input('broker_id') ?: 'null'
      ])
    @endif

    @include('/admin/parts/directory', [
      'class' => 'mr-2',
      'style' => 'width: 420px',
      'name' => 'estate_id',
      'key' => 'object_title',
      'route' => 'admin.estate.index',
      'directoryIndex' => 'App\Estate',
      'value' => request()->input('estate_id') ?: 'null'
    ])

    {!! Form::text('s', null, ['class'=>'form-control mr-2', 'placeholder' => 'Поиск']) !!}
    <button class="btn btn-outline-success my-2 my-sm-0">Найти</button>
    {!! Form::close() !!}
  </nav>

  @if(session('status'))
    <div class="alert alert-success">
      {{session('status')}}
    </div>
  @endif

  @if($declarations->count() == 0)
    <h5>Пока нет ни одной записи</h5>
  @else

    @include('admin/declaration/table', [
    'declarations' => $declarations
    ])

    {{  $declarations->links() }}
  @endif

  <div class="mt-4 alert alert-secondary">
    @if(!request()->filled('today'))
      <a href="{{route('admin.declaration.index', ['today' => 1])}}">Декларации на сегодня</a>
    @else
      Показаны декларации на сегодняшную дату. <a href="{{route('admin.declaration.index', [])}}">Весь список</a>
    @endif
  </div>

@stop
