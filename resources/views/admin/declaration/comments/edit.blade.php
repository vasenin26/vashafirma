@extends('admin/layout', [
'title' => 'Редактирование коментария'
])

@section('content')

  {!! Form::model($comment, ['route' => ['admin.declaration.comments.update', $estate->id, $comment->id], 'method' => 'PUT']) !!}

  <div class="form-group">
    <label>Комментарий</label>
    {!! Form::textarea('content', null, ['class' => 'form-control']) !!}
  </div>

  {{ Form::submit('Сохранить',['class'=>'btn btn-success']) }}
  {{ Form::submit('Вернуться',['class'=>'btn btn-light']) }}


@endsection