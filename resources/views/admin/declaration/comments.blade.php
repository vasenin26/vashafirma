@extends('admin/layout', [
'title' => 'Коментарии к декларации номер: ' . $declaration->id
])

@section('content')

  <nav aria-label="breadcrumb">
    <ol class="breadcrumb">
      <li class="breadcrumb-item">
        <a href="{{route('admin.estate.edit', ['estate' => $estate->id])}}">Объект</a>
      </li>
      <li class="breadcrumb-item">
        <a href="{{route('admin.estate.declarations', ['estate' => $estate->id])}}">Декларации</a>
      </li>
      <li class="breadcrumb-item">
        <a href="{{route('admin.declaration.edit', ['estate' => $declaration->id])}}">Параметры декларации</a>
      </li>
      <li class="breadcrumb-item active">Коментарии</li>
    </ol>
  </nav>

  @include('admin/declaration/menu', ['declaration' => $declaration])

  {!! $comments !!}

@endsection