
<ul class="nav nav-tabs mb-4">
  <li class="nav-item">
    <a class="nav-link {{classActivePath('DeclarationController@edit')}}"
       href="{{route('admin.declaration.edit', ['estate' => $declaration->id])}}">Параметры деклараци</a>
  </li>
  <li class="nav-item">
    <a class="nav-link {{classActivePath('DeclarationController@comments')}}"
       href="{{route('admin.declaration.comments', ['estate' => $declaration->id])}}">Коментарии
      @if($commentsCount = $declaration->comments()->count())
        <span class="badge badge-info badge-pill">{{$commentsCount}}</span>
      @endif
    </a>
  </li>
</ul>