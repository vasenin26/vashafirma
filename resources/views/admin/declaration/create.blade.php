@extends('admin/layout', [
'title' => 'Новая декларация'
])

@section('content')
  {!! Form::model($declaration, ['action' => ['Admin\DeclarationController@store'], 'method'=>'POST']) !!}

  @include('admin/declaration/form', ['declaration' => $declaration])

  {{ Form::submit('Добавить',['class'=>'btn btn-success'])}}

  {!! Form::close() !!}
@stop
