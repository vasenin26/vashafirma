<table class="table table-bordered table-hover">
  <thead>
  <tr>
    <th>№</th>
    <th>Объект (ID)</th>
    <th>Дата показа</th>
    <th>Автор</th>
    <th>Брокер</th>
    <th>Ф.И.О.</th>
    <th>Телефон</th>
    <th>Действия</th>
  </tr>
  </thead>
  <tbody>
  @foreach($declarations as $declaration)
    <tr class="{{classIsSession($declaration, 'declaration_id', 'table-success')}}">
      <td>{{ $declaration->id }}</td>
      <td>
        {{ $declaration->estate->object_title ?? 'N/A' }}
        @if($declaration->estate_id)
          <a href="{{ route('admin.estate.edit', $declaration->estate_id) }}"
             title="Редактировать">
            <i class="fas fa-edit"></i>
          </a>
        @endif
      </td>
      <td class="published">
        {{ $declaration->display_date ? $declaration->display_date->format('Y-m-d') : 'N\A' }}
      </td>
      <td>
        {{$declaration->owner->name}}
      </td>
      <td>
        {{ $declaration->broker->name ?? 'N/A' }}
      </td>
      <td>
        {{$declaration->client_name}}
      </td>
      <td>
        {{ $declaration->client_phone }}
      </td>
      <td class="actions">
        <form action="{{ route('admin.declaration.update', $declaration->id) }}" method="post">
          @csrf
          @method('delete')
          <a href="{{ route('admin.declaration.edit', $declaration->id) }}" class="btn btn-info btn-sm"
             title="Редактировать">
            <i class="fas fa-edit"></i>
          </a>
          <a href="" class="action_delete" title="Удалить">
            <button type="submit" class="btn btn-danger btn-sm" name="delete_article"
                    value="delete">
              <i class="fas fa-trash-alt"></i>
            </button>
          </a>
        </form>
      </td>

    </tr>
  @endforeach
  </tbody>
</table>