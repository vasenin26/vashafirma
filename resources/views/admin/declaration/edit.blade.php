@extends('admin/layout', [
'title' => 'Декларация номер: ' . $declaration->id
])

@section('content')

  <nav aria-label="breadcrumb">
    <ol class="breadcrumb">
      <li class="breadcrumb-item">
        <a href="{{route('admin.estate.edit', ['estate' => $estate->id])}}">Объект</a>
      </li>
      <li class="breadcrumb-item">
        <a href="{{route('admin.estate.declarations', ['estate' => $estate->id])}}">Декларации</a>
      </li>
      <li class="breadcrumb-item active">Редактирование записи</li>
    </ol>
  </nav>

  @include('admin/declaration/menu', ['declaration' => $declaration])

  {!! Form::model($declaration, ['action' => ['Admin\DeclarationController@update', $declaration->id], 'method'=>'PUT']) !!}

  @include('admin/declaration/form', ['declaration' => $declaration])

  {{ Form::submit('сохранить',['class'=>'btn btn-success'])}}

  {!! Form::close() !!}
@stop
