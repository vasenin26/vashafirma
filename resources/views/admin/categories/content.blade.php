@extends('admin/layout')

@section('content')
    <div id="pages_content">
        <div class="add_element">
            <h1>Все категории</h1>
            <a href="{{ route('categories.create') }}">
                <button class="btn btn-primary">Добавить категорию</button>
            </a>
        </div>
        <hr>
        <div class="pages_all">
            <table class="table table-bordered table-hover">
                <thead>
                <tr>
                    <th>№</th>
                    <th>Название</th>
                    <th>Дата добавления</th>
                    <th>Состояние</th>
                    <th>Действия</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>1</td>
                    <td>Первая категория</td>
                    <td>01.04.2012</td>
                    <td>
                        <a href="" class="status">
                            <i class="fas fa-eye"></i>
                        </a>
                    </td>
                    <td class="actions">
                        <a href="{{ route('categories.edit', 1) }}" class="action_edit" title="Редактировать">
                            <i class="fas fa-edit"></i>
                        </a>
                        <a href="" class="action_delete" title="Удалить">
                            <i class="fas fa-trash-alt"></i>
                        </a>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
@stop
