@extends('admin/layout', [
'title' => 'Документы'
])

@section('content')

  {{Form::open(['route' => 'admin.documents.upload', 'files' => true, 'method' => 'POST', 'class' => 'mb-4'])}}
  {{Form::file('document', ['class' => 'form-control', 'onchange' => 'form.submit()'])}}
  {{Form::close()}}

  <table class="table table-striped table-hover">
    <tbody>
    @foreach($files as $file)
      <tr>
        <td>
          <a href="{{Storage::disk()->url($file)}}" target="_blank">{{\File::basename($file)}}</a>
        </td>
        <td>
          {{Storage::disk()->getSize($file)}} b
        </td>
        <td>
          <a class="btn btn-outline-danger btn-sm"
             href="{{route('admin.documents.delete', ['file' => \File::basename($file)])}}">
            <i class="fas fa-trash-alt"></i></a>
        </td>
      </tr>
    @endforeach
    </tbody>
  </table>


@stop