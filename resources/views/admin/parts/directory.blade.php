<div class="input-group flex-nowrap {{$class ?? ''}}" {!! ($style ?? null) ? "style='$style'" : '' !!}>
  <combobox
      placeholder="{{$placeholder ?? 'Выберите из списка'}}"
      class="w-100"
      name="{{$name}}"
      label="{{$key ?? 'title'}}"
      v-bind:value='{{$value ?? 'null'}}'
      :list='{!! json_encode(DirectoryService::getSelectArray($directoryIndex) )  !!}'
  ></combobox>
  @if(Auth::user()->can('manage_dictionary'))
    <div class="input-group-append">
      @if($route ?? null)
        <a class="btn btn-outline-secondary col" target="_blank" href="{{route($route)}}">
          <i class="fas fa-bars"></i>
        </a>
      @else
        <a class="btn btn-outline-secondary col" target="_blank"
           href="{{route('admin.directory.show', $directoryIndex)}}">
          <i class="fas fa-bars"></i>
        </a>
      @endif
    </div>
  @endif
</div>