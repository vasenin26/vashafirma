@extends('admin/layout', [
'title' => "Заявка номер #{$form->id}: ". __('form.'.$form->form_key)
])

@section('content')
  @if(is_array($form->content))
    @foreach($form->content as $key => $value)
      <div class="form-row mb-2">
        <div class="col-2">
          {{__('form.'. $key)}}
        </div>
        <div class="col-5">
          {{$value}}
        </div>
      </div>
    @endforeach
  @else
    <div class="alert alert-info">
      Заявка не содержит данных
    </div>
  @endif

  <hr>

  <div class="form-row">
    <div class="col-2">
      Создана
    </div>
    <div class="col-5">
      {{$form->created_at}}
    </div>
  </div>
  <div class="form-row">
    <div class="col-2">
      Просмотрена
    </div>
    <div class="col-5">
      {{$form->opened_at}}
    </div>
  </div>
  <div class="form-row">
    <div class="col-2">
      Брокер
    </div>
    <div class="col-5">
      {{$form->reviewer->name}}
    </div>
  </div>

  {!! $comments !!}

@endsection