@extends('admin/layout', [
'title' => request()->filled('new') ? 'Необработанные заявки' : 'Заявки'
])

@section('content')

  <nav class="navbar navbar-light bg-light mb-4">
    {!! Form::model(request(), ['class' => 'form-inline d-flex flex-nowrap', 'method' => 'GET']) !!}

    @if(Auth::user()->can('user_data_access'))
      <label class="mr-2">Фильтр по брокеру: </label>
      @include('/admin/parts/directory', [
        'class' => 'mr-2',
        'style' => 'width: 320px',
        'name' => 'broker_id',
        'key' => 'name',
        'directoryIndex' => 'brokers',
        'value' => request()->input('broker_id') ?: 'null'
      ])
    @endif

    <label class="mr-2">Фильтр по типу формы: </label>
    <div class="input-group flex-nowrap mr-2" style="width: 320px">
      <combobox
          class="w-100"
          name="form_key"
          label="title"
          item_key="id"
          value='{{ request()->input('form_key') ?: 'null'}}'
          :list='{!! $form_keys !!}'
      ></combobox>
    </div>

    <label class="form-group mr-2"><input type="checkbox" name="is_new"
                                          value="1" {{request()->input('is_new') == 1 ? 'checked' : ''}}> показывать
      только новые</label>

    <button class="btn btn-outline-success my-2 my-sm-0">Найти</button>
    {!! Form::close() !!}
  </nav>

  @if(session('status'))
    <div class="alert alert-success">
      {{session('status')}}
    </div>
  @endif

  @if($forms->count() == 0)
    <h5>Пока нет ни одной записи</h5>
  @else
    <table class="table table-bordered table-hover">
      <thead>
      <tr>
        <th>№</th>
        <th>Ключи формы</th>
        <th>Дата получения</th>
        <th>Дата просмотра</th>
        <th>Брокер</th>
      </tr>
      </thead>
      <tbody>
      @foreach($forms as $form)
        <tr>
          <td>
            {{$form->id}}
          </td>
          <td>
            <a href="{{route('admin.form.show', $form->id)}}">{{__('form.'.$form->form_key)}}</a>
          </td>
          <td>
            {{$form->created_at}}
          </td>
          <td>
            {{$form->opened_at}}
          </td>
          <td>
            @if($form->opened_by)
              {{$form->reviewer->name}}
            @endif
          </td>
        </tr>
      @endforeach
      </tbody>
    </table>

    {{  $forms->links() }}
  @endif

@stop
