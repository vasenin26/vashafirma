@extends('admin/layout')

@section('content')

  <nav aria-label="breadcrumb">
    <ol class="breadcrumb">
      <li class="breadcrumb-item">
        <a href="{{route('admin.users.index')}}">Пользователи</a>
      </li>
      <li class="breadcrumb-item">
        <a href="{{route('admin.roles.index')}}">Роли</a>
      </li>
      <li class="breadcrumb-item active">Добавление роли</li>
    </ol>
  </nav>

  @if(session('status'))
    <div class="alert alert-success alert-dismissable">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
      {{session('status')}}
    </div>
  @endif
  <form action="{{ route('admin.roles.store') }}" method="post" role="form">
    @csrf
    @method('post')

    <div class="form-group">
      <label>Название роли *</label>
      <input type="text" class="form-control" name="display_name" value="{{ old('display_name') }}" required/>
      @if ($errors->has('display_name'))
        <span class="help-block">
                                    <strong>{{ $errors->first('display_name') }}</strong>
                                </span>
      @endif
    </div>

    <div class="form-group">
      <label>Описание роли *</label>
      <input type="text" class="form-control" name="description" value="{{ old('description') }}" required/>
      @if ($errors->has('description'))
        <span class="help-block">
                                    <strong>{{ $errors->first('description') }}</strong>
                                </span>
      @endif
    </div>

    <button class="btn btn-success" type="submit" name="roles" value="roles">Добавить</button>

  </form>
@stop

