@extends('admin/layout')

@section('content')


  <nav aria-label="breadcrumb">
    <ol class="breadcrumb">
      <li class="breadcrumb-item">
        <a href="{{route('admin.users.index')}}">Пользователи</a>
      </li>
      <li class="breadcrumb-item active">Роли</li>
    </ol>
  </nav>

  <nav class="navbar navbar-light bg-light mb-4">
    <a class="btn btn-outline-primary" href="{{ route('admin.roles.create') }}">
      <i class="fas fa-plus"></i>
      Добавить роль
    </a>
  </nav>

  @if(session('status'))
    <div class="alert alert-success alert-dismissable">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
      {{session('status')}}
    </div>
  @endif

  @if($roles->count() == 0)
    <h5>Нет ни одной добавленной роли</h5>
  @else
    <table class="table table-bordered table-hover">
      <thead>
      <tr>
        <th>№</th>
        <th>Название роли</th>
        <th>Разрешения</th>
        <th>Действия</th>
      </tr>
      </thead>
      <tbody>
      @foreach($roles as $role)
        <tr>
          <td>{{ $counter++ }}</td>
          <td>{{ $role->display_name }}</td>
          <td>
            @if($role->permissions->count() == 0)
              <p>Не назначено разрешений.
                <a href="{{ route('admin.roles.edit', $role->id) }}">Добавить</a>
              </p>
            @else
              {{ $role->permissions->pluck('display_name')->implode(', ') }}
            @endif
          </td>
          <td class="actions">
            <a href="{{ route('admin.roles.edit', $role->id) }}" class="btn btn-info" title="Редактировать">
              <i class="fas fa-edit"></i>
            </a>
          </td>
        </tr>
      @endforeach
      </tbody>
    </table>
  @endif
@endsection

