@extends('admin/layout', [
'title' => "Редактировать роль - $role->display_name"
])

@section('content')

  <nav aria-label="breadcrumb">
    <ol class="breadcrumb">
      <li class="breadcrumb-item">
        <a href="{{route('admin.users.index')}}">Пользователи</a>
      </li>
      <li class="breadcrumb-item">
        <a href="{{route('admin.roles.index')}}">Роли</a>
      </li>
      <li class="breadcrumb-item active">Редактирование записи</li>
    </ol>
  </nav>

  @if(session('status'))
    <div class="alert alert-success">
      {{session('status')}}
    </div>
  @endif
  <form action="{{ route('admin.roles.update', $role->id) }}" method="post" role="form">
    @csrf
    @method('put')

    <div class="form-group">
      <label>Название роли *</label>
      <input type="text" class="form-control" name="display_name" value="{{ $role->display_name }}"
             required/>
      @if ($errors->has('display_name'))
        <span class="help-block">
                                    <strong>{{ $errors->first('display_name') }}</strong>
                                </span>
      @endif
    </div>

    <div class="form-group">
      <label>Описание роли</label>
      <input type="text" class="form-control" name="description" value="{{ $role->description }}"/>
      @if ($errors->has('description'))
        <span class="help-block">
                                    <strong>{{ $errors->first('description') }}</strong>
                                </span>
      @endif
    </div>

    <hr>
    <h5>Разрешения для роли</h5>
    <hr>

    @foreach($permissions as $item)
      <div class="form-group">
        <input type="checkbox" name="permissions[]" value="{{ $item->id }}"
               @if($role->permissions->where('id', $item->id)->count())
               checked="checked"
            @endif
        >
        <label>{{ $item->display_name }}</label>
      </div>
    @endforeach
    <button class="btn btn-success" type="submit">Сохранить</button>
  </form>
@stop