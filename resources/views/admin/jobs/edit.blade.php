@extends('admin/layout', [
'title' => 'Вакансия: ' . $jobs->job_name
])

@section('content')
  {!! Form::model($jobs, ['action' => ['Admin\JobsController@update', $jobs->id], 'method'=>'PUT']) !!}

  @include('admin/jobs/form', ['jobs' => $jobs])

  {{ Form::submit('Сохранить',['class'=>'btn btn-success'])}}

  {!! Form::close() !!}
@stop
