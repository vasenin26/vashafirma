@extends('admin/layout', [
'title' => 'Список вакансий'
])

@section('content')

  <nav class="navbar navbar-light bg-light mb-4">

    <a class="btn btn-outline-primary" href="{{route('admin.jobs.create')}}">
      <i class="fas fa-plus"></i>
      Добавить вакансию
    </a>
  </nav>

  @if(session('status'))
    <div class="alert alert-success alert-dismissable">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
      {{session('status')}}
    </div>
  @endif

  @if($jobs->count() == 0)
    <h5>Пока нет ни одной страницы</h5>
  @else
    <table class="table table-bordered table-hover">
      <thead>
      <tr>
        <th>Название вакансии</th>
        <th>Дейтвия</th>
      </tr>
      </thead>
      <tbody>
      @foreach($jobs as $job)
        <tr>
          <td class="article_title">
            <span>
              <a href="{{ route('admin.jobs.edit', $job->id) }}">
                {{ $job->job_name }}
              </a>
            </span>
          </td>
          <td class="actions">
            <form class="d-inline" action="{{ route('admin.jobs.update', $job->id) }}" method="post">
              @csrf
              @method('put')
              <a href="{{ route('admin.jobs.edit', $job->id) }}" class="btn btn-info btn-sm"
                 title="Редактировать">
                <i class="fas fa-edit"></i>
              </a>
            </form>
            <form class="d-inline" method="POST" action="{{route('admin.jobs.destroy', $job->id)}}">
              @csrf
              @method('DELETE')
              <a href="" class="action_delete" title="Переместить в корзину">
                <button type="submit" class="btn btn-danger btn-sm" name="delete_review"
                        value="delete">
                  <i class="fas fa-trash-alt"></i>
                </button>
              </a>
            </form>
          </td>

        </tr>
      @endforeach
      @endif
      </tbody>
    </table>

@endsection
