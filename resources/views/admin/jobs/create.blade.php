@extends('admin/layout', [
'title' => 'Новая вакансия'
])

@section('content')
  {!! Form::open(['action' => ['Admin\JobsController@store'], 'method'=>'POST']) !!}

  @include('admin/jobs/form', ['jobs' => $jobs])

  {{ Form::submit('Добавить',['class'=>'btn btn-success'])}}

  {!! Form::close() !!}
@stop

