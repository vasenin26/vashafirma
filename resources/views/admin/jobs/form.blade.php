<div class="row">
  <div class="col-md-6">
    <div class="form-group">
      <label>Отдел</label>
      @include('/admin/parts/directory' , ['name' => 'department_id', 'directoryIndex' => 'departments'])
      @if ($errors->has('title'))
        <div class="alert alert-danger">
          {{ $errors->first('title') }}
        </div>
      @endif
    </div>
    <div class="form-group">
      <label>Название вакансии</label>
      {!! Form::input('text', 'job_name', $jobs->job_name, ['class' => 'form-control', 'required' => 'required']) !!}
      @if ($errors->has('job_name'))
        <div class="alert alert-danger">
          {{ $errors->first('job_name') }}
        </div>
      @endif
    </div>
    <div class="form-group">
      <label>Публикация страницы</label>
      <div class="form-check">
        <input type="hidden" name="is_published" value="0">
        <input type="checkbox"
               class="form-check-input"
               id="is_published"
               name="is_published" {{checked_is($jobs->is_published, 'is_published', 1)}}
               value="1">
        <label class="form-check-label" for="is_published">Опликовано</label>
      </div>
    </div>
    <div class="form-group">
      <label>Описание вакансии</label>

      <reachtext
          name="job_description"
          value="{{$jobs->job_description ?? ''}}"
      ></reachtext>
      @if ($errors->has('job_description'))
        <div class="alert alert-danger">
          {{ $errors->first('job_description') }}
        </div>
      @endif
    </div>
  </div>
</div>
