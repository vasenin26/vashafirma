require('./bootstrap');

window.Vue = require('vue');

Vue.component('categoryselect', require('./components/categorySelect').default);
Vue.component('colorpicker', require('./components/colorPicker').default);
Vue.component('combobox', require('./components/combobox').default);
Vue.component('datepicker', require('./components/datepicker').default);
Vue.component('timepicker', require('./components/timepicker').default);
Vue.component('filepicker', require('./components/filepicker').default);
Vue.component('maskedinput', require('vue-masked-input').default);
Vue.component('multiimage', require('./components/multiImage/multiImage').default);
Vue.component('numberinput', require('@chenfengyuan/vue-number-input'));
Vue.component('objectselector', require('./components/objectSelector').default);
Vue.component('price', require('./components/price').default);
Vue.component('reachtext', require('./components/reachtext').default);
Vue.component('sortable', require('./components/sortable').default);
Vue.component('nested', require('./components/nested').default);
Vue.component('confirmation', require('./components/confirmation').default);

const app = new Vue({
    el: '#app',
    data() {
        return {
            isOpenSideBar: this.defineSidebarState()
        }
    },
    methods: {
        toggleSideBar() {
            this.isOpenSideBar = !this.isOpenSideBar;
            window.localStorage.isOpenSideBar = this.isOpenSideBar;
        },
        defineSidebarState() {
            return window.localStorage.hasOwnProperty('isOpenSideBar') ? window.localStorage.isOpenSideBar === 'true' : true;
        }
    }
});
