require('./bootstrap');

window.MH = new (require('./ModuleHandler').default);

document.addEventListener('DOMContentLoaded', function () {

    MH.handleModules(document);

    $('[data-modal]').on('click', function () {
        MH.openModal($(this).attr('href') || $(this).data('href'));
    });

});