export default function (core, node) {

    $('[data-role=action-btn]', node).on('click', () => {
        let input = $('input', node);
        let $node = $(node);

        if($node.hasClass('active')){
            $node.submit();
            return false;
        }

        $node.addClass('active');
        input.focus();

        return false;
    });

    $('input', node).on('click', event => event.stopPropagation());

    $(document).on('click', function () {
        if ($('input', node).val() === '') {
            $(node).removeClass('active');
        }
    });

};