export default function (core, node) {

    $(node).on('click', (event) => {
        $('[data-role=phone-container]', node).text($(node).data('phone'));
        $(node).off('click');
    });

};