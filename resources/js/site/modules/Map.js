const YAMAP_KEY = '#yamap_loader';
const YAMAP_LINK = 'https://api-maps.yandex.ru/2.1/?apikey=db2c1a0d-00ce-4dc6-a0f9-30a82e8599bb&lang=ru_RU';

function waitYaMap() {
    return new Promise(function (s) {
        if(window.ymaps){
            s();
        }

        let script = document.querySelector(YAMAP_KEY);

        if(!script){
            script = document.createElement('script');
            script.id = YAMAP_KEY;
            script.src = YAMAP_LINK;

            document.querySelector('body')
                .appendChild(script);
        }

        script.addEventListener('load', () => {
            s();
        });

    });
}

export default function (core, node) {

    waitYaMap().then(() => {
        ymaps.ready(function () {

            var id = $(node).attr('id');

            if (!id) {
                id = 'map' + (new Date()).getTime();
                $(node).attr('id', id);
            }

            var cord = $(node).data('center');
            var point = $(node).data('pointer');

            cord = cord.split(',');
            point = point.split(',');

            var myMap = new ymaps.Map(id, {
                center: [cord[0], cord[1]],
                zoom: $(node).data('zoom')
            });

            var m = new ymaps.GeoObject({
                geometry: {
                    type: "Point",
                    coordinates: [point[0], point[1]],
                    pixelRendering: 'static'
                },
                properties: {}
            }, {
                preset: 'twirl#blueStretchyIcon'
            });

            myMap.behaviors.disable(['scrollZoom']);
            myMap.geoObjects.add(m);

        });

    });

};