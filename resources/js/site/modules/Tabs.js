require('jquery');

function showTab(idx) {
    $('.tabs__toggle', this).removeClass('tabs__toggle--active')
        .eq(idx).addClass('tabs__toggle--active');

    $('.tab', this).removeClass('tab--active')
        .eq(idx).addClass('tab--active');
}

/**
 * Available options
 *  action-idx : first showed tab idx
 * @param node
 */

export default function (core, node) {

    $('.tabs__toggle', node).on('click', function () {

        let idx = $(this).data('idx');

        showTab.call(node, idx);

    }).each(function (index) {

        $(this).data('idx', index);
    });


    let activeIdx = $(node).data('active-idx') || 0;
    showTab.call(node, activeIdx);

};
