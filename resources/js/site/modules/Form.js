let Form = require('../class/Form').default;

export default function (core, node) {

    let form = new Form(node, {
        success: () => {
            core.openModal($(node).data('success') || '/modal/success');
        },
        fail: () => {

        }
    });


};