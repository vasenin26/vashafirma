let counter = 0;

export default function (core, node) {

    if ($('.gallery-box__images img', node).length > 1) {
        $('<div>').addClass('gallery-box__carousel-navigation')
            .appendTo(node);

        let gallery = $('.gallery-box__images', node).owlCarousel({
            singleItem: true,
            paginationWrap: $('.gallery-box__carousel-navigation', node),
            autoHeight: true
        }).data('owlCarousel');

        $('<div>').addClass('gallery-box__carousel-arrow')
            .addClass('gallery-box__carousel-arrow--prev')
            .on('click', function () {
                gallery.prev();
            })
            .appendTo(node);

        $('<div>').addClass('gallery-box__carousel-arrow')
            .addClass('gallery-box__carousel-arrow--next')
            .on('click', function () {
                gallery.next();
            })
            .appendTo(node);
    }

    let fancyId = 'fancy' + (++counter);

    $('.simple-gallery__href', node)
        .attr('data-fancybox', fancyId)
        .fancybox();

};