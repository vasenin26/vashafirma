require('@fancyapps/fancybox');

export default class {
    handleModules(scope) {
        let modules = scope.querySelectorAll('[data-module]');

        modules.forEach((node) => {

            if (node.classList.contains('loaded')) {
                return;
            }

            node.classList.add('loaded');

            let result = require('./modules/' + node.dataset.module)
                .default(this, node);

            (() => {
                if (result instanceof Promise) {
                    return result;
                } else {
                    return new Promise((s) => {
                        s();
                    });
                }
            })().then(() => {
                node.classList.add('runned');
            });

        });
    }

    openModal(src, type, safe) {
        if (!safe && $.fancybox.getInstance()) {
            $.fancybox.getInstance().close();
        }

        $.fancybox.open({
            src: src,
            type: type || 'ajax',
            afterShow: (instanse, current) => {
                this.handleModules(instanse.$refs.container.get(0));
            }
        });
    }

    closeModal() {
        $.fancybox.getInstance().close();
    }
}