require('jquery');
let Inputmask = require('inputmask');
let validator = require('./FormValidator').default;

export default class {
    _node = null;
    _options = {};

    constructor(node, options) {
        this._node = node;
        this._options = options;

        $('input,textarea', this._node).on('focus', function () {
            $(this).parent()
                .removeClass('wrong');
        }).each(function () {
            $(this).wrap('<div class="field_wrapper"></div>');
            $(this).parent().append('<span class="error_label"></span>');
        });

        /* phone */

        let mask = "+7 (999) 999 99 99";

        let el = $("[data-type=phone]", this._node)
            .attr('placeholder', mask)
            .each(function () {
                (new Inputmask(mask)).mask(this);
            });

        let submitBtn = $('[type=submit]', node);

        $(node).closest('form')
            .on('submit', (event) => {
                this.submit(submitBtn, event);
                return false;
            });

        submitBtn
            .on('click', (event) => {
                this.submit(submitBtn, event);
                return false;
            });
    }

    submit(node, event) {
        event.preventDefault();
        event.stopPropagation();

        if ($(node).prop('disabled')) {
            return;
        }

        $(node).prop('disabled', true);

        this.sendForm()
            .always(function () {
                $(node).prop('disabled', false)
            });
    }

    sendForm() {
        let errors = validator.validate(this._node);

        if(errors){
            this.showErrors(errors);
            return $.Deferred().reject().promise();
        }

        let $form = $(this._node);
        let data = this.getFormData();

        let params = {
            url: $form.attr('action') || '/mail/mail.php',
            type: $form.attr('method') || 'post',
            data: data,
            dataType: 'JSON'
        };

        if ($form.prop("tagName").toLowerCase() === 'form') {
            params.processData = false;
            params.contentType = false;

            params.data = new FormData($form.get(0));
        }

        return $.ajax(params)
            .done((response) => {
                if (response.success === 1) {
                    $('input[type=text], textarea').val('');

                    if (typeof this._options.success == 'function') {
                        this._options.success(this, response);
                    }
                } else {
                    if (typeof this._options.fail == 'function') {
                        this._options.success(this, response);
                    } else {
                        alert(response.error);
                    }
                }
            }).fail((response) => {
                if (response.responseJSON && response.responseJSON.errors) {
                    this.showErrors(response.responseJSON.errors);
                }
            });
    }

    showErrors(errors) {
        $.each(errors, (key, item) => {
            let $field = $('[name=' + key + ']', this._node);
            let message = $.isArray(item) ? item.join(', ') : item;

            if(!message){
                return;
            }

            if (!$field.parent().hasClass('field_wrapper')) {
                $field.wrap('<div class="field_wrapper"></div>');
                $field.parent().append('<span class="error_label"></span>');
            }
            $field.parent().addClass('wrong');
            $field.siblings('.error_label').text(message);
        });
    }

    getFormData() {
        let data = {};

        $('input,textarea', this._node).each(function () {
            data[$(this).attr('name')] = $(this).val();
        });

        return data;
    }
}