require('jquery');

let messages = {
    required: 'поле должно быть заполнено'
};

let validator = {
    required: function ($i) {
        if ($i.val() == '' || $i.val() == $i.attr('placeholder')) {
            return false;
        }
        return true;
    },
    email: function ($i) {

        if ($i.val() == '')
            return true;

        var r = new RegExp(".+@.+\..+", "i");
        if (!r.test($i.val())) {
            return false;
        }
        return true;
    },
    phone: function ($i) {
        var r = new RegExp("^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$", "i");
        if (!r.test($i.val())) {
            return false;
        }
        return true;
    }
};

class FormValidator {
    validate(formNode) {

        var errors = {};
        var isError = false;

        $('input,textarea', formNode).each((key, item) => {

            let $item = $(item);
            let itemErrors = [];

            let r = $item.data('rules') || '';
            r = r.split(',');

            if ($item.prop('required')) {
                r.push('required');
            }

            for (let i = 0; i < r.length; i++) {
                let rule = r[i];
                if (validator[rule]) {
                    if (!validator[rule]($item)) {
                        itemErrors.push(messages[rule]);
                        isError = true;
                        break;
                    }
                }
            }

            if (itemErrors) {
                errors[$item.attr('name')] = itemErrors;
            }

        });

        if(!isError){
            return null;
        }

        return errors;

    }
}

export default new FormValidator();