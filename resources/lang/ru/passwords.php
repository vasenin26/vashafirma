<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Пароли должны совпадать и содержать не менее 8-ми символов.',
    'reset' => 'Ваш пароль успешно сброшен!',
    'sent' => 'На указанный адрес отправлено письмо с инструкциями!',
    'token' => 'Этот токен сброса пароля недействителен.',
    'user' => "Пользователь с таким адресом не зарегистрирован.",

];
