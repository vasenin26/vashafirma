<?php

return [
    'callback' => 'Обратный звонок',
    'sale_request' => 'Запрос на продажу',
    'makeprice' => 'Предложения цены',
    'object_request' => 'Заявка на объект',
    'overview' => 'Заявка на осмотр объекта',
    'job_request' => 'Заявка на работу',

    'object_id' => 'ID Оъекта',
    'form_source' => 'Название формы',
    'name' => 'Имя',
    'email' => 'E-mail',
    'phone' => 'Телефон',
    'want_price' => 'Желаемая цена',
];